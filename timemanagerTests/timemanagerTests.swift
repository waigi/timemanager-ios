//
//  timemanagerTests.swift
//  timemanagerTests
//
//  Created by Can on 10/05/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import XCTest
@testable import timemanager

class timemanagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLogin() {
        let expection = self.expectation(description: "Service Call Finished Expectation")
        LoginManager.sharedManager.login("admin", password: "admin",
                                         success: {
                                            expection.fulfill()
            },
                                         failure: { error in
                                            if let error = error {
                                            } else {
                                            }
        })
        self.waitForExpectations(timeout: 60.0) { (error: NSError?) -> Void in
            if error != nil {
                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
            }
        }
    }
    
    func testFamilyAccount() {
        let expection = self.expectation(description: "Service Call Finished Expectation")
        AccountManager.fetchAccounts({
            expection.fulfill()
            },
                                     failure: { error in
        })
        self.waitForExpectations(timeout: 60.0) { (error: NSError?) -> Void in
            if error != nil {
                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
            }
        }
    }
    
//    func testTask() {
//        let expection = self.expectationWithDescription("Service Call Finished Expectation")
//        AccountManager.fetchTasks({
//            expection.fulfill()
//            },
//                                     failure: { error in
//        })
//        self.waitForExpectationsWithTimeout(60.0) { (error: NSError?) -> Void in
//            if error != nil {
//                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
//            }
//        }
//    }
    
    func testAccount() {
        let expection = self.expectation(description: "Service Call Finished Expectation")
        AccountManager.fetchAccount({
            expection.fulfill()
            },
                                    failure: { error in
        })
        self.waitForExpectations(timeout: 60.0) { (error: NSError?) -> Void in
            if error != nil {
                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
            }
        }
    }
    
    func testGenerateTask() {
        let expection = self.expectation(description: "Service Call Finished Expectation")
        TaskManager.generateTaskWithAccountFamiltyId("57a54fbc0cf210b07c5048bd", success: {
            expection.fulfill()
            },
                                    failure: { error in
        })
        self.waitForExpectations(timeout: 60.0) { (error: NSError?) -> Void in
            if error != nil {
                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
            }
        }
    }
    
}
