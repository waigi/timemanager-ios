# README #

This project is currently written in Swift2.3 and in the process of transforming into Swift 3.0.

### How do I get set up? ###

* Checkout the latest codes, make sure it's Develop branch
* Run pod install
* Open up generated .xcworkspace
* use 'woods1' '194577' as test account username and password
* long press the clock to generate 'Tasks'
* Swipe up/down the 'Tasks' to start doing a 'Task'

### Who do I talk to? ###

* Can Zhan
* email: canzhan@gmail.com