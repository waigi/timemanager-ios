//
//  SmileWorldClockModal.swift
//  timemanager
//
//  Created by Can on 4/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@objc public protocol SmileWorldClockModelDelegate {
    func timeZonesInModelHaveChanged()
    func secondHasPassed()
}

open class SmileWorldClockModel: NSObject, NSCoding {
    //MARK: ==
    open override func isEqual(_ object: Any?) -> Bool {
        if let rhs = object as? SmileWorldClockModel {
            return selectedTimeZones == rhs.selectedTimeZones
        }
        return false
    }
    
    //MARK: Property
    var timer: Timer?
    open var selectedTimeZones = [SmileTimeZoneData]()
    var delegate: SmileWorldClockModelDelegate!
    
    //MARK: Init
    public override init() {
        
    }
    
    public convenience init(theDelegate: SmileWorldClockModelDelegate) {
        self.init()
        self.delegate = theDelegate
    }
    
    //MARK: NSCoding
    required public init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: "selectedTimeZones") as? [SmileTimeZoneData] {
            selectedTimeZones = data
        }
        super.init()
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(selectedTimeZones, forKey: "selectedTimeZones")
    }
    
    //MARK: Timer
    open func startTimerWithDelegate(_ theDelegate: SmileWorldClockModelDelegate) {
        delegate = theDelegate
        if selectedTimeZones.count > 0 {
            startTimer()
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(SmileWorldClockModel.fireTimer), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: RunLoopMode.commonModes)
    }
    
    func fireTimer() {
        if selectedTimeZones.count > 0 {
            for timeZoneData in selectedTimeZones {
                timeZoneData.updateTime()
            }
            delegate.secondHasPassed()
        }
    }
    
    //MARK: Add New TimeZoneData
    open func addData(_ timeZoneData: SmileTimeZoneData) {
        selectedTimeZones.append(timeZoneData)
        timeZoneData.updateTime()
        if selectedTimeZones.count == 1 {
            startTimer()
        }
    }
}
