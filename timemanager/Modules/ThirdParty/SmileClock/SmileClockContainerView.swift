//
//  SmileClockContainerView.swift
//  timemanager
//
//  Created by Can on 4/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable open class SmileClockContainerView: UIView {
    
    @IBInspectable open var bgColor: UIColor = UIColor.black
    @IBInspectable open var graduationColor: UIColor = UIColor.white
    @IBInspectable open var fontColor: UIColor = UIColor.white
    @IBInspectable open var handColor: UIColor = UIColor.white
    @IBInspectable open var secHandColor: UIColor = UIColor.yellow
    @IBInspectable open var clockStyleNum: Int = 3
    @IBInspectable open var hour: Int = 9
    @IBInspectable open var minute: Int = 30
    @IBInspectable open var second: Int = 7
    
    open var bgImage: UIImage?
    open var hourHandImage: UIImage?
    open var minHandImage: UIImage?
    open var secHandImage: UIImage?
    open var centerImage: UIImage?
    
    open var clockView: SmileClockView!
    
    #if TARGET_INTERFACE_BUILDER
    override public func willMoveToSuperview(newSuperview: UIView?) {
    addClockView()
    }
    #else
    override open func awakeFromNib() {
        super.awakeFromNib()
        addClockView()
    }
    #endif
    
    open func updateClockView() {
        clockView.removeFromSuperview()
        addClockView()
    }
    
    fileprivate func addClockView() {
        self.backgroundColor = UIColor.clear
        clockView = SmileClockView(frame: self.bounds)
        
        clockView.clockStyle = safeSetClockStyle(clockStyleNum)
        clockView.bgColor = bgColor
        clockView.graduationColor = graduationColor
        clockView.handColor = handColor
        clockView.secHandColor = secHandColor
        clockView.fontColor = fontColor
        
        clockView.hour = hour
        clockView.minute = minute
        clockView.second = second
        
        clockView.bgImage = bgImage
        clockView.centerImage = centerImage
        clockView.hourHandImage = hourHandImage
        clockView.minHandImage = minHandImage
        clockView.secHandImage = secHandImage
        
        clockView.updateClockViewLayers()
        
        self.addSubview(clockView)
    }
    
    func safeSetClockStyle(_ styleNum: Int) -> ClockStyle {
        if clockStyleNum > ClockStyle.count() || clockStyleNum < 0 {
            clockStyleNum = 0
        } else {
            clockStyleNum = styleNum
        }
        return ClockStyle(rawValue: clockStyleNum)!
    }
}
