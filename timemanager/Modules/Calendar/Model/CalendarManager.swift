//
//  CalendarManager.swift
//  timemanager
//
//  Created by Can on 30/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import EventKit
import SwiftDate

class CalendarManager {
    
    struct Constant {
        static let calendarIdentifierKey = "CDMCalendar"
        static let calendarTitle = "CDM"
    }
    
    fileprivate static func createCalendarWith(_ eventStore: EKEventStore) -> EKCalendar {
        let calendar = EKCalendar(for: .event, eventStore: eventStore)
        calendar.title = Constant.calendarTitle
        let sourcesInEventStore = eventStore.sources
        do {
            // Filter the available sources and select the "Local" source to assign to the new calendar's
            // source property
            calendar.source = sourcesInEventStore.filter{ (source: EKSource) -> Bool in
                source.sourceType.rawValue == EKSourceType.local.rawValue
                }.first ?? sourcesInEventStore.first!
            try eventStore.saveCalendar(calendar, commit: true)
            UserDefaults.standard.set(calendar.calendarIdentifier, forKey: Constant.calendarIdentifierKey)
            UserDefaults.standard.synchronize()
        } catch {
        }
        return calendar
    }
    
    static func checkCalendarAuthorizationStatus() {
        func requestAccessToCalendar() {
            let eventStore = EKEventStore()
            eventStore.requestAccess(to: .event) { (accessGranted, error) in
                if accessGranted {
                    createCalendarWith(eventStore)
                }
            }
        }
        
        let status = EKEventStore.authorizationStatus(for: .event)
        switch (status) {
        case .notDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case .authorized: break
        // Things are in line with being able to show the calendars in the table view
        case .restricted, .denied: break
            // We need to help them give us permission
        }
    }
    
    static func addEvent(withTask task: Task) {
        func addTask(_ task: Task, to calendar: EKCalendar, with eventStore: EKEventStore, isStart: Bool) {
            let newEvent = EKEvent(eventStore: eventStore)
            newEvent.calendar = calendar
            newEvent.title = isStart ? task.calendarEventStartTitle : task.calendarEventEndTitle
            newEvent.startDate = (isStart ? task.plannedStartTime : task.plannedEndTime) as Date
            newEvent.endDate = (isStart ? task.plannedStartTime : task.plannedEndTime) as Date
            newEvent.addAlarm(EKAlarm(absoluteDate: newEvent.startDate))
            do {
                try eventStore.save(newEvent, span: .thisEvent, commit: true)
            } catch {
            }
        }
        
        guard EKEventStore.authorizationStatus(for: .event) == .authorized else {
            DLog("No access to calendar")
            return
        }
        let eventStore = EKEventStore()
        let calendarIdentifier = String(describing: UserDefaults.standard.object(forKey: Constant.calendarIdentifierKey))
        // Use Event Store to create a new calendar instance
        let cal = eventStore.calendars(for: .event).filter { (calendar) -> Bool in
            calendar.title == Constant.calendarTitle
        }.first
        if let calendar = cal {
            // fetch existing event
            let predicate = eventStore.predicateForEvents(withStart: (task.plannedStartTime as Date) - 1.days, end: (task.plannedEndTime as Date) + 1.days, calendars: [calendar])
            let startEvent = eventStore.events(matching: predicate).filter { (event) -> Bool in
                event.title == task.calendarEventStartTitle
            }.first
            let endEvent = eventStore.events(matching: predicate).filter { (event) -> Bool in
                event.title == task.calendarEventEndTitle
                }.first
            let isStartExisting = startEvent != nil
            let isEndExisting = endEvent != nil
            if isStartExisting {
                if task.plannedStartTime.isInPast {
                    //TODO: delete event
                }
            } else {
                if task.plannedStartTime.isInFuture {
                    addTask(task, to: calendar, with: eventStore, isStart: true)
                }
            }
            if isEndExisting {
                if task.plannedEndTime.isInPast {
                    //TODO: delete event
                }
            } else {
                if task.plannedEndTime.isInFuture {
                    addTask(task, to: calendar, with: eventStore, isStart: false)
                }
            }
        } else {
            let calendar = createCalendarWith(eventStore)
            if task.plannedStartTime.isInFuture {
                addTask(task, to: calendar, with: eventStore, isStart: true)
                addTask(task, to: calendar, with: eventStore, isStart: false)
            }
        }
    }
    
    static func deleteEvent(withTask task: Task) {
        func delete(_ event: EKEvent, from calendar: EKCalendar, with eventStore: EKEventStore, isStart: Bool) {
            do {
                try eventStore.remove(event, span: .thisEvent, commit: true)
            } catch {
                DLog("Failed to delete from calendar: \(event.title)")
            }
        }
        
        guard EKEventStore.authorizationStatus(for: .event) == .authorized else {
            DLog("No access to calendar")
            return
        }
        let eventStore = EKEventStore()
        let calendarIdentifier = String(describing: UserDefaults.standard.object(forKey: Constant.calendarIdentifierKey))
        // Use Event Store to create a new calendar instance
        let cal = eventStore.calendars(for: .event).filter { (calendar) -> Bool in
            calendar.title == Constant.calendarTitle
            }.first
        if let calendar = cal {
            // fetch existing event
            let predicate = eventStore.predicateForEvents(withStart: (task.plannedStartTime as Date) - 1.days, end: (task.plannedEndTime as Date) + 1.days, calendars: [calendar])
            let startEvent = eventStore.events(matching: predicate).filter { (event) -> Bool in
                event.title == task.calendarEventStartTitle
                }.first
            let endEvent = eventStore.events(matching: predicate).filter { (event) -> Bool in
                event.title == task.calendarEventEndTitle
                }.first
            if let startEvent = startEvent {
                delete(startEvent, from: calendar, with: eventStore, isStart: true)
            }
            if let endEvent = endEvent {
                delete(endEvent, from: calendar, with: eventStore, isStart: false)
            }
        }
    }
    
}
