//
//  CircleButtonView.swift
//  timemanager
//
//  Created by Can on 23/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class CircleButtonView: NibView {
    
    @IBInspectable var text: String = "" {
        didSet {
            setNeedsUpdateView()
        }
    }

    @IBOutlet weak var button: UIButton!
    
    override func updateView() {
        super.updateView()
        button.titleLabel?.text = text
    }
    
}
