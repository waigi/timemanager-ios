//
//  OptionCell.swift
//  timemanager
//
//  Created by Can on 3/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class OptionCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var text1Label: UILabel!
    @IBOutlet weak var text2Label: UILabel!
    
}
