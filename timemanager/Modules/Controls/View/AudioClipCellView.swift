//
//  AudioClipCellView.swift
//  timemanager
//
//  Created by Can on 7/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import AVFoundation

@IBDesignable class AudioClipCellView: NibCellView {
    
    // MARK: Variables
    
    @IBInspectable @IBOutlet var viewController: PromiseViewController?
    
    @IBInspectable var audioURLString: String = "" {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    @IBInspectable var audioLengthInSeconds: Int = 0 {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    @IBInspectable var _promiseType: Int = 1
    
    @IBInspectable var _promisePersonType: Int = 1
    /// 是否只能播放不能录音
    @IBInspectable var playOnly: Bool = false {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    var promise: Promise?
    
    /// Remote audio
    var audio: Audio? {
        didSet {
            if let urlString = audio?.urlString {
                audioURLString = urlString
            }
            if let length = audio?.length {
                audioLengthInSeconds = length
            }
        }
    }
    
    /// Audio file (Local) URL, which is the one to be saved/played/uploaded and calculate audio length
    var audioURL: URL {
        let audioFilename = getDocumentsDirectory()
        let audioURL = URL(fileURLWithPath: audioFilename).appendingPathComponent("tmp.m4a")
        return audioURL
    }
    
    fileprivate var recordingSession = AVAudioSession.sharedInstance()
    fileprivate var audioRecorder: AVAudioRecorder!
    fileprivate var player: AVPlayer?
    
    /// Audio item (Remote) to be played
    fileprivate var playerItem: AVPlayerItem? {
        let item: AVPlayerItem?
        if let audioURL = URL(string: audioURLString) {
            item = AVPlayerItem(url: audioURL)
        } else {
            DLog("Nothing to play with url = \(audioURLString)")
            item = nil
        }
        return item
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var recordButton: UIButton!
    
    // MARK: UI updates
    
    override func updateView() {
        super.updateView()
        text = audioLengthInSeconds.audioLengthShortStringFromSeconds
        recordButton.isHidden = playOnly
    }
    
    // MARK: IBActions
    
    @IBAction func tappedPlayButton() {
        guard let playerItem = playerItem else {
            DLog("Nothing to play with url = \(audioURLString)")
            return
        }
        startedPlayAudio()
        player = AVPlayer(playerItem: playerItem)
        player?.play()
    }
    
    @IBAction func tappedRecordButton() {
        DLog("releasing record button")
        finishRecording(success: true)
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
    }
    
    @IBAction func touchDownRecordButton() {
        DLog("pressing record button")
        if audioRecorder == nil {
            do {
                try recordingSession.setCategory(AVAudioSessionCategoryRecord)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
            startRecording()
        }
    }
    
    // MARK: Recorder
    
    fileprivate func startRecording() {
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100.0,
            AVNumberOfChannelsKey: 2 as NSNumber,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
            AVEncoderBitRateKey : 320000
        ] as [String : Any]
        do {
            audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            viewController?.displayRecordingAnimationView()
//            recordButton.setImage(UIImage(named: ImageName.stop), forState: .Normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    fileprivate func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        viewController?.removeRecordingAnimationView()
        if success {
            audioLengthInSeconds = audioLengthInSeconds(audioURL)
            uploadAudio()
        } else {
            viewController?.presentAlertViewController("录音失败，请重试")
        }
    }
    
    // MARK: Helper methods
    
    fileprivate func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    /// Audio length in seconds
    fileprivate func audioLengthInSeconds(_ audioFileURL: URL) -> Int {
        let asset = AVURLAsset(url: audioFileURL, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        return Int(audioDurationSeconds)
    }
    
    func startedPlayAudio() {
        imageView.animationImages = AudioClipCellView.audioAnimationImages
        imageView.animationDuration = 2
        imageView.startAnimating()
        recordButton.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(AudioClipCellView.finishedPlayAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    func finishedPlayAudio() {
        imageView.stopAnimating()
        recordButton.isEnabled = true
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    // MARK: Service Call
    
    fileprivate func uploadAudio() {
        guard let
            promise = promise,
            let promiseType = PromiseType(rawValue: String(_promiseType)),
            let promisePersonType = PromisePersonType(rawValue: String(_promisePersonType))
        else { return }
        recordButton.isEnabled = false
        activityIndicator.startAnimating()
        PromiseManager.uploadPromiseAudio(promise.id,
                                          promiseType: promiseType,
                                          promisePersonType: promisePersonType,
                                          fileURL: audioURL,
                                          audioDurationSeconds: audioLengthInSeconds(audioURL),
                                          success: { [weak self] promise in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.recordButton.isEnabled = true
                                            strongSelf.promise = promise
                                            // TODO: apply indexOfAudios for familyMeetingAudios
                                            strongSelf.audio = promise.audioOfPromiseType(promiseType,
                                                andPromisePersonType: promisePersonType,
                                                indexOfAudios: nil)
                                            strongSelf.viewController?.promise = promise
                                            strongSelf.viewController?.presentAlertViewController("录音上传成功")
                                        },
                                          failure: { [weak self] error in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.recordButton.isEnabled = true
                                            strongSelf.viewController?.presentAlertViewController(error?.localizedFailureReason)
                                        })
    }
    
}

// MARK: AVAudioRecorderDelegate

extension AudioClipCellView: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}
