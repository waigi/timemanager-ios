//
//  ClockView.swift
//  timemanager
//
//  Created by Can on 4/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class ClockView: NibView {

    @IBOutlet weak var handlerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var clock: SmileClockContainerView!
    @IBOutlet weak var longPressGestureRecognizer: UILongPressGestureRecognizer!
    
    @IBInspectable var handlerHeight: Int = 60 {
        didSet {
            setNeedsUpdateView()
        }
    }
    @IBInspectable var clockData: SmileTimeZoneData = SmileTimeZoneData(timeZone: TimeZone.current) {
        didSet {
            setNeedsUpdateView()
        }
    }
    /// From 0:00 of today, how many seconds have elapsed till now
    var secondsElapsedToday: Int {
        return clock.hour * 3600 + clock.minute * 60 + clock.second
    }
    /// Percentage of time elapsed today
    var percentageOfTimeElapsedToday: Double {
        return Double(secondsElapsedToday) / (3600 * 24)
    }
    
    override func updateView() {
        super.updateView()
        handlerHeightConstraint.constant = CGFloat(handlerHeight)
        clock.hour = clockData.hour
        clock.minute = clockData.minute
        clock.second = clockData.second
        clock.updateClockView()
    }

}
