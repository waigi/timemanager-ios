//
//  DividerCellView.swift
//  timemanager
//
//  Created by Can on 22/06/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class DividerCellView: NibCellView {

    enum ScaleType: Int {
        case short = 0  // long scale
        case long = 1   // short scale
    }
    
    enum DividerCellType: Int {
        case normal = 0 // normal middle divider
        case left = 1   // most left hand side divider
        case right = 2  // most right hand side divider
    }
    
    var scaleType: ScaleType = .short { didSet { setNeedsUpdateView() } }
    var dividerCellType: DividerCellType = .normal { didSet { setNeedsUpdateView() } }
    
    @IBInspectable var scaleType_: Int {
        get { return scaleType.rawValue }
        set { scaleType = ScaleType(rawValue: newValue) ?? .short }
    }
    @IBInspectable var dividerCellType_: Int {
        get { return dividerCellType.rawValue }
        set { dividerCellType = DividerCellType(rawValue: newValue) ?? .normal }
    }
    @IBInspectable var showLabel: Bool = false {
        didSet {
            setNeedsUpdateView()
        }
    }
    @IBOutlet weak var dividerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dividerLabel: UILabel!
    @IBOutlet weak var horizontalLeftHandView: UIView!
    @IBOutlet weak var horizontalRightHandView: UIView!
    @IBOutlet weak var verticalLeftView: UIView!
    @IBOutlet weak var verticalRightView: UIView!
    @IBOutlet weak var bottomLineLeftView: UIView!
    @IBOutlet weak var bottomLineRightView: UIView!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!

    override func updateView() {
        super.updateView()
        switch scaleType {
        case .short:
            dividerHeightConstraint.constant = 4
        case .long:
            dividerHeightConstraint.constant = 6
        }
        switch dividerCellType {
        case .normal:
            horizontalLeftHandView.isHidden = false
            horizontalRightHandView.isHidden = false
            verticalLeftView.isHidden = true
            verticalRightView.isHidden = true
            bottomLineLeftView.isHidden = false
            bottomLineRightView.isHidden = false
            leftImageView.isHidden = true
            rightImageView.isHidden = true
        case .left:
            horizontalLeftHandView.isHidden = true
            horizontalRightHandView.isHidden = false
            verticalLeftView.isHidden = true
            verticalRightView.isHidden = true
            bottomLineLeftView.isHidden = true
            bottomLineRightView.isHidden = false
            leftImageView.isHidden = false
            rightImageView.isHidden = true
        case .right:
            horizontalLeftHandView.isHidden = false
            horizontalRightHandView.isHidden = true
            verticalLeftView.isHidden = true
            verticalRightView.isHidden = true
            bottomLineLeftView.isHidden = false
            bottomLineRightView.isHidden = true
            leftImageView.isHidden = true
            rightImageView.isHidden = false
        }
        dividerLabel.isHidden = !showLabel
    }

}
