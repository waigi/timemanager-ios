//
//  AudioRecordingCellView.swift
//  timemanager
//
//  Created by Can on 28/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class AudioRecordingCellView: NibCellView {

    @IBOutlet weak var imageView: UIImageView!

    func playAnimation() {
        imageView.animationImages = AudioRecordingCellView.audioRecordingAnimationImages
        imageView.animationDuration = 3
        imageView.startAnimating()
    }
    
    func stopAnimation() {
        imageView.stopAnimating()
    }
}
