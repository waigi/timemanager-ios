//
//  UIView+CDM.swift
//  timemanager
//
//  Created by Can on 28/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension UIView {

    var superviewCollectionCell: UICollectionViewCell? {
        var cell: UICollectionViewCell?
        var view: UIView? = self
        while view != nil {
            if let tryCell = view as? UICollectionViewCell {
                cell = tryCell
                break
            } else {
                view = view?.superview
            }
        }
        return cell;
    }
    
}
