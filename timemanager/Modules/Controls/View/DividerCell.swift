//
//  DividerCell.swift
//  timemanager
//
//  Created by Can on 29/06/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class DividerCell: UICollectionViewCell {

    @IBOutlet weak var dividerCellView: DividerCellView!
    
}
