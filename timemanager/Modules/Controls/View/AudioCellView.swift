//
//  AudioCellView.swift
//  timemanager
//
//  Created by Can on 28/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class AudioCellView: NibCellView {

    @IBInspectable var audioLengthInSeconds: Int = 0 {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    override func updateView() {
        super.updateView()
        textLabel?.text = audioLengthInSeconds.audioLengthLongStringFromSeconds
    }
    
}
