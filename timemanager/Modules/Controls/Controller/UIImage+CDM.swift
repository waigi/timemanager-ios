//
//  UIImage+CDM.swift
//  timemanager
//
//  Created by Can on 31/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension UIImage {
    /// Compressed image
    var uncompressedPNGData: Data      { return UIImagePNGRepresentation(self)!        }
    var highestQualityJPEGNSData: Data { return UIImageJPEGRepresentation(self, 1.0)!  }
    var highQualityJPEGNSData: Data    { return UIImageJPEGRepresentation(self, 0.75)! }
    var mediumQualityJPEGNSData: Data  { return UIImageJPEGRepresentation(self, 0.5)!  }
    var lowQualityJPEGNSData: Data     { return UIImageJPEGRepresentation(self, 0.25)! }
    var lowestQualityJPEGNSData:Data   { return UIImageJPEGRepresentation(self, 0.0)!  }

    /// Resize image with percentage
    func resizeWith(percentage: CGFloat) -> UIImage? {
        let width = CGFloat(size.width * percentage)
        let height = CGFloat(size.height * percentage)
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    /// Resize image with specified width, keep ratio
    func resizeWith(width: CGFloat) -> UIImage? {
        let height = CGFloat(ceil(width/size.width * size.height))
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
//        
//        let compressionQuality: CGFloat = 0.3
//        let height = CGFloat(ceil(width/size.width * size.height))
//        let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
//        UIGraphicsBeginImageContext(rect.size)
//        self.drawInRect(rect)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        guard let imageData = UIImageJPEGRepresentation(image, compressionQuality) else { return nil }
//        UIGraphicsEndImageContext()
//        return UIImage(data: imageData)
    }
    
    /// File size of the image in Byte
    func fileSize() -> Int {
        let imgData: Data = NSData(data: UIImagePNGRepresentation(self)!) as Data
        let imageSize: Int = imgData.count
        return imageSize
    }
    
    /// File size of the image in KB
    func fileSizeInKB() -> Double {
        return Double(fileSize()) / 1024.0
    }
}
