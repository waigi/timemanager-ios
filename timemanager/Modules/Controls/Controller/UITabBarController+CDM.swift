//
//  UITabBarController+CDM.swift
//  timemanager
//
//  Created by Can on 31/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

/// Only allow Landscape orientation
extension UITabBarController {
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .landscape
    }
}
