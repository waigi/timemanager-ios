//
//  NonRotatingUIImagePickerController.swift
//  timemanager
//
//  Created by Can on 31/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

/// Use this class to show ImagePicker in landscape
class LandscapeUIImagePickerController: UIImagePickerController {
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        return .Landscape
//    }
}