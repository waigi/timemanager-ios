//
//  SimpleWebViewController.swift
//  timemanager
//
//  Created by Can on 11/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class SimpleWebViewController: UIViewController {
    
    var url: URL?
    var fileName: String?
    @IBInspectable var screenTitle: String?
    /// Use this flag to avoid multiple calls to UIWebViewDelegate methods due to multiple frames of the web page.
    fileprivate var activityIndicatorShowed = false
        
    @IBInspectable var urlString: String = "" {
        didSet {
            url = URL(string: urlString)
        }
    }
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let screenTitle = screenTitle {
            navigationItem.title = screenTitle
        }
        webView.delegate = self
        if let filePath = Bundle.main.path(forResource: fileName, ofType: nil) {
            let request = URLRequest(url: URL(fileURLWithPath: filePath))
            webView.loadRequest(request)
        } else if let url = url {
            webView.loadRequest(URLRequest(url: url))
        }
    }
    
}

extension SimpleWebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        guard !activityIndicatorShowed else { return }
        activityIndicatorShowed = true
        if let activityIndicator = activityIndicator {
            activityIndicator.startAnimating()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let activityIndicator = activityIndicator {
            activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if let activityIndicator = activityIndicator {
            activityIndicator.stopAnimating()
        }
    }
    
}
