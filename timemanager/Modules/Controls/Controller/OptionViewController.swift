//
//  OptionViewController.swift
//  timemanager
//
//  Created by Can on 3/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol OptionDelegate {
    /// Pass selected option to delegate
    func selectedOption(_ option: AnyObject)
    /// Clear all flags if there is any
    func clearFlags()
}

class OptionViewController: UIViewController {
    
    fileprivate struct CellIdentifier {
        static let option = "option"
    }

    /// From which index to show the options
    var startingIndex = 0
    /// Options to view and select
    var options = [AnyObject]()
    /// Delegate to be called after selection
    var delegate: OptionDelegate?
    
    fileprivate let translationTransitioningController = TranslationTransitioningController()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var text2LabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var overlayTapGestureRecognizer: UITapGestureRecognizer!
    
    // MARK: - Init
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        translationTransitioningController.backdropColor = UIColor.white.withAlphaComponent(0.75)
        translationTransitioningController.direction = .up
        transitioningDelegate = translationTransitioningController
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    // MARK: Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if options.count > 0 {
            collectionView.selectItem(at: IndexPath.init(item: startingIndex, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        }
    }
    
    // MARK: IBAction
    
    @IBAction func dismissViewController(_ sender: AnyObject) {
        dismiss(sender)
        if !(sender is UICollectionView) {
            // 只有点击空白处关闭时才清除
            delegate?.clearFlags()
        }
    }
    
}

extension OptionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let option = options[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.option, for: indexPath) as! OptionCell
        cell.text1Label.text = option.description
        if indexPath.item == startingIndex {
            cell.containerView.backgroundColor = UIColor.orange
            cell.text1Label.textColor = UIColor.white
            cell.text2Label.textColor = UIColor.white
        } else {
            cell.containerView.backgroundColor = UIColor.white
            cell.text1Label.textColor = UIColor.black
            cell.text2Label.textColor = UIColor.black
        }
        return cell
    }
}

extension OptionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let option = options[indexPath.item]
        dismissViewController(collectionView)
        delegate?.selectedOption(option)
    }
}
