//
//  Photo.swift
//  timemanager
//
//  Created by Can on 15/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Photo: Hashable, Equatable {
    
    /// 照片Id
    var id: Int
    /// 照片URL String
    var urlString: String
    /// 照片位置X
    var positionX: Int
    /// 照片位置Y
    var positionY: Int
    
    /// 录音URL
    var url: URL? {
        return URL(string: urlString)
    }
    
    init?(json: JSON) {
        guard let urlString = json["urlString"].string else { return nil }
        self.urlString = urlString
        id = json["id"].intValue
        positionX = json["positionX"].intValue
        positionY = json["positionY"].intValue
    }
    
    var dictionary: [String: AnyObject] {
        return ["id": id as AnyObject,
                "urlString": urlString as AnyObject,
                "positionX": positionX as AnyObject,
                "positionY": positionY as AnyObject]
    }
    
    var json: String? {
        return JSON(dictionary).rawString()
    }
    
    var hashValue: Int { get { return id } }
    
}

func == (lhs: Photo, rhs: Photo) -> Bool {
    return lhs.id == rhs.id
}
