//
//  Video.swift
//  timemanager
//
//  Created by Can on 1/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Video: Hashable, Equatable {
    
    /// 视频Id
    var id: Int
    /// 视频URL String
    var urlString: String
    /// 视频预览图URL String
    var thumbnailURLString: String?
    /// 视频长度，单位为秒
    var length: Int?
    /// 录音位置X
    var positionX: Int
    /// 录音位置Y
    var positionY: Int
    
    /// 视频URL
    var url: URL? {
        return URL(string: urlString)
    }
    /// 预览图URL
    var thumbnailURL: URL? {
        guard let thumbnailURLString = thumbnailURLString else { return nil }
        return URL(string: thumbnailURLString)
    }
    
    init?(json: JSON) {
        id = json["id"].intValue
        guard let urlString = json["urlString"].string else { return nil }
        self.urlString = urlString
        thumbnailURLString = json["thumbnailURLString"].string
        length = json["videoLength"].intValue
        positionX = json["positionX"].intValue
        positionY = json["positionY"].intValue
    }
    
    var dictionary: [String: AnyObject] {
        return ["id": id as AnyObject,
                "urlString": urlString as AnyObject,
                "thumbnailURLString": thumbnailURLString as AnyObject? ?? "" as AnyObject,
                "videoLength": length as AnyObject? ?? 0 as AnyObject,
                "positionX": positionX as AnyObject,
                "positionY": positionY as AnyObject]
    }
    
    var hashValue: Int { get { return id } }
    
}

func == (lhs: Video, rhs: Video) -> Bool {
    return lhs.id == rhs.id
}
