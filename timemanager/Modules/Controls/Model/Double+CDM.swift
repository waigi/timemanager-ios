//
//  Double+CDM.swift
//  timemanager
//
//  Created by Can on 28/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension Double {
    var creditString: String {
        return NumberFormatter.creditFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}
