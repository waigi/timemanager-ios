//
//  Text.swift
//  timemanager
//
//  Created by Can on 15/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Text: Hashable, Equatable {
    
    /// 文本Id
    var id: Int
    /// 文本内容 String
    var text: String
    /// 文本位置X
    var positionX: Int
    /// 文本位置Y
    var positionY: Int
    
    init(text: String) {
        self.text = text
        id = 0
        positionX = 0
        positionY = 0
    }
    
    init?(json: JSON) {
        guard let id = json["id"].int else { return nil }
        self.id = id
        text = json["text"].stringValue
        positionX = json["positionX"].intValue
        positionY = json["positionY"].intValue
    }
    
    var dictionary: [String: AnyObject] {
        return ["id": id as AnyObject,
                "text": text as AnyObject,
                "positionX": positionX as AnyObject,
                "positionY": positionY as AnyObject]
    }
    
    var hashValue: Int { get { return id } }
    
}

func == (lhs: Text, rhs: Text) -> Bool {
    return lhs.id == rhs.id
}
