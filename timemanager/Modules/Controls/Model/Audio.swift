//
//  Audio.swift
//  timemanager
//
//  Created by Can on 7/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Audio: Hashable, Equatable {
    
    /// 录音Id
    var id: Int
    /// 录音URL String
    var urlString: String
    /// 录音长度，单位为秒
    var length: Int
    /// 录音位置X
    var positionX: Int
    /// 录音位置Y
    var positionY: Int
    
    /// 录音URL
    var url: URL? {
        return URL(string: urlString)
    }
    
    init?(json: JSON) {
        id = json["id"].intValue
        guard let urlString = json["audioURLString"].string else { return nil }
        self.urlString = urlString
        length = json["audioLength"].intValue
        positionX = json["positionX"].intValue
        positionY = json["positionY"].intValue
    }
    
    var dictionary: [String: AnyObject] {
        return ["id": id as AnyObject,
                "audioURLString": urlString as AnyObject,
                "audioLength": length as AnyObject,
                "positionX": positionX as AnyObject,
                "positionY": positionY as AnyObject]
    }
    
    var hashValue: Int { get { return id } }
    
}

func == (lhs: Audio, rhs: Audio) -> Bool {
    return lhs.id == rhs.id
}
