//
//  DLog.swift
//  timemanager
//
//  Created by Can on 24/1/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation

func DLog(_ items: Any...) {
    #if DEBUG
        print(items)
    #endif
}
