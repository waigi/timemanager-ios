//
//  UIColor+CDM.swift
//  timemanager
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension UIColor {
    // F report
//    var believable: UIColor {
//        return UIColor(red: /255.0, green: 255.0, blue: 255.0, alpha: <#T##CGFloat#>)
//    }
//    var responsible
//    var reliable
    
    /// #29793A
    class func textColor() -> UIColor {
        return UIColor(red: 41 / 255.0, green: 121 / 255.0, blue: 58 / 255.0, alpha: 1.0)
    }
}
