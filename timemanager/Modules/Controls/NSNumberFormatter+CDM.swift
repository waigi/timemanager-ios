//
//  NSNumberFormatter+CDM.swift
//  timemanager
//
//  Created by Can on 28/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    /// Format '1.1'
    @nonobjc static let creditFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        return formatter
    }()
    
}
