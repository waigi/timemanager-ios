//
//  CDMDate.swift
//  timemanager
//
//  Created by Can on 6/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftDate

class CDMDate: NSObject {
    
    var currentDate: Date
    
    var startDateOfWeek: Date? {
        let firstDayOfWeek = currentDate.startOf(component: .weekday)
        return firstDayOfWeek
//        return (currentDate.day - firstDayOfWeek).days.agoFromDate(currentDate)
    }
    
    var endDateOfWeek: Date? {
        let lastDayOfWeek = currentDate.endOf(component: .weekday)
        return lastDayOfWeek
//        return (lastDayOfWeek - currentDate.day).days.fromDate(currentDate)
    }
    
    /// 当前显示日期
    var currentDateDescription: String {
        return DateFormatter.standardDateFormatter.string(from: currentDate)
    }
    
    /// 当前星期起止时间，e.g.“16.11.7 ~ 16.11.1”
    override var description: String {
        guard let startDateOfWeek = startDateOfWeek,
            let endDateOfWeek = endDateOfWeek
            else { return "" }
        return "\(startDateOfWeek.toCDMSimpleString())\n ~ \n\(endDateOfWeek.toCDMSimpleString())"
    }
    
    init(date: Date) {
        currentDate = date
    }
}
