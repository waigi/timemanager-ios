//
//  UIView+Waigi.swift
//  timemanager
//
//  Created by Can on 2/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension UIView {
    @nonobjc static let audioAnimationImages =
        [
            UIImage(named: "animation_play_audio_0")!,
            UIImage(named: "animation_play_audio_1")!,
            UIImage(named: "animation_play_audio_2")!,
            UIImage(named: "animation_play_audio_3")!,
            UIImage(named: "animation_play_audio_4")!
        ]
    
    @nonobjc static let audioRecordingAnimationImages =
        [
            UIImage(named: "animation_record_audio_0")!,
            UIImage(named: "animation_record_audio_1")!,
            UIImage(named: "animation_record_audio_2")!,
            UIImage(named: "animation_record_audio_3")!,
            UIImage(named: "animation_record_audio_4")!,
            UIImage(named: "animation_record_audio_5")!,
            UIImage(named: "animation_record_audio_4")!,
            UIImage(named: "animation_record_audio_3")!,
            UIImage(named: "animation_record_audio_2")!,
            UIImage(named: "animation_record_audio_1")!
        ]
    
}
