//
//  Int+Waigi.swift
//  timemanager
//
//  Created by Can on 7/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension Int {
    
    /// 显示“XX分XX秒”
    var audioLengthLongStringFromSeconds: String {
        let minute = self / 60
        let second = self % 60
        return minute > 0 ? "\(minute)分\(second)秒" : "\(second)秒"
    }
    
    /// 显示"3'5""
    var audioLengthShortStringFromSeconds: String {
        let minute = self / 60
        let second = self % 60
        return minute > 0 ? "\(minute)'\(second)\"" : "\(second)\""
    }
    
    /// Get Random number
    static func randomIntFrom(_ start: Int, to end: Int) -> Int {
        var a = start
        var b = end
        // swap to prevent negative integer crashes
        if a > b {
            swap(&a, &b)
        }
        return Int(arc4random_uniform(UInt32(b - a + 1))) + a
    }

}
