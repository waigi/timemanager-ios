//
//  DetailCreditViewController.swift
//  timemanager
//
//  Created by Can on 8/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class DetailCreditViewController: UIViewController {
    
    let cellIdentifiers = ["f", "i", "k", "e", "a"]
    var credit: Credit!
    var delegate: CreditDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var swipeUpGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet var swipeDownGestureRecognizer: UISwipeGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeUpGestureRecognizer.addTarget(self, action: #selector(DetailCreditViewController.swipeUpAndDown(_:)))
        swipeDownGestureRecognizer.addTarget(self, action: #selector(DetailCreditViewController.swipeUpAndDown(_:)))
    }
    
    @IBAction func tappedDetailRankButton(_ button: UIButton) {
        if let detailRankView = button.superview as? DetailRankView {
            detailRankView.increaseRank()
            let detailCreditType = detailRankView.tag
            if let cell = detailRankView.superviewCollectionCell,
                let index = collectionView.indexPath(for: cell)?.item,
                let creditType = Credit.CreditType(rawValue: index)
            {
                credit.detailCreditOf(creditType, detailCreditType: detailCreditType).rank = detailRankView.rank
            }
        }
    }
    
    func swipeUpAndDown(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .ended {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
    /// Get all detailCredit values and update them into the Credit
    private func save() {
        Credit.CreditType.values.forEach { (creditType) in
            switch creditType {
            case .f:
                if let fCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? FCollectionViewCell {
                    for (index, view) in fCell.optionsView.subviews.enumerate() {
                        let detailCredit = credit.detailCreditOf(.f, detailCreditType: index)
                        if let detailRankView = view as? DetailRankView {
                            detailCredit.rank = detailRankView.rank
                        }
                    }
                }
            case .i:
                if let iCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? ICollectionViewCell {
                    for (index, view) in iCell.optionsView.subviews.enumerate() {
                        let detailCredit = credit.detailCreditOf(.i, detailCreditType: index)
                        if let detailRankView = view as? DetailRankView {
                            detailCredit.rank = detailRankView.rank
                        }
                    }
                }
            case .k:
                if let kCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? KCollectionViewCell {
                    for (index, view) in kCell.optionsView.subviews.enumerate() {
                        let detailCredit = credit.detailCreditOf(.k, detailCreditType: index)
                        if let detailRankView = view as? DetailRankView {
                            detailCredit.rank = detailRankView.rank
                        }
                    }
                }
            case .e:
                if let eCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? ECollectionViewCell {
                    for (index, view) in eCell.optionsView.subviews.enumerate() {
                        let detailCredit = credit.detailCreditOf(.e, detailCreditType: index)
                        if let detailRankView = view as? DetailRankView {
                            detailCredit.rank = detailRankView.rank
                        }
                    }
                }
            case .a:
                if let aCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? ACollectionViewCell {
                    for (index, view) in aCell.optionsView.subviews.enumerate() {
                        let detailCredit = credit.detailCreditOf(.a, detailCreditType: index)
                        if let detailRankView = view as? DetailRankView {
                            detailCredit.rank = detailRankView.rank
                        }
                    }
                }
            }
        }
        delegate?.update(credit)
    }
 */

}

extension DetailCreditViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifiers[indexPath.item], for: indexPath)
        switch indexPath.item {
        case 0:
            let fCell = cell as! FCollectionViewCell
            for (index, view) in fCell.optionsView.subviews.enumerated() {
                let detailCredit = credit.detailCreditOf(.f, detailCreditType: index)
                if let detailRankView = view as? DetailRankView {
                    detailRankView.rank = detailCredit.rank
                }
            }
        case 1:
            let iCell = cell as! ICollectionViewCell
            for (index, view) in iCell.optionsView.subviews.enumerated() {
                let detailCredit = credit.detailCreditOf(.i, detailCreditType: index)
                if let detailRankView = view as? DetailRankView {
                    detailRankView.rank = detailCredit.rank
                }
            }
        case 2:
            let kCell = cell as! KCollectionViewCell
            for (index, view) in kCell.optionsView.subviews.enumerated() {
                let detailCredit = credit.detailCreditOf(.k, detailCreditType: index)
                if let detailRankView = view as? DetailRankView {
                    detailRankView.rank = detailCredit.rank
                }
            }
        case 3:
            let eCell = cell as! ECollectionViewCell
            for (index, view) in eCell.optionsView.subviews.enumerated() {
                let detailCredit = credit.detailCreditOf(.e, detailCreditType: index)
                if let detailRankView = view as? DetailRankView {
                    detailRankView.rank = detailCredit.rank
                }
            }
        case 4:
            let aCell = cell as! ACollectionViewCell
            for (index, view) in aCell.optionsView.subviews.enumerated() {
                let detailCredit = credit.detailCreditOf(.a, detailCreditType: index)
                if let detailRankView = view as? DetailRankView {
                    detailRankView.rank = detailCredit.rank
                }
            }
            
        default: break
        }
        return cell
    }
}
