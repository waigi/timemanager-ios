//
//  BriefCreditViewController.swift
//  timemanager
//
//  Created by Can on 8/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol CreditDelegate {
    func update(_ credit: Credit)
}

class BriefCreditViewController: UITableViewController {
    
    fileprivate struct SegueIdentifier {
        static let detail = "detail"
    }

    var task: Task!
    var credit: Credit?
    var taskDelegate: TaskDelegate?
    var selectedEvaluactionType: Credit.EvaluationType = .selfEvaluation
    
    /// Weight options
    fileprivate let weights = Array(1...9)
    
    // MARK: IBOutlets
    
    @IBOutlet weak var rankView1: RankView!
    @IBOutlet weak var rankView2: RankView!
    @IBOutlet weak var rankView3: RankView!
    @IBOutlet weak var rankView4: RankView!
    @IBOutlet weak var rankView5: RankView!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var weightPickerView: UIPickerView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var evaluationContainerView: UIView!
    @IBOutlet weak var selfButton: UIButton!
    @IBOutlet weak var guardianButton: UIButton!
    @IBOutlet weak var teacherButton: UIButton!
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BriefCreditViewController.hideKeyboard))
        view.addGestureRecognizer(tapGestureRecognizer)
        // add border to view
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 40.0 / 255.0, green: 122.0 / 255.0, blue: 57.0 / 255.0, alpha: 1.0).cgColor
        view.layer.cornerRadius = 5.0
        if AppUserInfo.account.isStudent {
            selfButton.isEnabled = false
            guardianButton.isEnabled = true
            teacherButton.isEnabled = true
            selfButton.isHidden = false
            guardianButton.isHidden = true
            teacherButton.isHidden = true
            selectedEvaluactionType = .selfEvaluation
        } else if AppUserInfo.account.isGuardian {
            selfButton.isEnabled = true
            guardianButton.isEnabled = false
            teacherButton.isEnabled = true
            selfButton.isHidden = false
            guardianButton.isHidden = false
            teacherButton.isHidden = true
            selectedEvaluactionType = .guardian
        } else if AppUserInfo.account.isTeacher {
            selfButton.isEnabled = true
            guardianButton.isEnabled = true
            teacherButton.isEnabled = false
            selfButton.isHidden = false
            guardianButton.isHidden = false
            teacherButton.isHidden = false
            selectedEvaluactionType = .teacher
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchCredit()
    }
    
    // MARK: IBActions
    
    @IBAction func tappedSaveButton() {
        if let credit = credit {
            credit.fTotal = rankView1.rank
            credit.iTotal = rankView2.rank
            credit.kTotal = rankView3.rank
            credit.eTotal = rankView4.rank
            credit.aTotal = rankView5.rank
            credit.review = reviewTextView.text
            credit.weight = weights[weightPickerView.selectedRow(inComponent: 0)]
//            self.dismiss(animated: true, completion: ({ self.saveCredit() }))
            saveCredit()
        } else {
            dismiss(nil)
        }
    }
    
    @IBAction func tappedEvaluationButton(_ button: UIButton) {
        // update button state
        evaluationContainerView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        if let evaluationType = Credit.EvaluationType(rawValue: button.tag) {
            selectedEvaluactionType = evaluationType
        }
        fetchCredit()
    }
    
    // MARK: UI
    
    func hideKeyboard(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            reviewTextView.resignFirstResponder()
        }
    }
    
    fileprivate func updateUI() {
        guard let credit = credit else { return }
        // set credit values to view
        rankView1.rank = credit.fTotal
        rankView2.rank = credit.iTotal
        rankView3.rank = credit.kTotal
        rankView4.rank = credit.eTotal
        rankView5.rank = credit.aTotal
        reviewTextView.text = credit.review
        weightPickerView.selectRow(weights.index(of: credit.weight) ?? 1, inComponent: 0, animated: true)
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should: Bool = true
        if identifier == SegueIdentifier.detail {
            should = (credit != nil)
            should = AppUserInfo.account.isTeacherSupervisor
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.detail,
            let detailCreditViewController = segue.destination as? DetailCreditViewController
        {
            detailCreditViewController.credit = credit
            detailCreditViewController.delegate = self
        }
    }
    
    // MARK: Service
    
    fileprivate func saveCredit() {
        guard let credit = credit else { return }
        CreditManager.update(
            credit,
            success: { [weak self] (task) in
                guard let strongSelf = self else { return }
                strongSelf.task = task
                strongSelf.taskDelegate?.updateTask(strongSelf.task)
                strongSelf.dismiss(nil)
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    
    fileprivate func fetchCredit() {
        activityIndicator.startAnimating()
        CreditManager.fetchCreditWith(
            task.id,
            and: selectedEvaluactionType,
            success: { [weak self] (credit) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                // if there is no credit to be loaded, create a new one
                strongSelf.credit = credit ?? Credit(taskId: strongSelf.task.id, evaluationType: strongSelf.selectedEvaluactionType)
                strongSelf.updateUI()
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }

}

extension BriefCreditViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return weights.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = "\(weights[row])"
        pickerLabel.textColor = UIColor(red: 34.0 / 255.0, green: 118.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
        pickerLabel.font = UIFont(name: "System", size: 10)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
}

// MARK: UITableViewDataSource, UITableViewDelegate
extension BriefCreditViewController {
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: CreditDelegate
extension BriefCreditViewController: CreditDelegate {
    func update(_ credit: Credit) {
        self.credit = credit
    }
}
