//
//  DetailRankView.swift
//  timemanager
//
//  Created by Can on 9/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class DetailRankView: NibCellView {
    
    // allowed rank 0...5
    let maxRank = 6
    
    var rank: Int {
        get {
            return Int(detailText.flatMap() { $0 } ?? "0") ?? 0
        }
        set {
            detailText = String(newValue)
        }
    }
    
    var type: Int { return tag }

    /// Increase rank of current view, if over maxRank, start from 0 again
    func increaseRank() {
        rank = (rank + 1) % maxRank
    }

}
