//
//  RankView.swift
//  timemanager
//
//  Created by Can on 6/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class RankView: NibView {

    let starHollowImage = UIImage(named: "star hollow")
    let starSolidImage = UIImage(named: "star solid")
    
    @IBInspectable var rank: Int = 0 {
        didSet {
            for (index, button) in (subviews.flatMap{$0 as? UIButton}).enumerated() {
                button.setImage((rank > index) ? starSolidImage : starHollowImage, for: UIControlState())
            }
        }
    }
    
    @IBInspectable var rankType: Int = 0 {
        didSet {
            let image: UIImage
            switch rankType {
            case 0: image = UIImage(named: "F")!
            case 1: image = UIImage(named: "I")!
            case 2: image = UIImage(named: "K")!
            case 3: image = UIImage(named: "E")!
            case 4: image = UIImage(named: "A")!
            default: image = UIImage(named: "F")!
            }
            imageView.image = image
        }
    }

    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func tappedRank1Button() {
        rank = rank == 0 ? 1 : 0
    }
    
    @IBAction func tappedRank2Button() {
        rank = 2
    }
    
    @IBAction func tappedRank3Button() {
        rank = 3
    }
    
    @IBAction func tappedRank4Button() {
        rank = 4
    }
    
    @IBAction func tappedRank5Button() {
        rank = 5
    }
    
}
