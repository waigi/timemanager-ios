//
//  Credit.swift
//  timemanager
//
//  Created by Can on 6/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 一个Task的总积分
class Credit {
    
    /// 积分类别
    enum CreditType: Int {
        case f, i, k, e, a
        /// all possible values of the Type
        static var values: [CreditType] { return [.f, .i, .k, .e, .a] }
    }
    
    enum EvaluationType: Int {
        case selfEvaluation, guardian, teacher
    }
    
    /// 精评积分
    class DetailCredit {
        /// 精评类别0-8
        var type: Int
        /// 精评分数0-5
        var rank: Int = 0
        
        // MARK: Computed var
        /// JSON parameter for service calls
        var parameters: [String: AnyObject] {
            return [
                "type": type as AnyObject,
                "rank": rank as AnyObject
            ]
        }
        
        init(type: Int, rank: Int) {
            self.type = type
            self.rank = rank
        }
        
        init?(json: JSON) {
            guard let type = json["type"].int else { return nil }
            guard let rank = json["rank"].int else { return nil }
            self.type = type
            self.rank = rank
        }
    }
    
    // MARK: Vars
    
    /// 积分所属任务的ID
    var taskId: String
    /// FIKEA简评分
    var fTotal: Int = 0
    var iTotal: Int = 0
    var kTotal: Int = 0
    var eTotal: Int = 0
    var aTotal: Int = 0
    /// FIKEA精评分
    var fDetails = [DetailCredit]()
    var iDetails = [DetailCredit]()
    var kDetails = [DetailCredit]()
    var eDetails = [DetailCredit]()
    var aDetails = [DetailCredit]()
    /// 评价
    var review: String?
    /// 权重
    var weight: Int = 1
    /// Task所属日期
    var taskDate: Date = Date()
    /// 评分人，“自、亲、师”，对应server端的‘graderType’, 自=0，亲=1，师=2
    var evaluationType: EvaluationType
    /// 折扣率
    var discount: Double = 1.0
    
    // MARK: Computed var
    
    var parameters: [String: AnyObject] {
        var params: [String: AnyObject] = [
            "fTotal": fTotal as AnyObject,
            "iTotal": iTotal as AnyObject,
            "kTotal": kTotal as AnyObject,
            "eTotal": eTotal as AnyObject,
            "aTotal": aTotal as AnyObject,
            "review": (review as AnyObject? ?? "" as AnyObject),
            "weight": weight as AnyObject,
            "graderType": evaluationType.rawValue as AnyObject
        ]
        params["fDetails"] = fDetails.map({ $0.parameters }) as AnyObject
        params["iDetails"] = iDetails.map({ $0.parameters }) as AnyObject
        params["kDetails"] = kDetails.map({ $0.parameters }) as AnyObject
        params["eDetails"] = eDetails.map({ $0.parameters }) as AnyObject
        params["aDetails"] = aDetails.map({ $0.parameters }) as AnyObject
        let accountFamily: [String: AnyObject] = ["id": AppUserInfo.account.selectedStudentAccountFamily?.id as AnyObject? ?? "" as AnyObject]
        let task: [String: AnyObject] = [
                    "accountFamily": accountFamily as AnyObject,
                    "id": taskId as AnyObject,
                    "plannedStartTime": String(Int(taskDate.timeIntervalSince1970)) + "000" as AnyObject,
                    "discount": discount as AnyObject
                    ]
        params["task"] = task as AnyObject?
        return params
    }
    
    // MARK: init
    
    init(taskId: String, evaluationType: EvaluationType) {
        self.taskId = taskId
        self.evaluationType = evaluationType
    }
    
    init?(json: JSON) {
        guard let taskId = json["task"]["id"].string else { return nil }
        self.taskId = taskId
        guard let evaluationType = EvaluationType(rawValue: json["graderType"].intValue) else { return nil }
        self.evaluationType = evaluationType
        fTotal = json["fTotal"].intValue
        iTotal = json["iTotal"].intValue
        kTotal = json["kTotal"].intValue
        eTotal = json["eTotal"].intValue
        aTotal = json["aTotal"].intValue
        review = json["review"].stringValue
        weight = json["weight"].intValue
        for subjson in json["fDetails"].arrayValue {
            if let detailCredit = DetailCredit(json: subjson) {
                fDetails.append(detailCredit)
            }
        }
        for subjson in json["iDetails"].arrayValue {
            if let detailCredit = DetailCredit(json: subjson) {
                iDetails.append(detailCredit)
            }
        }
        for subjson in json["kDetails"].arrayValue {
            if let detailCredit = DetailCredit(json: subjson) {
                kDetails.append(detailCredit)
            }
        }
        for subjson in json["eDetails"].arrayValue {
            if let detailCredit = DetailCredit(json: subjson) {
                eDetails.append(detailCredit)
            }
        }
        for subjson in json["aDetails"].arrayValue {
            if let detailCredit = DetailCredit(json: subjson) {
                aDetails.append(detailCredit)
            }
        }
        if let timeInterval = json["taskDate"].double {
            taskDate = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
    }
    
    // MARK: Functions
    
    /// 根据精评类型和简评类型获取相应的精评对象，如果此对象未生成过，生成一个rank为0的新对象。
    func detailCreditOf(_ creditType: CreditType, detailCreditType: Int) -> DetailCredit {
        var detailCredit: DetailCredit?
        switch creditType {
        case .f:
            detailCredit = fDetails.filter({ (detailCredit) -> Bool in
                detailCredit.type == detailCreditType
            }).first
            if detailCredit == nil {
                detailCredit = DetailCredit(type: detailCreditType, rank: 0)
                fDetails.append(detailCredit!)
            }
        case .i:
            detailCredit = iDetails.filter({ (detailCredit) -> Bool in
                detailCredit.type == detailCreditType
            }).first
            if detailCredit == nil {
                detailCredit = DetailCredit(type: detailCreditType, rank: 0)
                iDetails.append(detailCredit!)
            }
        case .k:
            detailCredit = kDetails.filter({ (detailCredit) -> Bool in
                detailCredit.type == detailCreditType
            }).first
            if detailCredit == nil {
                detailCredit = DetailCredit(type: detailCreditType, rank: 0)
                kDetails.append(detailCredit!)
            }
        case .e:
            detailCredit = eDetails.filter({ (detailCredit) -> Bool in
                detailCredit.type == detailCreditType
            }).first
            if detailCredit == nil {
                detailCredit = DetailCredit(type: detailCreditType, rank: 0)
                eDetails.append(detailCredit!)
            }
        case .a:
            detailCredit = aDetails.filter({ (detailCredit) -> Bool in
                detailCredit.type == detailCreditType
            }).first
            if detailCredit == nil {
                detailCredit = DetailCredit(type: detailCreditType, rank: 0)
                aDetails.append(detailCredit!)
            }
        }
        return detailCredit!
    }
    
    /// 更新一个精评值
    func setDetailCreditWith(_ creditType: CreditType, detailCreditType: Int, detailCreditRank: Int) {
        detailCreditOf(creditType, detailCreditType: detailCreditType).rank = detailCreditRank
    }
    
}
