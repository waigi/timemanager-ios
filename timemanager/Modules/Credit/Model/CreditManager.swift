//
//  CreditManager.swift
//  timemanager
//
//  Created by Can on 16/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftDate

class CreditManager {
    
    fileprivate struct EndPoint {
        static let getCreditByTask = "api/credits/getByTaskIdAndGraderType"
        static let updateCredit = "api/credits/update"
        static let searchCredit = "api/credits/reportSearchCriteria"
    }
    
    /// 根据Task Id获取Credit
    static func fetchCreditWith(_ taskId: String, and evaluationType: Credit.EvaluationType, success: @escaping ((Credit?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["task_id": taskId as AnyObject, "grader_type": evaluationType.rawValue as AnyObject]
        DLog("fetchCreditWith parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.getCreditByTask,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched Credit = \(data)")
                    let json = JSON(data)
                    let credit = Credit(json: json)
                    success(credit)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
 
    /// 保存／创建／更新Credit中有内容的字段
    static func update(_ credit: Credit, success: @escaping ((Task?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = credit.parameters
        DLog("update parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.updateCredit,
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Credit = \(data)")
                    let json = JSON(data)
                    let credit = Credit(json: json)
                    let task = Task(json: json["task"])
                    task?.credit = credit
                    success(task)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据Task Id获取Credit
    static func searchCreditsInCycleWith(_ accountFamilyId: String, reportSearchCriteria: ReportSearchCriteria, success: @escaping (([CreditsInCycle]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = [
            "account_family_id": accountFamilyId as AnyObject,
            "cycle_days_number": reportSearchCriteria.cycleDaysNumber as AnyObject,
            "start_date": String(Int64(reportSearchCriteria.startDate.startOfDay.timeIntervalSince1970 * 1000)) as AnyObject,
            "end_date": String(Int64(reportSearchCriteria.endDate.endOfDay.timeIntervalSince1970 * 1000)) as AnyObject,
            "grader_type": reportSearchCriteria.evaluationType.rawValue as AnyObject]
        DLog("searchCreditsInCycleWith parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.searchCredit,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    var creditsInCycles = [CreditsInCycle]()
                    DLog("Searched creditsInCycles = \(data)")
                    if let data = response.result.value {
                        let json = JSON(data)
                        for (_, subjson): (String, JSON) in json {
                            if let creditsInCycle = CreditsInCycle(json: subjson) {
                                creditsInCycles.append(creditsInCycle)
                            }
                        }
                    }
                    success(creditsInCycles)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
}
