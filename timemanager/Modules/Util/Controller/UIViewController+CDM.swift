//
//  UIViewController+CDM.swift
//  timemanager
//
//  Created by Can on 29/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

extension UIViewController {

    func hasWechatInstalled() -> Bool {
        return false
    }
    
    /// Launch Wechat
    func launchWechat() {
        if let url = URL(string: "weixin://") {
            UIApplication.shared.openURL(url)
        }
    }
    
    func presentAlertViewController(_ errorString: String?) {
        presentAlertViewController(errorString, showCancel: false, handler: nil)
    }
    
    /// Show Alert View
    func presentAlertViewController(_ message: String?, showCancel: Bool, handler: ((Void) -> Void)?) {
        let alertViewController = UIAlertController(title: "", message: message ?? "", preferredStyle: .alert)
        
        let alertOKButton = UIAlertAction(title: "确认", style: UIAlertActionStyle.default) { _ in
            handler?()
        }
        alertViewController.addAction(alertOKButton)
        
        if showCancel {
            alertViewController.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
        }
        
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
        }
        
    }
    
    /// Dismiss View Controller
    @IBAction func dismiss(_ sender: AnyObject?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Display Options View Controller
    func displayOptionWithOptions(_ options: [AnyObject], startAt index: Int = 0, withDelegate delegate: OptionDelegate) {
        guard let optionViewController = UIStoryboard(name: "Options", bundle: nil).instantiateInitialViewController() as? OptionViewController else {
            return
        }
        optionViewController.options = options
        optionViewController.startingIndex = index
        optionViewController.delegate = delegate
        present(optionViewController, animated: true, completion: nil)
    }
    
    /// Display Recording animation
    func displayRecordingAnimationView() {
        let width: CGFloat = 130
        let frame = CGRect(x: (view.frame.width - width) / 2, y: (view.frame.height - width) / 2, width: width, height: width)
        let animationView = AudioRecordingCellView(frame: frame)
        view.addSubview(animationView)
        view.bringSubview(toFront: animationView)
        animationView.playAnimation()
    }
    
    /// Remove Recording animation
    func removeRecordingAnimationView() {
        for subview in view.subviews {
            if let animationView = subview as? AudioRecordingCellView {
                animationView.stopAnimation()
                animationView.removeFromSuperview()
                break
            }
        }
    }
    
    /// Used to get file path for Video/Audio/Photo
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first
        return documentsDirectory!
    }

    func randomFrameOnView(_ view: UIView, with width: CGFloat, and height: CGFloat) -> CGRect {
        let x = CGFloat(Int.randomIntFrom(0, to: Int(view.frame.width)))
        let y = CGFloat(Int.randomIntFrom(0, to: Int(view.frame.height)))
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func randomPositionOnView(_ view: UIView) -> (x: Int, y: Int) {
        let x = Int.randomIntFrom(0, to: Int(view.frame.width))
        let y = Int.randomIntFrom(0, to: Int(view.frame.height))
        return (x, y)
    }
    
}
