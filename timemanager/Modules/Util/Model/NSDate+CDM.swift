//
//  NSDate+CDM.swift
//  timemanager
//
//  Created by Can on 9/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftDate

extension Date {
    
    @nonobjc static let secondsOfADay = 86400
    @nonobjc static let minutesOfADay = 1440
    
    /// Get string with pattern '2016-07-10'
    func toCDMStandardString() -> String {
        return DateFormatter.standardDateFormatter.string(from: self)
    }
    
    /// Get string with pattern '16.7.10'
    func toCDMSimpleString() -> String {
        return DateFormatter.simpleDateFormatter.string(from: self)
    }
    
    var timeString: String {
        return DateFormatter.timeDateFormatter.string(from: self)
    }
}
