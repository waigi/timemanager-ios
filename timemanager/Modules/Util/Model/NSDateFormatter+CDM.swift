//
//  NSDateFormatter+CDM.swift
//  timemanager
//
//  Created by Can on 9/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /// Format '2016-11-05'
    @nonobjc static let standardDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    /// Format '16.11.5'
    @nonobjc static let simpleDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yy.MM.dd"
        return dateFormatter
    }()
    
    /// Format '3:30'
    @nonobjc static let timeDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"
        return dateFormatter
    }()
    
}
