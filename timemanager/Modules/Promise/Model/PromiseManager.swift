//
//  PromiseManager.swift
//  timemanager
//
//  Created by Can on 8/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftDate

class PromiseManager {
    
    fileprivate struct EndPoint {
        static let getPromises = "api/promises/familyPromises/"
        static let getPromiseByTask = "api/promises/byTask"
        static let getPromiseById = "/api/promises/"
        static let updatePromise = "api/promises/update/"
        static let generatePromise = "/api/promises/generatePromise/"
        static let upload = "api/promises/upload/"
        static let deletePromise = "/api/promises/"
        static let associateTaskWithPromise = "api/promises/taskEmbedPromise"
    }
    
    /// 根据accountFamilyId生成当前登录用户的Promise
    static func generatePromiseWithAccountFamiltyId(_ accountFamilyId: String, taskId: String?, success: @escaping ((Promise?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["task_id": taskId as AnyObject? ?? "" as AnyObject]
        Alamofire.request(
            Api.baseURL + EndPoint.generatePromise,
            method: .post,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Generated Promise = \(data)")
                    let json = JSON(data)
                    let promise = Promise(json: json)
                    success(promise)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 删除Promise
    static func deletePromise(_ promise: Promise, success: @escaping ((Bool) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.deletePromise + promise.id,
            method: .delete,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let _):
                    success(true)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据Task Id获取Promise
    static func fetchPromiseWithPromiseId(_ promiseId: String, success: @escaping ((Promise?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.getPromiseById + promiseId,
            method: .get,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched Promise = \(data)")
                    let json = JSON(data)
                    let promise = Promise(json: json)
                    success(promise)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据AccessToken获取当前登录用户所属的所有Promise
    static func fetchPromisesWithAccountFamiltyId(_ accountFamilyId: String, success: @escaping (([Promise]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["accountFamily_id": accountFamilyId as AnyObject]
        Alamofire.request(
            Api.baseURL + EndPoint.getPromises,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    var promises = [Promise]()
                    DLog("received promises = \(data)")
                    let json = JSON(data)
                    for (_, subjson):(String, JSON) in json {
                        if let promise = Promise(json: subjson) {
                            promises.append(promise)
                        }
                    }
                    success(promises)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 更新Promise中有内容的字段
    static func updatePromise(_ promise: Promise, success: @escaping ((Promise?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: [String: AnyObject] = ["id": promise.id as AnyObject]
        if let name = promise.name {
            parameters["promiseContent"] = name as AnyObject?
        }
        DLog("Original Promise = \(promise)")
        Alamofire.request(
            Api.baseURL + EndPoint.updatePromise,
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Promise = \(data)")
                    let json = JSON(data)
                    let promise = Promise(json: json)
                    success(promise)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    static func associateTaskWithPromise(_ taskId: String, promiseId: String, success: @escaping ((Promise?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["task_id": taskId as AnyObject, "promise_id": promiseId as AnyObject]
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.associateTaskWithPromise,
            method: .put,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Promise = \(data)")
                    let json = JSON(data)
                    let promise = Promise(json: json)
                    success(promise)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 上传音频
    static func uploadPromiseAudio(_ promiseId: String, promiseType: PromiseType, promisePersonType: PromisePersonType, fileURL: URL, audioDurationSeconds: Int, index: Int? = nil, success: @escaping ((Promise) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters = [
            "promise_id": promiseId,
            "promise_type": promiseType.rawValue,
            "promise_person_type": promisePersonType.rawValue,
            "audio_length": "\(audioDurationSeconds)"]
        if let index = index {
            parameters["position_x"] = String(index)
        }
        DLog("上传录音参数: \(parameters)")
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileURL, withName: "file")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
            to: Api.baseURL + EndPoint.upload,
            headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let
                            value = response.result.value,
                            let promise = Promise(json: JSON(value))
                        {
                            DLog("上传文件后Promise=\n\(value)")
                            success(promise)
                        } else {
                            failure(ErrorManager.errorWithMessage("上传音频失败，请重试"))
                        }
                    }
                case .failure(let encodingError):
                    DLog(encodingError)
                    failure(ErrorManager.errorWithMessage("上传音频失败，请检查网络"))
                }
        }
    }
    
}
