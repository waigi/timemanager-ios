//
//  Promise.swift
//  timemanager
//
//  Created by Can on 31/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON

enum PromiseStatus: Int {
    case notStarted = 0
    case started = 1
    case finished = 2
}

enum PromiseType: String {
    case content = "0"
    case personality = "1"
    case tag = "2"
    case resource = "3"
    case grounded = "4"
    case criticize = "5"
    case familyMeeting = "6"
}

enum PromisePersonType: String {
    case other = "0"
    case child = "1"
    case guardian = "2"
    case teacher = "3"
}

class Promise: NSObject {
    
    // MARK: Vars
    
    /// 承诺ID，从server端获取，正式生成Promise，返回生成的ID
    var id: String
    /// 承诺序号，根据每个Family Account独立编号
    var sequenceNumber: Int = 0
    /// 承诺事件名称
    var name: String?
    /// 承诺所属的Task
//    var task: Task?
    /// 承诺日期时间
    var promiseTime: Date
    /// 承诺录音状态
    var promiseStatus: PromiseStatus = .notStarted
    /// 承诺录音
    var promiseChildAudio: Audio?
    /// 承诺录音－家长
    var promiseGuardianAudio: Audio?
    /// 承诺录音－教师
    var promiseTeacherAudio: Audio?
    /// 人格诺录音状态
    var personalityPromiseStatus: PromiseStatus = .notStarted
    /// 人格诺录音－孩子
    var personalityChildAudio: Audio?
    /// 人格诺录音－家长
    var personalityGuardianAudio: Audio?
    /// 人格诺录音－教师
    var personalityTeacherAudio: Audio?
    /// 标签诺录音状态
    var tagPromiseStatus: PromiseStatus = .notStarted
    /// 标签诺录音－孩子
    var tagChildAudio: Audio?
    /// 标签诺录音－家长
    var tagGuardianAudio: Audio?
    /// 标签诺录音－教师
    var tagTeacherAudio: Audio?
    /// 资源诺录音状态
    var resourcePromiseStatus: PromiseStatus = .notStarted
    /// 资源诺录音－孩子
    var resourceChildAudio: Audio?
    /// 资源诺录音－家长
    var resourceGuardianAudio: Audio?
    /// 资源诺录音－教师
    var resourceTeacherAudio: Audio?
    /// 禁足诺录音状态
    var groundedPromiseStatus: PromiseStatus = .notStarted
    /// 禁足诺录音－孩子
    var groundedChildAudio: Audio?
    /// 禁足诺录音－家长
    var groundedGuardianAudio: Audio?
    /// 禁足诺录音－教师
    var groundedTeacherAudio: Audio?
    /// 训诫诺录音状态
    var criticizePromiseStatus: PromiseStatus = .notStarted
    /// 训诫诺录音－孩子
    var criticizeChildAudio: Audio?
    /// 训诫诺录音－家长
    var criticizeGuardianAudio: Audio?
    /// 训诫诺录音－教师
    var criticizeTeacherAudio: Audio?
    /// 家庭大会录音状态
    var familyMeetingPromiseStatus: PromiseStatus = .notStarted
    /// 家庭大会录音
    var familyMeetingAudios = [Audio]()
    /// F
    var fTags = [String]()
    /// I
    var iTags = [String]()
    /// K
    var kTags = [String]()
    /// E
    var eTags = [String]()
    /// A
    var aTags = [String]()
    /// 承诺开始时间，均由服务器生成
    var promiseStartTime: Date?
    /// 人格诺开始时间
    var personalityStartTime: Date?
    /// 标签诺开始时间
    var tagStartTime: Date?
    /// 资源诺开始时间
    var resourceStartTime: Date?
    /// 禁足诺开始时间
    var groundedStartTime: Date?
    /// 训诫诺开始时间
    var criticizeStartTime: Date?
    /// 家庭大会开始时间
    var familyMeetingStartTime: Date?
    
    // MARK: init
    
    init(id: String, promiseTime: Date) {
        self.id = id
        self.promiseTime = promiseTime
    }
    
    init?(json: JSON) {
        guard let id = json["id"].string else { return nil }
        self.id = id
        promiseTime = NSDate(timeIntervalSince1970: json["promiseTime"].doubleValue / 1000.0) as Date
        name = json["promiseContent"].string
        sequenceNumber = json["sequenceNumber"].intValue
//        task = Task(json: json["task"])
        promiseStatus = PromiseStatus(rawValue: json["promiseStatus"].intValue) ?? .notStarted
        promiseChildAudio = Audio(json: json["promiseAudio"])
        promiseGuardianAudio = Audio(json: json["promiseGuardianAudio"])
        promiseTeacherAudio = Audio(json: json["promiseTeacherAudio"])
        personalityPromiseStatus = PromiseStatus(rawValue: json["personalityPromiseStatus"].intValue) ?? .notStarted
        personalityChildAudio = Audio(json: json["personalityChildAudio"])
        personalityGuardianAudio = Audio(json: json["personalityGuardianAudio"])
        personalityTeacherAudio = Audio(json: json["personalityTeacherAudio"])
        tagPromiseStatus = PromiseStatus(rawValue: json["tagPromiseStatus"].intValue) ?? .notStarted
        tagChildAudio = Audio(json: json["tagChildAudio"])
        tagGuardianAudio = Audio(json: json["tagGuardianAudio"])
        tagTeacherAudio = Audio(json: json["tagTeacherAudio"])
        resourcePromiseStatus = PromiseStatus(rawValue: json["resourcePromiseStatus"].intValue) ?? .notStarted
        resourceChildAudio = Audio(json: json["resourceChildAudio"])
        resourceGuardianAudio = Audio(json: json["resourceGuardianAudio"])
        resourceTeacherAudio = Audio(json: json["resourceTeacherAudio"])
        groundedPromiseStatus = PromiseStatus(rawValue: json["groundedPromiseStatus"].intValue) ?? .notStarted
        groundedChildAudio = Audio(json: json["groundedChildAudio"])
        groundedGuardianAudio = Audio(json: json["groundedGuardianAudio"])
        groundedTeacherAudio = Audio(json: json["groundedTeacherAudio"])
        criticizePromiseStatus = PromiseStatus(rawValue: json["criticizePromiseStatus"].intValue) ?? .notStarted
        criticizeChildAudio = Audio(json: json["criticizeChildAudio"])
        criticizeGuardianAudio = Audio(json: json["criticizeGuardianAudio"])
        criticizeTeacherAudio = Audio(json: json["criticizeTeacherAudio"])
        familyMeetingPromiseStatus = PromiseStatus(rawValue: json["familyMeetingPromiseStatus"].intValue) ?? .notStarted
        if let jsons = json["familyMeetingAudios"].array {
            for json in jsons {
                if let audio = Audio(json: json) {
                    familyMeetingAudios.append(audio)
                }
            }
        }
        fTags = json["fTags"].arrayObject as? [String] ?? [String]()
        iTags = json["iTags"].arrayObject as? [String] ?? [String]()
        kTags = json["kTags"].arrayObject as? [String] ?? [String]()
        eTags = json["eTags"].arrayObject as? [String] ?? [String]()
        aTags = json["aTags"].arrayObject as? [String] ?? [String]()
        if let timeInterval = json["promiseStartTime"].double {
            promiseStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["personalityStartTime"].double {
            personalityStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["tagStartTime"].double {
            tagStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["resourceStartTime"].double {
            resourceStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["groundedStartTime"].double {
            groundedStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["criticizeStartTime"].double {
            criticizeStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
        if let timeInterval = json["familyMeetingStartTime"].double {
            familyMeetingStartTime = NSDate(timeIntervalSince1970: timeInterval / 1000.0) as Date
        }
    }
    
    // MARK: Functions
    
    /// 指定类型的Audio, indexOfAudios only applicable for FamilyMeetingAudios
    func audioOfPromiseType(_ promiseType: PromiseType, andPromisePersonType promisePersonType: PromisePersonType, indexOfAudios index: Int?) -> Audio? {
        var audio: Audio?
        switch promiseType {
        case .content:
            switch promisePersonType {
            case .child: audio = promiseChildAudio
            case .guardian: audio = promiseGuardianAudio
            case .teacher: audio = promiseTeacherAudio
            case .other: break
            }
        case .personality:
            switch promisePersonType {
            case .child: audio = personalityChildAudio
            case .guardian: audio = personalityGuardianAudio
            case .teacher: audio = personalityTeacherAudio
            case .other: break
            }
        case .tag:
            switch promisePersonType {
            case .child: audio = tagChildAudio
            case .guardian: audio = tagGuardianAudio
            case .teacher: audio = tagTeacherAudio
            case .other: break
            }
        case .resource:
            switch promisePersonType {
            case .child: audio = resourceChildAudio
            case .guardian: audio = resourceGuardianAudio
            case .teacher: audio = resourceTeacherAudio
            case .other: break
            }
        case .grounded:
            switch promisePersonType {
            case .child: audio = groundedChildAudio
            case .guardian: audio = groundedGuardianAudio
            case .teacher: audio = groundedTeacherAudio
            case .other: break
            }
        case .criticize:
            switch promisePersonType {
            case .child: audio = criticizeChildAudio
            case .guardian: audio = criticizeGuardianAudio
            case .teacher: audio = criticizeTeacherAudio
            case .other: break
            }
        case .familyMeeting:
            audio = familyMeetingAudios.filter({ (familyMeetingAudio) -> Bool in
                familyMeetingAudio.positionX == index
            }).first
//            for familyMeetingAudio in familyMeetingAudios {
//                if familyMeetingAudio.positionX == index {
//                    audio = familyMeetingAudio
//                    break
//                }
//            }
        }
        return audio
    }
    
    /// Promise的描述
    override var description: String {
        return "\(sequenceNumber) \(name ?? "")"
    }
    
}
