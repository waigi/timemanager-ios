//
//  Promise2ViewController.swift
//  timemanager
//
//  Created by Can on 20/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class Promise2ViewController: UIViewController {
    
    fileprivate struct Constant {
        static let finishedPromiseImage = UIImage(named: "finished promise")
        static let currentPromiseImage = UIImage(named: "current promise")
        static let futurePromiseImage = UIImage(named: "future promise")
    }
    
    fileprivate struct CellIdentifier {
        static let promiseItem = "promiseItemCell"
        static let meeting = "promiseMeetingCell"
    }
    
    fileprivate struct SegueIdentifier {
        static let familyActivity = "familyActivity"
        static let report = "report"
    }
    
    // MARK: Variables
    
    var promise: Promise?
    fileprivate var lastTappedButton: UIButton?
    
    /// 当前正在点击按钮的FamilyActivity
    fileprivate var selectedFamilyActivity: FamilyActivity?
    
    // MARK: IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var promiseButton: UIButton?
    @IBOutlet weak var personalityButton: UIButton?
    @IBOutlet weak var tagButton: UIButton?
    @IBOutlet weak var resourceButton: UIButton?
    @IBOutlet weak var groundedButton: UIButton?
    @IBOutlet weak var criticizeButton: UIButton?
    @IBOutlet weak var meetingButton: UIButton?
    @IBOutlet weak var nameTextField: UITextField?
    @IBOutlet weak var sequenceNumberLabel: UILabel?
    @IBOutlet weak var promiseLabel: UILabel?
    @IBOutlet weak var personalityLabel: UILabel?
    @IBOutlet weak var tagLabel: UILabel?
    @IBOutlet weak var resourceLabel: UILabel?
    @IBOutlet weak var groundedLabel: UILabel?
    @IBOutlet weak var criticizeLabel: UILabel?
    @IBOutlet weak var meetingLabel: UILabel?
    @IBOutlet weak var promiseTimeLabel: UILabel?
    @IBOutlet weak var personalityTimeLabel: UILabel?
    @IBOutlet weak var tagTimeLabel: UILabel?
    @IBOutlet weak var resourceTimeLabel: UILabel?
    @IBOutlet weak var groundedTimeLabel: UILabel?
    @IBOutlet weak var criticizeTimeLabel: UILabel?
    @IBOutlet weak var meetingTimeLabel: UILabel?
    
    // MARK: Lift Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    /// Update UI based on passed in Promise
    fileprivate func updateUI() {
        updateButtons()
        updateLabels()
        nameTextField?.text = promise?.name
        if let sequenceNumber = promise?.sequenceNumber {
            sequenceNumberLabel?.text = String(sequenceNumber)
        }
        promiseTimeLabel?.text = promise?.promiseStartTime?.toCDMSimpleString()
        personalityTimeLabel?.text = promise?.personalityStartTime?.toCDMSimpleString()
        tagTimeLabel?.text = promise?.tagStartTime?.toCDMSimpleString()
        resourceTimeLabel?.text = promise?.resourceStartTime?.toCDMSimpleString()
        groundedTimeLabel?.text = promise?.groundedStartTime?.toCDMSimpleString()
        criticizeTimeLabel?.text = promise?.criticizeStartTime?.toCDMSimpleString()
        meetingTimeLabel?.text = promise?.familyMeetingStartTime?.toCDMSimpleString()
        collectionView.reloadData()
    }
    
    // MARK: IBActions

    @IBAction func tappedPromiseItemButton(_ button: UIButton) {
        lastTappedButton = button
        updateLabels(lastTappedButton?.tag)
        let indexPath = IndexPath(item: button.tag, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func tappedTaskButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedChoosePromiseButton(_ sender: AnyObject) {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        PromiseManager.fetchPromisesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (promises) in
                guard let strongSelf = self else { return }
                var index = 0
                if let promise = strongSelf.promise,
                    let indexOfPromise = promises.index(where: {$0.id == promise.id})
                {
                    index = indexOfPromise
                }
                strongSelf.displayOptionWithOptions(promises, startAt: index, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    
    @IBAction func tappedChooseFamilyActivityButton(_ sender: AnyObject) {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        FamilyActivityManager.fetchFamilyActivitiesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (familyActivities) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(familyActivities, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    
    @IBAction func tappedRefreshFamilyActivityButton(_ sender: UIButton) {
        PromiseManager.fetchPromiseWithPromiseId(
            (promise?.id)!,
            success: { [weak self] promise in
                guard let strongSelf = self else { return }
                strongSelf.promise = promise
                strongSelf.updateUI()
            },
            failure: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })

    }
    // MARK: UI
    
    fileprivate func updateButtons() {
        func imageOfStatus(_ status: PromiseStatus) -> UIImage? {
            return (status == .notStarted) ? Constant.futurePromiseImage : (status == .started) ? Constant.currentPromiseImage : Constant.finishedPromiseImage
        }
        // update button image based on status
        guard let promise = promise else { return }
        promiseButton?.setBackgroundImage(imageOfStatus(promise.promiseStatus), for: UIControlState())
        personalityButton?.setBackgroundImage(imageOfStatus(promise.personalityPromiseStatus), for: UIControlState())
        tagButton?.setBackgroundImage(imageOfStatus(promise.tagPromiseStatus), for: UIControlState())
        resourceButton?.setBackgroundImage(imageOfStatus(promise.resourcePromiseStatus), for: UIControlState())
        groundedButton?.setBackgroundImage(imageOfStatus(promise.groundedPromiseStatus), for: UIControlState())
        criticizeButton?.setBackgroundImage(imageOfStatus(promise.criticizePromiseStatus), for: UIControlState())
        meetingButton?.setBackgroundImage(imageOfStatus(promise.familyMeetingPromiseStatus), for: UIControlState())
    }
    
    fileprivate func updateLabels(_ selectedButtonIndex: Int? = nil) {
        func colorOfSelection(_ selected: Bool) -> UIColor {
            return selected ? UIColor.orange : UIColor.white
        }
        let index = selectedButtonIndex ?? 0
        promiseLabel?.textColor = colorOfSelection(index == 0)
        personalityLabel?.textColor = colorOfSelection(index == 1)
        tagLabel?.textColor = colorOfSelection(index == 2)
        resourceLabel?.textColor = colorOfSelection(index == 3)
        groundedLabel?.textColor = colorOfSelection(index == 4)
        criticizeLabel?.textColor = colorOfSelection(index == 5)
        meetingLabel?.textColor = colorOfSelection(index == 6)
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        if identifier == SegueIdentifier.familyActivity {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent
            should = hasPrivilege
        } else if identifier == SegueIdentifier.report {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = hasPrivilege
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if
            let navigationController = segue.destination as? UINavigationController,
            let familyActivityViewController = navigationController.topViewController as? FamilyActivityViewController, segue.identifier == SegueIdentifier.familyActivity
        {
            familyActivityViewController.familyActivity = selectedFamilyActivity
        }
    }

}

// MARK: UICollectionViewDataSource

extension Promise2ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item != 6 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.promiseItem, for: indexPath) as! PromiseItemCell
            cell.promiseItemView.promiseType_ = indexPath.item
            cell.promiseItemView.promise = promise
            cell.promiseItemView.viewController = self
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.meeting, for: indexPath) as! PromiseMeetingCell
            cell.promiseMeetingView.promise = promise
            cell.promiseMeetingView.viewController = self
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        updateLabels(indexPath.item)
    }
}

// MARK: UICollectionViewDelegate

extension Promise2ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}

// MARK: UITextFieldDelegate

extension Promise2ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        saveText()
    }
    
    
    /// Save text to server
    fileprivate func saveText() {
        guard let promise = promise else { return }
        promise.name = nameTextField?.text
        PromiseManager.updatePromise(promise,
                               success: { [weak self] promise in
                                guard let strongSelf = self else { return }
                                strongSelf.promise = promise
                                strongSelf.nameTextField?.text = strongSelf.promise?.name
                                strongSelf.presentAlertViewController("保存成功")
                                },
                               failure: { [weak self] error in
                                guard let strongSelf = self else { return }
                                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: OptionDelegate
extension Promise2ViewController: OptionDelegate {
    /// OptionDelegate method
    func selectedOption(_ option: AnyObject) {
        DLog("selected \(option.description)")
        if let promise = option as? Promise {
            self.promise = promise
            updateUI()
        } else if let familyActivity = option as? FamilyActivity {
            selectedFamilyActivity = familyActivity
            performSegue(withIdentifier: SegueIdentifier.familyActivity, sender: self)
        }
    }
    
    func clearFlags() {}
}

