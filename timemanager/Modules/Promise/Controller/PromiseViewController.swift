//
//  PromiseViewController.swift
//  timemanager
//
//  Created by Can on 8/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation

class PromiseViewController: UIViewController {

    var promise: Promise?
    fileprivate var recordingSession = AVAudioSession.sharedInstance()

    // MARK: Outlets
    
    @IBOutlet weak var promiseDateLabel: UILabel!
    @IBOutlet weak var fButton: UIButton!
    @IBOutlet weak var iButton: UIButton!
    @IBOutlet weak var kButton: UIButton!
    @IBOutlet weak var eButton: UIButton!
    @IBOutlet weak var aButton: UIButton!
    @IBOutlet weak var personalityButton: UIButton!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var resourceButton: UIButton!
    @IBOutlet weak var groundedButton: UIButton!
    @IBOutlet weak var criticizeButton: UIButton!
    @IBOutlet weak var familyMeetingButton: UIButton!
    @IBOutlet weak var promiseAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var personalityChildAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var personalityGuardianAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var tagChildAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var tagGuardianAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var resourceChildAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var resourceGuardianAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var groundedChildAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var groundedGuardianAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var criticizeChildAudioClipCellView: AudioClipCellView!
    @IBOutlet weak var criticizeGuardianAudioClipCellView: AudioClipCellView!
    var familyMeetingAudioClipCellView = [AudioClipCellView]()

    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DLog(promise)
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [weak self] (allowed: Bool) -> Void in
                guard let strongSelf = self else { return }
                do {
                    try strongSelf.recordingSession.setCategory(AVAudioSessionCategoryPlayback)
                } catch {
                    DLog("set AVAudioSessionCategoryPlayAndRecord error")
                }
                if allowed {
                    strongSelf.setupUI()
                } else {
                    // failed to record!
                    strongSelf.presentAlertViewController("请授权录音")
                }
            }
        } catch {
            // failed to record!
            presentAlertViewController("请授权录音")
            do {
                try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func tappedFButton(_ button: UIButton) {
    }
    
    @IBAction func tappedIButton(_ button: UIButton) {
    }
    
    @IBAction func tappedKButton(_ button: UIButton) {
    }
    
    @IBAction func tappedEButton(_ button: UIButton) {
    }
    
    @IBAction func tappedAButton(_ button: UIButton) {
    }
    
    // MARK: UI
    
    fileprivate func setupUI() {
        guard let promise = promise else { return }
        promiseAudioClipCellView.promise = promise
        promiseAudioClipCellView.audio = promise.promiseChildAudio
        personalityChildAudioClipCellView.promise = promise
        personalityChildAudioClipCellView.audio = promise.personalityChildAudio
        personalityGuardianAudioClipCellView.promise = promise
        personalityGuardianAudioClipCellView.audio = promise.personalityGuardianAudio
        tagChildAudioClipCellView.promise = promise
        tagChildAudioClipCellView.audio = promise.tagChildAudio
        tagGuardianAudioClipCellView.promise = promise
        tagGuardianAudioClipCellView.audio = promise.tagGuardianAudio
        resourceChildAudioClipCellView.promise = promise
        resourceChildAudioClipCellView.audio = promise.resourceChildAudio
        resourceGuardianAudioClipCellView.promise = promise
        resourceGuardianAudioClipCellView.audio = promise.resourceGuardianAudio
        groundedChildAudioClipCellView.promise = promise
        groundedChildAudioClipCellView.audio = promise.groundedChildAudio
        groundedGuardianAudioClipCellView.promise = promise
        groundedGuardianAudioClipCellView.audio = promise.groundedGuardianAudio
        criticizeChildAudioClipCellView.promise = promise
        criticizeChildAudioClipCellView.audio = promise.criticizeChildAudio
        criticizeGuardianAudioClipCellView.promise = promise
        criticizeGuardianAudioClipCellView.audio = promise.criticizeGuardianAudio
        personalityButton.isEnabled = promise.personalityPromiseStatus == .notStarted
    }
}
