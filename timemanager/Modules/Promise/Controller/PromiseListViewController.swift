//
//  PromiseListViewController.swift
//  timemanager
//
//  Created by Can on 13/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class PromiseListViewController: UITableViewController {
    
    struct SegueIdentifier {
        static let promise = "promise2"
    }
    
    var promises = [Promise]()
    fileprivate var selectedPromise: Promise?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBActions
    
    @IBAction func tappedAddButton() {
        guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
            presentAlertViewController("没有选择的学生")
            return
        }
        // Create a new Promise
        PromiseManager.generatePromiseWithAccountFamiltyId(selectedStudentAccountFamily.id,
                                                           taskId: nil,
                                                           success: { [weak self] (promise) in
                                                            guard let strongSelf = self else { return }
                                                            if let promise = promise {
                                                                strongSelf.selectedPromise = promise
                                                                strongSelf.promises.insert(promise, at: strongSelf.promises.startIndex)
                                                                DispatchQueue.main.async {
//                                                                    strongSelf.tableView.beginUpdates()
//                                                                    strongSelf.tableView.insertRowsAtIndexPaths([NSIndexPath(index: strongSelf.promises.startIndex)], withRowAnimation: .Automatic)
//                                                                    strongSelf.tableView.endUpdates()
                                                                    strongSelf.tableView.reloadData()
                                                                    strongSelf.performSegue(withIdentifier: SegueIdentifier.promise, sender: strongSelf)
                                                                }
                                                            } else {
                                                                strongSelf.presentAlertViewController("生成承诺书失败，请重试")
                                                            }
                                                        },
                                                           failure: { [weak self] (error) in
                                                            guard let strongSelf = self else { return }
                                                            strongSelf.presentAlertViewController("生成承诺书失败，请重试")
                                                        })
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promises.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let promise = promises[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "promiseCell", for: indexPath) as! PromiseTableViewCell
        cell.promiseDateLabel.text = promise.promiseTime.toCDMSimpleString()
        cell.audioClipCellView.promise = promise
        cell.audioClipCellView.audio = promise.promiseChildAudio
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPromise = promises[indexPath.row]
        performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let promiseViewController = segue.destination as? Promise2ViewController {
            promiseViewController.promise = selectedPromise
        }
    }

}
