//
//  PromiseMeetingView.swift
//  timemanager
//
//  Created by Can on 9/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class PromiseMeetingView: NibView {
    
    // MARK: Variables
    var viewController: Promise2ViewController? {
        didSet {
            audioView1.viewController = viewController
            audioView2.viewController = viewController
            audioView3.viewController = viewController
            audioView4.viewController = viewController
            audioView5.viewController = viewController
            audioView6.viewController = viewController
            audioView7.viewController = viewController
            audioView8.viewController = viewController
            audioView9.viewController = viewController
            audioView10.viewController = viewController
            audioView11.viewController = viewController
            audioView12.viewController = viewController
        }
    }
    
    var promise: Promise? {
        didSet {
            guard let promise = promise else { return }
            audioView1.promise = promise
            audioView2.promise = promise
            audioView3.promise = promise
            audioView4.promise = promise
            audioView5.promise = promise
            audioView6.promise = promise
            audioView7.promise = promise
            audioView8.promise = promise
            audioView9.promise = promise
            audioView10.promise = promise
            audioView11.promise = promise
            audioView12.promise = promise
        }
    }
    
    // MARK: IBOutlets
    @IBOutlet weak var audioView1: PromiseAudioDialogueView!
    @IBOutlet weak var audioView2: PromiseAudioDialogueView!
    @IBOutlet weak var audioView3: PromiseAudioDialogueView!
    @IBOutlet weak var audioView4: PromiseAudioDialogueView!
    @IBOutlet weak var audioView5: PromiseAudioDialogueView!
    @IBOutlet weak var audioView6: PromiseAudioDialogueView!
    @IBOutlet weak var audioView7: PromiseAudioDialogueView!
    @IBOutlet weak var audioView8: PromiseAudioDialogueView!
    @IBOutlet weak var audioView9: PromiseAudioDialogueView!
    @IBOutlet weak var audioView10: PromiseAudioDialogueView!
    @IBOutlet weak var audioView11: PromiseAudioDialogueView!
    @IBOutlet weak var audioView12: PromiseAudioDialogueView!

}
