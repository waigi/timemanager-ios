//
//  PromiseTableViewCell.swift
//  timemanager
//
//  Created by Can on 13/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class PromiseTableViewCell: UITableViewCell {

    @IBOutlet weak var audioClipCellView: AudioClipCellView!
    @IBOutlet weak var promiseDateLabel: UILabel!
    
}
