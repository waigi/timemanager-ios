//
//  PromiseItemCell.swift
//  timemanager
//
//  Created by Can on 24/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class PromiseItemCell: UICollectionViewCell {
    
    @IBOutlet weak var promiseItemView: PromiseItemView!
    
}
