//
//  PromiseAudioDialogueView.swift
//  timemanager
//
//  Created by Can on 9/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation

class PromiseAudioDialogueView: NibCellView {

    
    // MARK: Variables
    
    @IBInspectable @IBOutlet var viewController: Promise2ViewController?
    @IBInspectable var position: Int = 0
    
    @IBInspectable var audioURLString: String? {
        didSet {
            if audioURLString != nil {
                playImageView.isHidden = false
                playButton.isHidden = false
                playButton.isEnabled = true
                bringSubview(toFront: playButton)
                recordButton?.removeFromSuperview()
            }
        }
    }
    
    @IBInspectable var audioLengthInSeconds: Int = 0 {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    var promise: Promise? {
        didSet {
            audio = promise?.audioOfPromiseType(promiseType, andPromisePersonType: promisePersonType, indexOfAudios: position)
        }
    }
    var promiseType = PromiseType.familyMeeting
    var promisePersonType = PromisePersonType.child
    
    /// Remote audio
    var audio: Audio? {
        didSet {
            if let urlString = audio?.urlString {
                audioURLString = urlString
            }
            if let length = audio?.length {
                audioLengthInSeconds = length
            }
        }
    }
    
    /// Audio file (Local) URL, which is the one to be saved/played/uploaded and calculate audio length
    var audioURL: URL {
        let audioFilename = getDocumentsDirectory()
        let audioURL = URL(fileURLWithPath: audioFilename).appendingPathComponent("tmp.m4a")
        return audioURL
    }
    
    fileprivate var recordingSession = AVAudioSession.sharedInstance()
    fileprivate var audioRecorder: AVAudioRecorder!
    fileprivate var player: AVPlayer?
    
    /// Audio item (Remote) to be played
    fileprivate var playerItem: AVPlayerItem? {
        let item: AVPlayerItem?
        if let audioURLString = audioURLString,
            let audioURL = URL(string: audioURLString)
        {
            item = AVPlayerItem(url: audioURL)
        } else {
            DLog("Nothing to play with url = \(audioURLString)")
            item = nil
        }
        return item
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var recordButton: UIButton?
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordButtonLongPressGestureRecognizer: UILongPressGestureRecognizer? {
        didSet { setNeedsUpdateView() }
    }
    
    // MARK: UI updates
    
    override func updateView() {
        super.updateView()
        text = audioLengthInSeconds.audioLengthShortStringFromSeconds
        recordButtonLongPressGestureRecognizer?.addTarget(self, action: #selector(PromiseAudioDialogueView.longPressRecordButton(_:)))
    }
    
    // MARK: IBActions
    
    /// Play audio
    @IBAction func tappedPlayButton() {
        guard let audioURLString = audioURLString,
            let url = URL(string: audioURLString)
            else { return }
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
        player = AVPlayer(url: url)
        startedPlayAudio()
        player?.play()
    }
    
    // MARK: Recorder
    
    /// Long press on record button
    func longPressRecordButton(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            startRecording()
            viewController?.displayRecordingAnimationView()
        } else if recognizer.state == .ended || recognizer.state == .cancelled || recognizer.state == .failed {
            viewController?.removeRecordingAnimationView()
            if recognizer.location(in: recordButton).y < 0 {
                // 上滑取消
                finishRecording(success: false)
            } else {
                finishRecording(success: true)
            }
        }
    }
    
    func startRecording() {
        if audioRecorder == nil {
            do {
                try recordingSession.setCategory(AVAudioSessionCategoryRecord)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100.0,
                AVNumberOfChannelsKey: 2 as NSNumber,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
                AVEncoderBitRateKey : 320000
            ] as [String : Any]
            do {
                audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
            } catch {
                finishRecording(success: false)
            }
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        if success {
            audioLengthInSeconds = audioLengthInSeconds(audioURL)
            uploadAudio()
        } else {
            //            viewController?.presentAlertViewController("录音失败，请重试")
            // recording failed or cancelled
        }
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
    }
    
    // MARK: Helper methods
    
    fileprivate func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first
        return documentsDirectory!
    }
    
    /// Audio length in seconds
    fileprivate func audioLengthInSeconds(_ audioFileURL: URL) -> Int {
        let asset = AVURLAsset(url: audioFileURL, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        return Int(audioDurationSeconds)
    }
    
    func startedPlayAudio() {
        playImageView.animationImages = UIView.audioAnimationImages
        playImageView.animationDuration = 2
        playImageView.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(PromiseAudioDialogueView.finishedPlayAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    func finishedPlayAudio() {
        playImageView.stopAnimating()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    // MARK: Service Call
    
    fileprivate func uploadAudio() {
        guard let promise = promise else { return }
        recordButton?.isEnabled = false
        activityIndicator.startAnimating()
        PromiseManager.uploadPromiseAudio(promise.id,
                                          promiseType: promiseType,
                                          promisePersonType: promisePersonType,
                                          fileURL: audioURL,
                                          audioDurationSeconds: audioLengthInSeconds(audioURL),
                                          index: position,
                                          success: { [weak self] promise in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.promise = promise
                                            // TODO: apply indexOfAudios for familyMeetingAudios
                                            strongSelf.audio = promise.audioOfPromiseType(strongSelf.promiseType,
                                                andPromisePersonType: strongSelf.promisePersonType,
                                                indexOfAudios: strongSelf.position)
                                            strongSelf.viewController?.promise = promise
                                            strongSelf.viewController?.presentAlertViewController("录音上传成功")
            },
                                          failure: { [weak self] error in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.recordButton?.isEnabled = true
                                            strongSelf.viewController?.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: AVAudioRecorderDelegate

extension PromiseAudioDialogueView: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

