//
//  PromiseItemView.swift
//  timemanager
//
//  Created by Can on 19/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class PromiseItemView: NibView {
    
    // MARK: Variables
    var viewController: Promise2ViewController? {
        didSet {
            promiseParentView.viewController = viewController
            promiseChildView.viewController = viewController
            promiseTeacherView.viewController = viewController
        }
    }
    var promise: Promise? {
        didSet {
            guard let promise = promise else { return }
            promiseParentView.promise = promise
            promiseChildView.promise = promise
            promiseTeacherView.promise = promise
            switch promiseType {
            case .content:
                promiseParentView.audio = promise.promiseGuardianAudio
                promiseChildView.audio = promise.promiseChildAudio
                promiseTeacherView.audio = promise.promiseTeacherAudio
            case .personality:
                promiseParentView.audio = promise.personalityGuardianAudio
                promiseChildView.audio = promise.personalityChildAudio
                promiseTeacherView.audio = promise.personalityTeacherAudio
            case .tag:
                promiseParentView.audio = promise.tagGuardianAudio
                promiseChildView.audio = promise.tagChildAudio
                promiseTeacherView.audio = promise.tagTeacherAudio
            case .resource:
                promiseParentView.audio = promise.resourceGuardianAudio
                promiseChildView.audio = promise.resourceChildAudio
                promiseTeacherView.audio = promise.resourceTeacherAudio
            case .grounded:
                promiseParentView.audio = promise.groundedGuardianAudio
                promiseChildView.audio = promise.groundedChildAudio
                promiseTeacherView.audio = promise.groundedTeacherAudio
            case .criticize:
                promiseParentView.audio = promise.criticizeGuardianAudio
                promiseChildView.audio = promise.criticizeChildAudio
                promiseTeacherView.audio = promise.criticizeTeacherAudio
            case .familyMeeting:
                break
            }
        }
    }
    fileprivate var promiseType: PromiseType = .content {
        didSet {
            promiseParentView.promiseType = promiseType
            promiseChildView.promiseType = promiseType
            promiseTeacherView.promiseType = promiseType
        }
    }
    
    @IBInspectable var promiseType_: Int {
        set {
            promiseType = PromiseType(rawValue: String(newValue)) ?? .content
        }
        get {
            return Int(promiseType.rawValue) ?? 0
        }
    }

    // MARK: IBOutlets
    @IBOutlet weak var promiseParentView: PromiseParentView!
    @IBOutlet weak var promiseChildView: PromiseChildView!
    @IBOutlet weak var promiseTeacherView: PromiseTeacherView!
    
}
