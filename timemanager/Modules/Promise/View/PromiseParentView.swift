//
//  PromiseParentView.swift
//  timemanager
//
//  Created by Can on 19/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

class PromiseParentView: PromisePersonView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        promisePersonType = .guardian
    }
    
}
