//
//  PromiseButtonView.swift
//  timemanager
//
//  Created by Can on 17/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class PromiseButtonView: NibCellView {

    @IBInspectable var promiseType_: Int {
        get {
            return Int(promiseType.rawValue)!
        }
        set {
            promiseType = PromiseType(rawValue: String(newValue)) ?? .personality
        }
    }
    
    var promise: Promise? {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    fileprivate var promiseType: PromiseType = .personality { didSet {setNeedsUpdateView()} }
    
    // MARK: - UpdateView mechanism
    
    /// Override in subclasses and call super. Update view and subview properties that are affected by properties of this class.
    override func updateView() {
        super.updateView()
        // 根据此承诺状态变幻button颜色
//        if let promise = promise {
            var status: PromiseStatus = .notStarted
            switch promiseType {
            case .content:
                break
            case .personality:
                status = promise?.personalityPromiseStatus ?? .notStarted
                text = "人格诺"
            case .tag:
                status = promise?.tagPromiseStatus ?? .notStarted
                text = "标签诺"
            case .resource:
                status = promise?.resourcePromiseStatus ?? .notStarted
                text = "资源诺"
            case .grounded:
                status = promise?.groundedPromiseStatus ?? .notStarted
                text = "禁足诺"
            case .criticize:
                status = promise?.criticizePromiseStatus ?? .notStarted
                text = "训诫诺"
            case .familyMeeting:
                status = promise?.familyMeetingPromiseStatus ?? .notStarted
                text = "家庭大会"
            }
            switch status {
            case .notStarted:
                backgroundColor = UIColor.lightGray
                textLabel?.textColor = UIColor.lightText
            case .started:
                backgroundColor = UIColor.blue
                textLabel?.textColor = UIColor.white
            case .finished:
                backgroundColor = UIColor.green
                textLabel?.textColor = UIColor.purple
            }
//        }
    }

}
