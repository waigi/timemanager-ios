//
//  PromiseMeetingCell.swift
//  timemanager
//
//  Created by Can on 9/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class PromiseMeetingCell: UICollectionViewCell {
    
    @IBOutlet weak var promiseMeetingView: PromiseMeetingView!
    
}
