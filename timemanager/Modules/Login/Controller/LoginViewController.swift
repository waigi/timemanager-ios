//
//  LoginViewController.swift
//  timemanager
//
//  Created by Can on 2/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class LoginViewController: UITableViewController {

    struct SegueIdentifier {
        static let unwindToHome = "unwindToHome"
    }
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.text = AppUserInfo.username
        passwordTextField.text = AppUserInfo.validPassword
    }

    @IBAction func tappedLoginButton() {
        guard let username = usernameTextField.text,
            let password = passwordTextField.text
            else { return }
        if username.isEmpty || password.isEmpty {
            presentAlertViewController("WOW，没有用户名和密码怎么能登录呢")
            return
        }
        AppUserInfo.username = username
        AppUserInfo.validPassword = password
        LoginManager.sharedManager.login(username, password: password,
        success: {
            DLog("登录成功，token ＝ \(AppUserInfo.accessToken)")
            AccountManager.fetchAccount(
                {
                    if AppUserInfo.hasAccount {
                        self.performSegue(withIdentifier: SegueIdentifier.unwindToHome, sender: self)
                    } else {
                        self.presentAlertViewController("无法找到您的帐户信息，请联系客服。")
                    }
                }, failure: { (error) in
                    self.presentAlertViewController("获取帐户失败，请重试")
            })
        },
        failure: { error in
            if let error = error {
                self.presentAlertViewController(error.description)
            } else {
                self.presentAlertViewController("登录失败，请重试")
            }
        })
    }
}
