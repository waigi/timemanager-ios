//
//  TimelineContentView.swift
//  timemanager
//
//  Created by Can on 30/06/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

/// Allow horizontal zooming only
class TimelineContentView: UIView {

    var unzoomedViewHeight: CGFloat?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        unzoomedViewHeight = frame.size.height
    }
    
    override var transform: CGAffineTransform {
        get { return super.transform }
        set {
            if let unzoomedViewHeight = unzoomedViewHeight {
                var t = newValue
                t.d = 1.0
                t.ty = (1.0 - t.a) * unzoomedViewHeight/2
                super.transform = t
            }
        }
    }

}
