//
//  TimelineScrollView.swift
//  timemanager
//
//  Created by Can on 9/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

/// override below method to allow swipe gesture recognizer in scrollview
class TimelineScrollView: UIScrollView {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        var should = true
        if (gestureRecognizer == self.panGestureRecognizer) {
            let touchLocation = touch.location(in: self)
            // Disable pan gesture on scroll view when it happens on top of TaskClockView or TaskDollView or TaskDetailView
            for subView in self.subviews[0].subviews {
                if ((subView is TaskClockView) || (subView is TaskDollView) || (subView is TaskDetailView)) && subView.frame.contains(touchLocation)  {
                    should = false
                }
            }
        }
        return should
    }
    
}
