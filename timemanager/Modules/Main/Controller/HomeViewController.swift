//
//  HomeViewController.swift
//  timemanager
//
//  Created by Can on 29/06/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation
import EventKit
import SwiftDate

typealias TaskWithView = (taskId: String, task: Task, taskClockView: TaskClockView?, taskDollView: TaskDollView?, taskDetailBarView: TaskDetailBarView?)

class HomeViewController: UIViewController {
    
    enum ZoomLevel: Int {
        case hour = 0
        case quarter = 1
        case minute = 2
    }
    
    enum TaskOptionButtonType {
        case nPhoto, nText, nAudio, sStart, sEnd, jPhoto, jText, jAudio, jVideo, jDemerit, pSelf, pGuardian, pTeacher
    }
    
    struct SegueIdentifier {
        static let login = "login"
        static let taskNDetail = "taskNDetail"
        static let taskJDetail = "taskJDetail"
        static let taskPDetail = "taskPDetail"
        static let promise = "promise"
        static let familyActivity = "familyActivity"
        static let report = "report"
        static let taskCategoryPopOver = "taskCategoryPopOver"
        static let supervisorPopOver = "supervisorPopOver"
        static let growthTeacherPopOver = "growthTeacherPopOver"
        static let lessonTeacherPopOver = "lessonTeacherPopOver"
        static let guardianPopOver = "guardianPopOver"
        static let studentProfilePopover = "studentProfilePopover"
        static let expiryDatePopover = "expiryDatePopover"
    }
    
    struct Constant {
        static let numberOfDividers = 12 * 24 + 1
        static let minDividerCellWidth: CGFloat = 20
        static let maxDividerCellWidth: CGFloat = 120
        static let dividerCellHeight:CGFloat = 50
        static let clockViewTop: CGFloat = -1
        static let clockViewWidth: CGFloat = 59
        static let clockViewHeight: CGFloat = 67
        static let taskClockViewBottom: CGFloat = 8
        static let taskClockViewWidth: CGFloat = 200
        static let taskClockViewHeight: CGFloat = 55
        static let taskClockViewMaxHeight: CGFloat = 100
        static let taskClockViewMinTimeLengthFromMiddle: CGFloat = 10
        static let studentProfilePopoverSize = CGSize(width: 350, height: 80)
        static let teacherProfilePopoverSize = CGSize(width: 200, height: 80)
        static let expiryDatePopoverSize = CGSize(width: 200, height: 80)
        static let timelineMargin: CGFloat = 100
    }
    
    // MARK: Model Variables
    
    /// 当前正在点击按钮的Task
    var selectedTask: Task?
    /// 当前正在点击按钮的Promise
    var selectedPromise: Promise?
    /// 当前正在点击按钮的FamilyActivity
    var selectedFamilyActivity: FamilyActivity?
    /// 点击的Task按钮
    var tappedButtonType: TaskOptionButtonType?
    /// 当前显示的Tasks所属Date
    var taskDate = Date() {
        didSet {
            updateTaskDateButton()
        }
    }
    var taskWithViews = [String : TaskWithView]()
    /// 是否当前在关联Promise
    var isAssociatingPromise = false
//    private var shouldPlayAlarm = false
    /// The player to play alarm sound
//    private var alarmPlayer: AVPlayer?
    /// Loaded promises for promise list view
    fileprivate var promises = [Promise]()
    /// Loaded students for student profile view
    fileprivate var students = [Student]()
    /// Loaded teachers for teacher profile view
    fileprivate var teachers = [Teacher]()
    /// Loaded guardians for guardian profile view
    fileprivate var guardians = [Guardian]()
    
    // MARK: UI Variables
    
    var zoomLevel: ZoomLevel = .hour
    var clockView = ClockView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var clockModel: SmileWorldClockModel?
    var clockViewCenterXConstraint: NSLayoutConstraint?
    var clockViewUpdateAllowed = false
    
    var dividerCellWidth: CGFloat = Constant.minDividerCellWidth {
        didSet {
            zoomLevel = ZoomLevel(rawValue: Int((dividerCellWidth - Constant.minDividerCellWidth) / ((Constant.maxDividerCellWidth - Constant.minDividerCellWidth) / 3))) ?? .hour
        }
    }
    
    lazy var dividerCellViews: [DividerCellView] = {
        var views = [DividerCellView]()
        for index in 0..<Constant.numberOfDividers {
            let cellView = DividerCellView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            views.append(cellView)
        }
        return views
    }()
    
    /// TimelineView length (include leading & ending white space)
    var timelineViewWidth: CGFloat {
        return dividerCellWidth * CGFloat(Constant.numberOfDividers)
    }
    
    /// Timeline length (exclude leading & ending white space)
    var timelineWidth: CGFloat {
        return dividerCellWidth * CGFloat(Constant.numberOfDividers - 1)
    }
    
    // MARK: IBOutlets
    @IBOutlet weak var timelineScrollView: UIScrollView!
    /// The View contains all Task Views
    @IBOutlet weak var timelineContentView: UIView!
    @IBOutlet weak var timelineContentWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var timelineView: UIView!
    @IBOutlet weak var taskDateButton: UIButton!
    @IBOutlet var rightArrowUpSwipeGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet var rightArrowDownSwipeGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet weak var studentsButton: UIButton!
    @IBOutlet weak var timelineScaleButtonsContainerView: UIView!
    @IBOutlet weak var studentAvatarImageView: UIImageView!
    @IBOutlet weak var zoomLevel3Button: UIButton!
    @IBOutlet weak var expiryDateButton: UIButton!
    
    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // check login status
        if !AppUserInfo.hasAccount {
            performSegue(withIdentifier: SegueIdentifier.login, sender: self)
        }
        // calculate divider width, default to 3 hours
        dividerCellWidth = UIScreen.main.bounds.size.width / CGFloat(3 * 12)
        // resize timeline
        updateTimeline()
        // init dividerCellViews
        initTimelineScaleDividers()
        // init clock after all views layout done
        initClock()
        // other UI updates
        updateTaskDateButton()
        initRightArrow()
        // init alarm player
//        if let url = NSURL(string: "/System/Library/Audio/UISounds/alarm.caf") {
//            alarmPlayer = AVPlayer(URL: url)
//        }
        CalendarManager.checkCalendarAuthorizationStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkLogin()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToClockView()
        // start updating clock when it's appearing
        clockViewUpdateAllowed = true
        // load previous tasks if applicable
        loadTasksIntoTimeline()
    }
    
    /// Add scale dividers into timeline view and set constraints properly
    fileprivate func initTimelineScaleDividers() {
        for index in 0..<dividerCellViews.count {
            let cellView = dividerCellViews[index]
            timelineView.addSubview(cellView)
            cellView.translatesAutoresizingMaskIntoConstraints = false
            // add constraints to cellViews
            var constraints = [NSLayoutConstraint]()
            let topConstraint = NSLayoutConstraint(item: cellView, attribute: .top, relatedBy: .equal, toItem: timelineView, attribute: .top, multiplier: 1.0, constant: 0)
            constraints.append(topConstraint)
            if index == 0 {
                let leadingConstraint = NSLayoutConstraint(item: cellView, attribute: .leading, relatedBy: .equal, toItem: timelineView, attribute: .leading, multiplier: 1.0, constant: 0)
                constraints.append(leadingConstraint)
            } else {
                let leadingConstraint = NSLayoutConstraint(item: cellView, attribute: .leading, relatedBy: .equal, toItem: dividerCellViews[index - 1], attribute: .trailing, multiplier: 1.0, constant: 0)
                constraints.append(leadingConstraint)
            }
            let widthConstraint = NSLayoutConstraint(item: cellView, attribute: .width, relatedBy: .equal, toItem: timelineView, attribute: .width, multiplier: 1.0 / CGFloat(Constant.numberOfDividers), constant: 0)
            let heightConstraint = NSLayoutConstraint(item: cellView, attribute: .height, relatedBy: .equal, toItem: timelineView, attribute: .height, multiplier: 1.0, constant: 0)
            constraints.append(widthConstraint)
            constraints.append(heightConstraint)
            timelineView.addConstraints(constraints)
        }
    }
    
    // MARK: IBActions
    
    /// ZoomLevel SegmentedControl Tapped
    /*@IBAction func onZoomLevelChanged(control: UISegmentedControl) {
        zoomLevel = ZoomLevel(rawValue: control.selectedSegmentIndex) ?? .hour
        // reverse of zoomLevel calulation formula
        dividerCellWidth = CGFloat(zoomLevel.rawValue) * ((Constant.maxDividerCellWidth - Constant.minDividerCellWidth) / 3) + Constant.minDividerCellWidth
        updateTimeline()
    }*/
    
    /// ZoomLevel Buttons
    @IBAction func tappedZoomLevelButton(_ button: UIButton) {
        // update button state
        timelineScaleButtonsContainerView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        // update timeline view scale
        let numberOfHoursToShow = button.tag
        // reverse of zoomLevel calulation formula
        dividerCellWidth = UIScreen.main.bounds.size.width / CGFloat(numberOfHoursToShow * 12)
        updateTimeline()
    }
    
    /// Update zoomLevel when there is Pinch Gesture on the timelineScrollView
    @IBAction func scaleTimelineContentView(_ recognizer: UIPinchGestureRecognizer) {
        let newDividerCellWidth = Int(CGFloat(dividerCellWidth) * recognizer.scale)
        if case Int(Constant.minDividerCellWidth) ... Int(Constant.maxDividerCellWidth) = newDividerCellWidth {
            dividerCellWidth = CGFloat(newDividerCellWidth)
            updateTimeline()
        }
    }
    
    /*@IBAction func tapClockButton() {
        scrollToClockView()
    }

    @IBAction func tapScrollToLeftEndButton() {
        UIView.animateWithDuration(1, animations: {
            self.timelineScrollView.contentOffset.x = 0
        })
    }
    
    @IBAction func tapScrollToRightEndButton() {
        UIView.animateWithDuration(1, animations: {
            self.timelineScrollView.contentOffset.x = self.timelineScrollView.contentSize.width - self.view.bounds.width
        })
    }*/
    
    @IBAction func tapPreviousWeekButton() {
        if let date = 1.weeks.ago(from: taskDate) {
            switchTaskDate(to: date)
        }
    }
    
    @IBAction func tapNextWeekButton() {
        if let date = 1.weeks.from(date: taskDate) {
            switchTaskDate(to: date)
        }
    }
    
    @IBAction func tappedLogoutButton() {
        AppUserInfo.clearData()
        performSegue(withIdentifier: SegueIdentifier.login, sender: self)
    }
    
    @IBAction func tappedChooseDateButton(_ sender: AnyObject) {
        displayOptionWithOptions(weeksArray(), startAt: 30, withDelegate: self)
    }
    
    @IBAction func tappedWechatButton(_ sender: UIButton) {
        launchWechat()
    }
    
    @IBAction func tappedChangeChildButton(_ sender: UIButton) {
        // privilege check
        guard AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor else { return }
        // fetch students from server
        AccountManager.fetchManagedStudentsWithTeacherId(
            AppUserInfo.account.id,
            success: { [weak self] (students) in
                guard let strongSelf = self else { return }
                strongSelf.students = students
                strongSelf.performSegue(withIdentifier: SegueIdentifier.studentProfilePopover, sender: sender)
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController("获取学生列表失败，请重试")
            })
    }
    
    @IBAction func tappedExpiryDateButton(_ sender: UIButton) {
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor else { return }
        performSegue(withIdentifier: SegueIdentifier.expiryDatePopover, sender: sender)
    }
    
    @IBAction func tappedChoosePromiseButton(_ sender: AnyObject) {
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor else { return }
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        PromiseManager.fetchPromisesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (promises) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(promises, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    
    @IBAction func tappedChooseFamilyActivityButton(_ sender: AnyObject) {
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent else { return }
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        FamilyActivityManager.fetchFamilyActivitiesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (familyActivities) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(familyActivities, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    @IBAction func tappedClockButton(_ sender: AnyObject) {
        scrollToClockView()
    }
    
    @IBAction func tappedAlarmButton(_ sender: AnyObject) {
//        shouldPlayAlarm = !shouldPlayAlarm
//        playStopAlarm()
    }
    
    @IBAction func tappedRefreshButton() {
        loadTasksIntoTimeline()
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        if identifier == SegueIdentifier.promise {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = (selectedTask?.promise != nil) && hasPrivilege
        } else if identifier == SegueIdentifier.familyActivity {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent
            should = (selectedTask?.familyActivity != nil)  && hasPrivilege
        } else if identifier == SegueIdentifier.report {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = hasPrivilege
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tabBarController = segue.destination as? UITabBarController,
            let buttonType = tappedButtonType, segue.identifier == SegueIdentifier.taskNDetail
        {
            switch buttonType {
            case .nPhoto:
                tabBarController.selectedIndex = 0
            case .nText:
                tabBarController.selectedIndex = 1
            case .nAudio:
                tabBarController.selectedIndex = 2
            default:
                break
            }
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                taskTabBarController.task = selectedTask
                taskTabBarController.taskDelegate = self
            }
        }
        else if let
            tabBarController = segue.destination as? UITabBarController,
            let buttonType = tappedButtonType, segue.identifier == SegueIdentifier.taskJDetail
        {
            switch buttonType {
            case .jPhoto:
                tabBarController.selectedIndex = 0
            case .jText:
                tabBarController.selectedIndex = 1
            case .jAudio:
                tabBarController.selectedIndex = 2
            case .jVideo:
                tabBarController.selectedIndex = 3
            case .jDemerit:
                tabBarController.selectedIndex = 4
            default:
                break
            }
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                taskTabBarController.task = selectedTask
                taskTabBarController.taskDelegate = self
            }
        }
        else if segue.identifier == SegueIdentifier.taskPDetail,
            let navigationController = segue.destination as? UINavigationController,
            let briefCreditViewController = navigationController.topViewController as? BriefCreditViewController
        {
            if let task = selectedTask {
                briefCreditViewController.task = task
                briefCreditViewController.taskDelegate = self
            } else {
                presentAlertViewController("没有选择的任务")
            }
        } else if
            let navigationController = segue.destination as? UINavigationController,
            let promiseListViewController = navigationController.topViewController as? PromiseListViewController
        {
            promiseListViewController.promises = promises
        } else if let taskCategoryViewController = segue.destination as? TaskCategoryViewController {
            taskCategoryViewController.delegate = self
            taskCategoryViewController.task = selectedTask
        } else if
            let navigationController = segue.destination as? UINavigationController,
            let promise2ViewController = navigationController.topViewController as? Promise2ViewController, segue.identifier == SegueIdentifier.promise
        {
            promise2ViewController.promise = selectedPromise
        }
        else if
            let navigationController = segue.destination as? UINavigationController,
            let familyActivityViewController = navigationController.topViewController as? FamilyActivityViewController, segue.identifier == SegueIdentifier.familyActivity
        {
            familyActivityViewController.familyActivity = selectedFamilyActivity
        }
        else if let profileViewController = segue.destination as? TeacherProfileViewController {
            switch segue.identifier! {
            case SegueIdentifier.guardianPopOver: profileViewController.profileType = TeacherProfileViewController.ProfileType.guardian
            case SegueIdentifier.lessonTeacherPopOver: profileViewController.profileType = TeacherProfileViewController.ProfileType.lessonTeacher
            case SegueIdentifier.growthTeacherPopOver: profileViewController.profileType = TeacherProfileViewController.ProfileType.growthTeacher
            case SegueIdentifier.supervisorPopOver: profileViewController.profileType = TeacherProfileViewController.ProfileType.supervisor
            default: break
            }
            profileViewController.modalPresentationStyle = .popover
            profileViewController.popoverPresentationController?.backgroundColor = UIColor.white
            profileViewController.popoverPresentationController?.delegate = self
            if let view = sender as? UIView {
                profileViewController.popoverPresentationController?.sourceRect = view.bounds
            }
            profileViewController.preferredContentSize = Constant.teacherProfilePopoverSize
        } else if let studentProfileViewController = segue.destination as? StudentProfileViewController {
            studentProfileViewController.delegate = self
            studentProfileViewController.students = students
            studentProfileViewController.modalPresentationStyle = .popover
            studentProfileViewController.popoverPresentationController?.backgroundColor = UIColor.white
            studentProfileViewController.popoverPresentationController?.delegate = self
            studentProfileViewController.popoverPresentationController?.sourceRect = studentsButton.bounds
            studentProfileViewController.preferredContentSize = Constant.studentProfilePopoverSize
        } else if SegueIdentifier.expiryDatePopover == segue.identifier,
            let expiryDateViewController = segue.destination as? ExpiryDateViewController
        {
            expiryDateViewController.modalPresentationStyle = .popover
            expiryDateViewController.popoverPresentationController?.backgroundColor = UIColor.white
            expiryDateViewController.popoverPresentationController?.delegate = self
            expiryDateViewController.popoverPresentationController?.sourceRect = expiryDateButton.bounds
            expiryDateViewController.preferredContentSize = Constant.expiryDatePopoverSize
        }
    }
    
    @IBAction func unwindToHomeWithSegue(_ segue: UIStoryboardSegue) {
        if segue.source is LoginViewController {
            showAccountAvatar()
            // Reload Tasks after login
            loadTasksIntoTimeline()
        } else if let promiseViewController = segue.source as? Promise2ViewController {
            selectedTask?.promise = promiseViewController.promise
        } else if let familyActivityViewController = segue.source as? FamilyActivityViewController {
            selectedTask?.familyActivity = familyActivityViewController.familyActivity
        }
    }
    
    // MARK: Login, Calendar check
    
    /// If not logged in, navigated to login view
    fileprivate func checkLogin() {
        let loggedIn = AppUserInfo.hasAccount
        if !loggedIn {
            performSegue(withIdentifier: SegueIdentifier.login, sender: self)
        } else {
            DLog("User logged in with account = \(AppUserInfo.account)")
            // check authentication
            AccountManager.fetchAccountFamilyWithId(
                AppUserInfo.account.accountFamily.id,
                success: { [weak self] accountFamily in
                    self?.showAccountAvatar()
                },
                failure: { error in
                    self.presentAlertViewController(error?.localizedFailureReason, showCancel: false, handler: {
                        if error?.code == 401 {
                            self.performSegue(withIdentifier: SegueIdentifier.login, sender: self)
                        }
                    })
            })
        }
    }
    
    private func showAccountAvatar() {
        // show avatar
        //if let url = AppUserInfo.account.avatarURL {
         //   self.studentAvatarImageView.kf.setImage(with: url)
        //}
        //li guan wei begin
        if AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor {
            if let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily {
                if let url = selectedStudentAccountFamily.studentAvatarURL{
                    self.studentAvatarImageView.kf.setImage(with: url)
                }
            }
        }else {
            if let url = AppUserInfo.account.avatarURL {
                self.studentAvatarImageView.kf.setImage(with: url)
            }
        }
        //li guan wei end
    }
    
}

// MARK: Timeline UI

extension HomeViewController {
    
    /// Update timeline sizes and it's scales
    fileprivate func updateTimeline() {
        // resize scrollView content size
        timelineScrollView.contentSize = CGSize(width: dividerCellWidth * CGFloat(Constant.numberOfDividers) + Constant.timelineMargin * 2, height: timelineScrollView.contentSize.height)
        timelineContentWidthConstraint.constant = timelineScrollView.contentSize.width
        timelineContentView.layoutIfNeeded()
        // resize each divider cells
        for index in 0..<dividerCellViews.count {
            let cellView = dividerCellViews[index]
            cellView.frame = CGRect(x: CGFloat(index) * dividerCellWidth, y: 0, width: dividerCellWidth, height: Constant.dividerCellHeight)
            updateDividerCellView(cellView, atIndex: index)
        }
        // update all Task Views (Clock/Doll)
        taskWithViews.values.forEach { taskWithView in
            updateTaskViewPosition(taskWithView)
        }
    }
    
    /// Update divider label and scale height based on current zoomLevel
    fileprivate func updateDividerCellView(_ dividerCellView: DividerCellView, atIndex index: Int) {
        func labelStringAtIndex(_ index: Int) -> String {
            let currentMinutes = 5 * index
            return isHourDividerAtIndex(index) ? "\(currentMinutes / 60)" : "\(currentMinutes % 60)"
        }
        func isHourDividerAtIndex(_ index: Int) -> Bool { return (index % 12) == 0 }
        func isQuarterOrHourDividerAtIndex(_ index: Int) -> Bool { return (index % 3) == 0 }
        
        switch index {
        case 0:
            dividerCellView.dividerCellType = .left
        case Constant.numberOfDividers - 1:
            dividerCellView.dividerCellType = .right
        default:
            break
        }
//        switch zoomLevel {
//        case .hour:
//            dividerCellView.showLabel = isHourDividerAtIndex(index)
//        case .quarter:
//            dividerCellView.showLabel = isQuarterOrHourDividerAtIndex(index)
//        case .minute:
//            dividerCellView.showLabel = true
//        }
        dividerCellView.showLabel = isHourDividerAtIndex(index)
        if isQuarterOrHourDividerAtIndex(index) {
            dividerCellView.scaleType = .long
        }
        dividerCellView.dividerLabel.text = labelStringAtIndex(index)
    }
    
    fileprivate func updateTaskDateButton() {
        taskDateButton.setTitle(taskDate.toCDMSimpleString(), for: UIControlState())
    }
}

// MARK: UIScrollViewDelegate

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // disable virtical scroll
        if timelineScrollView.contentOffset.y != 0 {
            timelineScrollView.contentOffset.y = 0
        }
    }
}

// MARK: Clock

extension HomeViewController: SmileWorldClockModelDelegate {
    /// Create clock instance and set clock view's initial position
    fileprivate func initClock() {
        // set data model
        clockModel = SmileWorldClockModel(theDelegate: self)
        clockModel?.addData(SmileTimeZoneData(timeZone: TimeZone.current))
        updateClockViewTime()
        // set handler
        clockView.handlerHeight = Int(timelineView.frame.height)
        // set constraints
        let topConstraint = NSLayoutConstraint(item: clockView, attribute: .top, relatedBy: .equal, toItem: timelineView, attribute: .bottom, multiplier: 1.0, constant: Constant.clockViewTop)
        let widthConstraint = NSLayoutConstraint(item: clockView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: Constant.clockViewWidth)
        let heightConstraint = NSLayoutConstraint(item: clockView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: Constant.clockViewHeight)
        // initial position of clock is current time
        let now = Date()
        let totalSecondsElapsedToday = now.hour * 3600 + now.minute * 60 + now.second
        let percentageOfTimeElapsedToday = CGFloat(totalSecondsElapsedToday / Date.secondsOfADay)
        let totalTimelineLength = CGFloat(Constant.numberOfDividers - 1) * dividerCellWidth
        let currentTimePosition = totalTimelineLength * percentageOfTimeElapsedToday
        clockViewCenterXConstraint = NSLayoutConstraint(item: clockView, attribute: .centerX, relatedBy: .equal, toItem: timelineView, attribute: .leading, multiplier: 1.0, constant: CGFloat(currentTimePosition) + (clockView.frame.width / 2))
        timelineContentView.addSubview(clockView)
        clockView.translatesAutoresizingMaskIntoConstraints = false
        timelineContentView.addConstraints([topConstraint, widthConstraint, heightConstraint])
        if let clockViewCenterXConstraint = clockViewCenterXConstraint {
            timelineContentView.addConstraint(clockViewCenterXConstraint)
        }
        timelineContentView.layoutIfNeeded()
        updateClockViewPosition()
        // add clock gesture action
        clockView.longPressGestureRecognizer.addTarget(self, action: #selector(HomeViewController.generateTaskClock(_:)))
    }
    
    func secondHasPassed() {
        // update clock view position
        if clockViewUpdateAllowed {
            updateClockView()
        }
    }
    
    func timeZonesInModelHaveChanged() {
        // nothing to do
    }
    
    /// Update clock time and position.
    fileprivate func updateClockView() {
        updateClockViewTime()
        updateClockViewPosition()
    }
    
    /// Update hands of clock.
    fileprivate func updateClockViewTime() {
        clockView.clockData = clockModel?.selectedTimeZones.first ?? SmileTimeZoneData(timeZone: TimeZone.current)
    }
    
    /// Update clock position on timeline.
    fileprivate func updateClockViewPosition() {
        guard let clockViewCenterXConstraint = clockViewCenterXConstraint else { return }
        let totalTimelineLength = CGFloat(Constant.numberOfDividers - 1) * dividerCellWidth
        let currentTimePosition = Double(totalTimelineLength) * clockView.percentageOfTimeElapsedToday
        // clock moves per second if taskDate is today, otherwise, park at the end of timeline for past and at the begin of timeline for future date
        if taskDate.isToday {
            clockViewCenterXConstraint.constant = CGFloat(currentTimePosition) + CGFloat(dividerCellWidth / 2)
        } else if taskDate.isInPast {
            clockViewCenterXConstraint.constant = totalTimelineLength + CGFloat(dividerCellWidth / 2)
        } else if taskDate.isInFuture {
            clockViewCenterXConstraint.constant = 0 + CGFloat(dividerCellWidth / 2)
        }
//        DLog("updated clock position to = \(clockView.frame) to time = \(clockView.clock.hour):\(clockView.clock.minute):\(clockView.clock.second)")
    }
    
    /// Scroll view to show clockView in the center of screen
    fileprivate func scrollToClockView() {
        UIView.animate(withDuration: 1, animations: {
            self.timelineScrollView.contentOffset.x = self.clockView.center.x - self.view.bounds.width / 2
        })
    }
}

// MARK: Task Clock View

extension HomeViewController {
    /// Generate a Task Clock start from now
    func generateTaskClock(_ recognizer: UILongPressGestureRecognizer) {
        
        /// Create task clock view with generated task id
        func generateTask(_ task: Task) {
            let taskClockView = TaskClockView(frame: CGRect(x: clockView.frame.origin.x, y: clockView.frame.origin.y, width: Constant.taskClockViewWidth, height: Constant.taskClockViewHeight))
            // set constraints
            let topConstraint = NSLayoutConstraint(item: taskClockView, attribute: .top, relatedBy: .equal, toItem: timelineView, attribute: .bottom, multiplier: 1.0, constant: Constant.clockViewTop)
            let bottomConstraint = NSLayoutConstraint(item: taskClockView, attribute: .bottom, relatedBy: .equal, toItem: timelineContentView, attribute: .bottom, multiplier: 1.0, constant: -Constant.taskClockViewBottom)
            let widthConstraint = NSLayoutConstraint(item: taskClockView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: Constant.taskClockViewWidth)
            let centerXConstraint = NSLayoutConstraint(item: taskClockView, attribute: .centerX, relatedBy: .equal, toItem: timelineView, attribute: .leading, multiplier: 1.0, constant: clockViewCenterXConstraint?.constant ?? 0 + taskClockView.leftTimeTrailingConstraint.constant)
            taskClockView.taskClockViewWidthConstraint = widthConstraint
            taskClockView.taskClockViewCenterXConstraint = centerXConstraint
            timelineContentView.addSubview(taskClockView)
            taskClockView.translatesAutoresizingMaskIntoConstraints = false
            timelineContentView.addConstraints([topConstraint, bottomConstraint, widthConstraint, centerXConstraint])
            timelineContentView.layoutIfNeeded()
            // gestures
            taskClockView.leftHandPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockViewLeftHand(_:)))
            taskClockView.rightHandPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockViewRightHand(_:)))
            taskClockView.taskClockViewPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockView(_:)))
            taskClockView.taskClockViewSwipeUpGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeUpTaskClockView(_:)))
            // update Task planned time
            let initialStartTime = dateAtTimelinePostion(centerXConstraint.constant - taskClockView.frame.width / 2)
            let initialEndTime = dateAtTimelinePostion(centerXConstraint.constant + taskClockView.frame.width / 2)
            // Update Task time to backend, if failed, remove generated taskview
            TaskManager.updateTaskTime(
                task.id,
                plannedStartTime: initialStartTime,
                plannedEndTime: initialEndTime,
                actualStartTime: nil,
                actualEndTime: nil,
                success: { [weak self] task in
                    guard let strongSelf = self else { return }
                    if let task = task {
                        let taskWithView = TaskWithView(taskId: task.id, task, taskClockView, nil, nil)
                        strongSelf.taskWithViews[task.id] = taskWithView
                        strongSelf.updateTimeOfTaskClockView(taskClockView)
                    } else {
                        taskClockView.removeFromSuperview()
                    }
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController(error?.description ?? "")
                    taskClockView.removeFromSuperview()
            })
        }
        
        if recognizer.state == UIGestureRecognizerState.ended {
            guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
                presentAlertViewController("没有选择的学生")
                return
            }
            // generate taskId from server
            TaskManager.generateTaskWithAccountFamilyId(
                selectedStudentAccountFamily.id,
                success: { [weak self] (task) in
                    guard let strongSelf = self else { return }
                    if let task = task {
                        DispatchQueue.main.async {
                            generateTask(task)
                        }
                    } else {
                        strongSelf.presentAlertViewController("生成任务失败，请重试")
                    }
                },
                failure: { [weak self] (error) in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController("生成任务失败，请重试")
                })
        }
    }
    
    /// Update task start time
    func panTaskClockViewLeftHand(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: timelineContentView)
        if let
            view = recognizer.view?.superview,
            let taskClockView = view as? TaskClockView
        {
            let newLeftTimeLengthFromMiddle = taskClockView.leftTimeTrailingConstraint.constant - translation.x
            if newLeftTimeLengthFromMiddle > Constant.taskClockViewMinTimeLengthFromMiddle {
                let distanceBetweenHands = newLeftTimeLengthFromMiddle + taskClockView.rightTimeLeadingConstraint.constant
                taskClockView.leftTimeLengthFromMiddle = distanceBetweenHands / 2
                taskClockView.rightTimeLengthFromMiddle = distanceBetweenHands / 2
                taskClockView.taskClockViewCenterXConstraint?.constant += (translation.x / 2)
                taskClockView.taskClockViewWidthConstraint?.constant -= translation.x
                updateTimeOfTaskClockView(taskClockView)
                taskClockView.layoutIfNeeded()
            }
        }
        recognizer.setTranslation(CGPoint.zero, in: timelineContentView)
    }
    
    /// Update task end time
    func panTaskClockViewRightHand(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: timelineContentView)
        if let
            view = recognizer.view?.superview,
            let taskClockView = view as? TaskClockView
        {
            let newRightTimeLengthFromMiddle = taskClockView.rightTimeLeadingConstraint.constant + translation.x
            if newRightTimeLengthFromMiddle > Constant.taskClockViewMinTimeLengthFromMiddle {
                let distanceBetweenHands = newRightTimeLengthFromMiddle + taskClockView.leftTimeTrailingConstraint.constant
                taskClockView.leftTimeLengthFromMiddle = distanceBetweenHands / 2
                taskClockView.rightTimeLengthFromMiddle = distanceBetweenHands / 2
                taskClockView.taskClockViewCenterXConstraint?.constant += (translation.x / 2)
                taskClockView.taskClockViewWidthConstraint?.constant += translation.x
                updateTimeOfTaskClockView(taskClockView)
                taskClockView.layoutIfNeeded()
            }
        }
        recognizer.setTranslation(CGPoint.zero, in: timelineContentView)
    }
    
    /// Move TaskClockView when drag or swipe up to generate Doll view
    func panTaskClockView(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: timelineContentView)
        // move clock view when pan left/right
        if let
            view = recognizer.view,
            let taskClockView = view as? TaskClockView,
            let taskClockViewCenterXConstraint = taskClockView.taskClockViewCenterXConstraint
        {
            taskClockView.taskClockViewCenterXConstraint?.constant = taskClockViewCenterXConstraint.constant + translation.x
            updateTimeOfTaskClockView(taskClockView)
        }
        recognizer.setTranslation(CGPoint.zero, in: timelineContentView)
        // if location in taskClockView has y < 0, it means swipe up
        if recognizer.state == .ended,
            let taskClockView = recognizer.view as? TaskClockView {
            if recognizer.location(in: taskClockView).y < 0 {
                // generate doll view when swipe up
                swipeUpTaskClockView(recognizer)
            } else if recognizer.location(in: taskClockView).y > taskClockView.frame.height {
                // swipe down to delete Task
                swipeDownTaskClockView(recognizer)
            }
        }
    }
    
    /// Swipe up to generate TaskDollView
    func swipeUpTaskClockView(_ recognizer: UIPanGestureRecognizer) {
        if let
            view = recognizer.view,
            let taskClockView = view as? TaskClockView {
            // find taskWithView
            guard let taskWithView = taskWithViewAssociatedWithTaskClockView(taskClockView) else { return }
            let task = taskWithView.task
            guard (task.plannedEndTime.timeIntervalSince1970 - task.plannedStartTime.timeIntervalSince1970) >= 60 else {
                presentAlertViewController("任务时长不能少于1分钟")
                return
            }
            // privilege check
            if (task.isAfterEnd && !(AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor))
                || (task.isInProgress && !(AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor))
            {
                return
            }
            taskClockView.removeFromSuperview()
            generateTaskDollViewOfTask(taskWithView, inView: self.timelineContentView)
            
            // update Task in server
            let actualStartTime: Date? = task.plannedStartTime < Date() ? task.plannedStartTime : nil
            let actualEndTime: Date? = task.plannedEndTime < Date() ? task.plannedEndTime : nil
            TaskManager.updateTaskTime(
                task.id,
                plannedStartTime: task.plannedStartTime,
                plannedEndTime: task.plannedEndTime,
                actualStartTime: actualStartTime,
                actualEndTime: actualEndTime,
                success: { [weak self] task in
                    guard let strongSelf = self else { return }
                    if let task = task {
                        let taskWithView = strongSelf.taskWithViews[task.id] ?? TaskWithView(taskId: task.id, task, taskClockView, nil, nil)
                        strongSelf.updateTimeOfTaskClockView(taskClockView)
                        taskWithView.taskDollView?.task = task
                        taskWithView.taskDetailBarView?.task = task
                        strongSelf.taskWithViews[task.id] = (task.id, task, taskWithView.taskClockView, taskWithView.taskDollView, taskWithView.taskDetailBarView)
                    } else {
                        taskClockView.removeFromSuperview()
                    }
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController(error?.description ?? "")
                    taskClockView.removeFromSuperview()
            })
        }
    }
    
    /// Swipe down to delete task
    func swipeDownTaskClockView(_ recognizer: UIPanGestureRecognizer) {
        if let view = recognizer.view,
            let taskClockView = view as? TaskClockView
        {
            // find taskWithView
            guard let taskWithView = taskWithViewAssociatedWithTaskClockView(taskClockView) else { return }
            // delete task and if success, delete associated views
            TaskManager.deleteTask(
                taskWithView.task,
                success: { [weak self] (success) in
                    guard let strongSelf = self else { return }
                    if success {
                        taskWithView.taskClockView?.removeFromSuperview()
                        taskWithView.taskDollView?.removeFromSuperview()
                        taskWithView.taskDetailBarView?.removeFromSuperview()
                        strongSelf.taskWithViews.removeValue(forKey: taskWithView.task.id)
                    } else {
                        strongSelf.presentAlertViewController("删除失败，请重试。")
                    }
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController(error?.localizedFailureReason, showCancel: false, handler: {
                        if error?.code == 401 {
                            strongSelf.performSegue(withIdentifier: SegueIdentifier.login, sender: self)
                        }
                    })
            })
        }
    }
    
    /// Update TaskDollView position if there is one, otherwise, update TaskClockView position when zooming
    func updateTaskViewPosition(_ taskWithView: TaskWithView) {
        if taskWithView.taskDollView != nil {
            updateTaskDollViewPosition(taskWithView)
        } else {
            updateTaskClockViewPosition(taskWithView)
        }
    }
    
    /// Update Task clock width and position on timeline with Task value when zoom in/out timeline.
    fileprivate func updateTaskClockViewPosition(_ taskWithView: TaskWithView) {
        let task = taskWithView.task
        guard let taskClockView = taskWithView.taskClockView else { return }
        let leftHandPosition = timelinePositionOfDate(task.plannedStartTime as Date)
        let rightHandPosition = timelinePositionOfDate(task.plannedEndTime as Date)
        let handToMiddleLength = (rightHandPosition - leftHandPosition) / 2
//        let zoomRate = Constant.taskClockViewWidth * (dividerCellWidth / Constant.minDividerCellWidth)
        let zoomRate = dividerCellWidth / Constant.minDividerCellWidth
        taskClockView.taskClockViewWidthConstraint?.constant *= zoomRate
        taskClockView.taskClockViewCenterXConstraint?.constant = leftHandPosition + handToMiddleLength
        taskClockView.leftTimeLengthFromMiddle = handToMiddleLength
        taskClockView.rightTimeLengthFromMiddle = handToMiddleLength
        taskClockView.layoutIfNeeded()
    }
    
    /// Update time labels on Task Clock and associated Task while it's moving
    func updateTimeOfTaskClockView(_ taskClockView: TaskClockView) {
        // find taskWithView
        guard let taskWithView = taskWithViewAssociatedWithTaskClockView(taskClockView) else { return }
        let leftHandPosition = (taskClockView.taskClockViewCenterXConstraint?.constant ?? 0) - taskClockView.leftTimeTrailingConstraint.constant
        let rightHandPosition = (taskClockView.taskClockViewCenterXConstraint?.constant ?? 0) + taskClockView.rightTimeLeadingConstraint.constant
        let startTime = dateAtTimelinePostion(leftHandPosition)
        let endTime = dateAtTimelinePostion(rightHandPosition)
        // update Task
        taskWithView.task.plannedStartTime = startTime
        taskWithView.task.plannedEndTime = endTime
        taskWithView.task.actualStartTime = nil
        taskWithView.task.actualEndTime = nil
        // update taskClockView
        taskClockView.plannedStartTimeLabel.text = DateFormatter.timeDateFormatter.string(from: startTime)
        taskClockView.plannedEndTimeLabel.text = DateFormatter.timeDateFormatter.string(from: endTime)
        taskClockView.durationInMinutes.text = String(taskWithView.task.plannedDurationInMinutes)
    }
    
    /// Convert frame.x value on timeline to NSDate, position is the frame.x position to timelineView left edge
    fileprivate func dateAtTimelinePostion(_ position: CGFloat) -> Date {
        let secondsPerPoint = CGFloat(Date.secondsOfADay) / (timelineView.frame.width - dividerCellWidth)
        let secondsFromDayBegining = (position - dividerCellWidth / 2) * secondsPerPoint
        // round start time and end time to whole minutes.
        let roundedSecondsFromDayBegining = secondsFromDayBegining - secondsFromDayBegining.truncatingRemainder(dividingBy: 60)
        let time = Int(roundedSecondsFromDayBegining).seconds.from(date: taskDate.startOfDay)!
        return time
    }
    
    /// Find the TaskWithView object which contains given taskClockView
    fileprivate func taskWithViewAssociatedWithTaskClockView(_ taskClockView: TaskClockView) -> TaskWithView? {
        return taskWithViews.values.filter { (taskWithView) -> Bool in
            return taskWithView.taskClockView == taskClockView
        }.first
    }
}

// MARK: Timeline

extension HomeViewController {
    /// Find proper position on timeline for given date(time), position is the frame.x position to timelineView left edge
    func timelinePositionOfDate(_ date: Date) -> CGFloat {
        let secondsElaspedOnTaskDate = date.timeIntervalSince1970 - taskDate.startOfDay.timeIntervalSince1970
        let pointsPerSecond = (timelineView.frame.width - dividerCellWidth) / CGFloat(Date.secondsOfADay)
        let position = pointsPerSecond * CGFloat(secondsElaspedOnTaskDate) + dividerCellWidth / 2
        return position
    }
    
    // MARK: Timeline Right Arrow
    
    fileprivate func initRightArrow() {
        rightArrowUpSwipeGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeRightArrow(_:)))
        rightArrowDownSwipeGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeRightArrow(_:)))
    }
    
    func swipeRightArrow(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.direction == .down {
            switchTaskDate(to: 1.days.from(date: taskDate))
        } else if recognizer.direction == .up {
            switchTaskDate(to: 1.days.ago(from: taskDate))
        }
    }
}

// MARK: Task Date

extension HomeViewController {
    /// Update the date to load Tasks
    fileprivate func switchTaskDate(to newDate: Date?) {
        guard let newDate = newDate else { return }
        taskDate = newDate
        loadTasksIntoTimeline()
    }
}

// MARK: UIGestureRecognizerDelegate

extension HomeViewController: UIGestureRecognizerDelegate {
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceivePress press: UIPress) -> Bool {
//        return true
//    }
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//        return true
//    }
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
}

// MARK: UIPopoverControllerDelegate

extension HomeViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: TaskCategoryViewDelegate
extension HomeViewController: TaskCategoryViewDelegate {
    /// 选择Task类型后的回调方法
    func taskCategoryView(_ taskCategoryView: TaskCategoryView, clickedButtonAtIndex index: Int) {
        guard let task = selectedTask else { return }
        task.category = TaskCategory(rawValue: index) ?? .original
        if task.category != .original {
            TaskManager.updateTask(task,
                                   success: { [weak self] (task) in
                                    guard let strongSelf = self else { return }
                                    strongSelf.selectedTask = task
                                    DispatchQueue.main.async(execute: { 
                                        strongSelf.dollViewWithTask(strongSelf.selectedTask)?.task = strongSelf.selectedTask
                                        strongSelf.detailViewWithTask(strongSelf.selectedTask)?.task = strongSelf.selectedTask
                                    })
                                    },
                                   failure: { [weak self] (error) in
                                    guard let strongSelf = self else { return }
                                    strongSelf.presentAlertViewController("任务更新失败，请重试")
                                    })
        }
    }
}

// MARK: TaskWithView
extension HomeViewController {
    /// Get Task Doll View associated with a task
    func dollViewWithTask(_ task: Task?) -> TaskDollView? {
        var dollView: TaskDollView?
        if let task = task, let taskWithView = taskWithViews[task.id] {
            dollView = taskWithView.taskDollView
        }
        return dollView
    }
    
    /// Get Task Detail View associated with a task
    func detailViewWithTask(_ task: Task?) -> TaskDetailView? {
        var detailView: TaskDetailView?
        if let task = task, let taskWithView = taskWithViews[task.id] {
            detailView = taskWithView.taskDetailBarView?.taskDetailView
        }
        return detailView
    }
}

// MARK: OptionDelegate
extension HomeViewController: OptionDelegate {
    /// OptionDelegate method
    func selectedOption(_ option: AnyObject) {
        DLog("selected \(option.description)")
        if let cdmDate = option as? CDMDate {
            switchTaskDate(to: cdmDate.currentDate as Date)
        } else if let promise = option as? Promise {
            selectedPromise = promise
            if isAssociatingPromise {
                // 关联Promise和task
                isAssociatingPromise = false
                guard let task = selectedTask else {
                    presentAlertViewController("找不到任务")
                    return
                }
                PromiseManager.associateTaskWithPromise(
                    task.id,
                    promiseId: promise.id,
                    success: { [weak self] (promise) in
                        guard let strongSelf = self else { return }
                        strongSelf.selectedPromise = promise
                        strongSelf.performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
                    },
                    failure: { [weak self] (error) in
                        guard let strongSelf = self else { return }
                        strongSelf.presentAlertViewController(error?.localizedFailureReason)
                    })
            } else {
                // 显示选取的Promise
                performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
            }
        } else if let familyActivity = option as? FamilyActivity {
            selectedFamilyActivity = familyActivity
            performSegue(withIdentifier: SegueIdentifier.familyActivity, sender: self)
        }
    }
    
    func clearFlags() {
        isAssociatingPromise = false
    }
    
    /// 生成前后30周的时间选项
    fileprivate func weeksArray() -> [CDMDate] {
        var dates = [CDMDate]()
        for i in (-30...30).reversed() {
            if let optionDate = i.weeks.ago(from: taskDate) {
                dates.append(CDMDate(date: optionDate))
            }
        }
        return dates
    }
}

// MARK: StudentProfileDelegate
extension HomeViewController: StudentProfileDelegate {
    func selectedStudent(_ student: Student) {
        if AppUserInfo.account.selectedStudentAccountFamily?.id != student.accountFamilyId {
            // load a new student's family account and set it into current teacher's account
            
            AccountManager.fetchAccountFamilyWithId(
                student.accountFamilyId,
                success: { [weak self] accountFamily in
                    guard let strongSelf = self else { return }
                    var account = AppUserInfo.account
                    account.selectedStudentAccountFamily = accountFamily
                    // Save changes locally
                    AppUserInfo.account = account
                    // Reload tasks
                    strongSelf.loadTasksIntoTimeline()
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController(error?.localizedFailureReason, showCancel: false, handler: {
                        if error?.code == 401 {
                            strongSelf.performSegue(withIdentifier: SegueIdentifier.login, sender: self)
                        }
                    })
                })
           
        }
    }
}

// MARK: TaskDelegate
extension HomeViewController: TaskDelegate {
    func updateTask(_ task: Task?) {
        guard let task = task,
            let taskWithView = taskWithViews[task.id]
            else { return }
        // update taskWithView in taskWithViews
        taskWithViews[task.id] = (task.id, task, taskWithView.taskClockView, taskWithView.taskDollView, taskWithView.taskDetailBarView)
        // update task in task views
        for view in timelineContentView.subviews {
            if let taskDollView = view as? TaskDollView, taskDollView.task?.id == task.id
            {
                taskDollView.task = task
            } else if let taskDetailBarView = view as? TaskDetailBarView, taskDetailBarView.task?.id == task.id
            {
                taskDetailBarView.taskDetailView.task = task
            }
        }
    }
}

// MARK: Debug

extension HomeViewController {
    fileprivate func printDebugInfo(_ message: String?) {
        DLog(message)
        DLog("scroll view frame = \(timelineScrollView.frame), scroll view bound = \(timelineScrollView.bounds)")
        DLog("content view frame = \(timelineContentView.frame), content view bound = \(timelineContentView.bounds)")
        DLog("timeline view frame = \(timelineView.frame), timeline view bound = \(timelineView.bounds)")
        DLog("clock view frame = \(clockView.frame), clock view bound = \(clockView.bounds)")
    }
}

// MARK: Alarm
extension HomeViewController {
//    private func playStopAlarm() {
//        if shouldPlayAlarm {
//            AudioServicesPlayAlertSoundWithCompletion(1005) {
//                self.playStopAlarm()
//            }
//        }
//    }
}
