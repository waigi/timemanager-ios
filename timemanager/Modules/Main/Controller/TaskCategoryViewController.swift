//
//  TaskCategoryViewController.swift
//  timemanager
//
//  Created by Can on 6/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol TaskCategoryViewDelegate {
    func taskCategoryView(_ taskCategoryView: TaskCategoryView, clickedButtonAtIndex index: Int)
}

class TaskCategoryViewController: UIViewController {
    
    var task: Task?
    var delegate: TaskCategoryViewDelegate?
    
    // MARK: - Private variables
    
    fileprivate var isInNavigationController: Bool {
        return presentingViewController?.presentedViewController is UINavigationController
    }
    
    // MARK: IBOutlets
    @IBOutlet weak var taskCategoryView: TaskCategoryView!
    @IBInspectable var autoDismisses: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        taskCategoryView.hightlightButtonIndex = task?.category.rawValue
    }
    
    @IBAction func actionButton(_ button: UIButton) {
        guard let index = taskCategoryView.indexOfButton(button) else { return }
        // privilege check
        if taskCategoryView.isPromise(button) && !(AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth) {
            return
        }
        delegate?.taskCategoryView(taskCategoryView, clickedButtonAtIndex: index)
        if !isInNavigationController && autoDismisses {
            dismissAlert(button)
        }
    }
    
    @IBAction func dismissAlert(_ sender: AnyObject?) {
        if presentingViewController != nil {
            self.dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
}
