//
//  ReportBarChartViewController.swift
//  timemanager
//
//  Created by Can on 13/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol ReportBarChartDelegate {
    /// credits to be rendered are updated
    func update(_ creditsInCycles: [CreditsInCycle])
    /// fiter conditions are updated
    func update(_ reportSearchCriteria: ReportSearchCriteria)
}

class ReportBarChartViewController: UIViewController, ReportBarChartDelegate {
    
    /// All credits searched with dates and cycle number
    var creditsInCycles = [CreditsInCycle]()
    /// Selected ReportSearchCriteria
    var reportSearchCriteria: ReportSearchCriteria!
    /// Bar Chart Height
    fileprivate var maxBarChartHeight: CGFloat = 196
    /// 所有周期的最大满分值
    fileprivate var maxFullMark: Double = 0
    /// 每一积分所对应的高度值
    fileprivate var heightPerCredit: CGFloat = 1// { return maxFullMark == 0 ? 0 : maxBarChartHeight / CGFloat(maxFullMark) }

    // MARK: IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshReport()
    }

    // MARK: ReportBarChartDelegate
    func update(_ creditsInCycles: [CreditsInCycle]) {
        self.creditsInCycles = creditsInCycles
        refreshReport()
    }
    
    func update(_ reportSearchCriteria: ReportSearchCriteria) {
        self.reportSearchCriteria = reportSearchCriteria
        refreshReport()
    }
    
    /// re-generate report with new conditions/values
    fileprivate func refreshReport() {
        // find max full mark value
        maxFullMark = creditsInCycles.reduce(0) { (maxFullMark, cycle) in
            let fullMark: Double
            if reportSearchCriteria.isBriefReport {
                fullMark = cycle.maxBriefRank
            } else if reportSearchCriteria.isSingleDetailReport {
                fullMark = cycle.maxSingleDetailRank
            } else {
                fullMark = cycle.maxDetailRank(of: reportSearchCriteria.creditType)
            }
            return maxFullMark > fullMark ? maxFullMark : fullMark
        }
        scrollView.contentSize = CGSize(width: max(CGFloat(creditsInCycles.count * 80), scrollView.frame.width), height: max(CGFloat(maxFullMark + 80) + ReportBarView.Constant.topCreditViewHeight + ReportBarView.Constant.bottomLineViewHeight, scrollView.frame.height))
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height)
        scrollView.setContentOffset(bottomOffset, animated: true)
        populateBars()
    }
    
    fileprivate func populateBars() {
        scrollView.subviews.forEach { (subview) in
            subview.removeFromSuperview()
        }
        for (index, creditsInCycle) in creditsInCycles.enumerated() {
            var heights: [CGFloat] = Array(repeating: 0.0, count: 11)
            var credit: Double = 0
            var fullmark: Double = 0
            if reportSearchCriteria.isBriefReport || reportSearchCriteria.isApplyDiscount,
                let creditType = reportSearchCriteria?.creditType
            {
                let maxRank = creditsInCycle.maxBriefRank
                let totalDetailRank = creditsInCycle.totalBriefRank(of: creditType, discounted: reportSearchCriteria.isApplyDiscount)
                credit = creditsInCycle.totalBriefRank(of: creditType, discounted: reportSearchCriteria.isApplyDiscount)
                fullmark = creditsInCycle.maxBriefRank
                if fullmark != 0 {
                    heights[9] = CGFloat(totalDetailRank) * heightPerCredit
                    heights[10] = CGFloat(maxRank - totalDetailRank) * heightPerCredit
                }
            } else if let creditType = reportSearchCriteria?.creditType {
                if reportSearchCriteria.isSingleDetailReport {
                    guard let detailCreditType = reportSearchCriteria?.detailCreditType else { return }
                    let maxRank = creditsInCycle.maxSingleDetailRank
                    let totalDetailRank = creditsInCycle.totalDetailRank(of: creditType, and: detailCreditType)
                    credit = totalDetailRank
                    fullmark = maxRank
                    if fullmark != 0 {
                        heights[detailCreditType] = CGFloat(totalDetailRank) * heightPerCredit
                        heights[10] = CGFloat(maxRank - totalDetailRank) * heightPerCredit
                    }
                } else {
                    let maxRank = creditsInCycle.maxDetailRank(of: creditType)
                    let totalDetailRank = creditsInCycle.totalDetailRank(of: creditType)
                    credit = totalDetailRank
                    fullmark = maxRank
                    if fullmark != 0 {
                        for index in 0...8 {
                            heights[index] = CGFloat(creditsInCycle.totalDetailRank(of: creditType, and: index)) * heightPerCredit
                        }
                        heights[10] = CGFloat(maxRank - totalDetailRank) * heightPerCredit
                    }
                }
            }
            let barContainerView = ReportBarView.reportBarView(
                heights,
                at: CGPoint(x: CGFloat(index * 80), y: scrollView.contentSize.height),
                cycle: index + 1,
                credit: credit,
                fullmark: fullmark)
            scrollView.addSubview(barContainerView)
        }
    }
    
}





