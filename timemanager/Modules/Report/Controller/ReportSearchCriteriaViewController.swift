//
//  ReportSearchCriteriaViewController.swift
//  timemanager
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol ReportSearchCriteriaDelegate {
    func update(_ reportSearchCriteria: ReportSearchCriteria)
}

class ReportSearchCriteriaViewController: UIViewController {
    
    var reportSearchCriteria: ReportSearchCriteria!
    var delegate: ReportSearchCriteriaDelegate?
    /// Cycle days options
    fileprivate let cycleDays = Array(1...90)
    fileprivate let textColor = UIColor(red: 34.0 / 255.0, green: 118.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    
    // MARK: IBOutlets

    @IBOutlet weak var cycleDaysPickerView: UIPickerView!
    @IBOutlet weak var fromDatePicker: UIDatePicker!
    @IBOutlet weak var toDatePicker: UIDatePicker!
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fromDatePicker.setValue(textColor, forKeyPath: "textColor")
        toDatePicker.setValue(textColor, forKeyPath: "textColor")
        fromDatePicker.addTarget(self, action: #selector(ReportSearchCriteriaViewController.fromDatePickerScrolled), for: .valueChanged)
        toDatePicker.addTarget(self, action: #selector(ReportSearchCriteriaViewController.toDatePickerScrolled), for: .valueChanged)
        updateUI()
    }
    
    // MARK: IBActions

    @IBAction func tappedConfirmButton() {
        reportSearchCriteria.startDate = fromDatePicker.date
        reportSearchCriteria.endDate = toDatePicker.date
        reportSearchCriteria.cycleDaysNumber = cycleDays[cycleDaysPickerView.selectedRow(inComponent: 0)]
        delegate?.update(reportSearchCriteria)
        dismiss(nil)
    }
    
    // MARK: UI
    
    fileprivate func updateUI() {
        fromDatePicker.date = reportSearchCriteria.startDate as Date
        toDatePicker.date = reportSearchCriteria.endDate as Date
        cycleDaysPickerView.selectRow(cycleDays.index(of: reportSearchCriteria.cycleDaysNumber) ?? 0, inComponent: 0, animated: true)
    }
    
    func fromDatePickerScrolled() {
        toDatePicker.minimumDate = fromDatePicker.date
    }
    
    func toDatePickerScrolled() {
        fromDatePicker.maximumDate = toDatePicker.date
    }

}

// MARK: Cycle days picker

extension ReportSearchCriteriaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cycleDays.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = "\(cycleDays[row])"
        pickerLabel.textColor = textColor
        pickerLabel.font = UIFont(name: "System", size: 10)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
}
