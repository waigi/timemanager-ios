//
//  ReportViewController.swift
//  timemanager
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {
    
    fileprivate struct SegueIdentifier {
        static let familyActivity = "familyActivity"
        static let promise = "promise"
        static let searchCriteria = "searchCriteria"
        static let barchart = "barchart"
    }
    
    // MARK: Variables
    
    /// Report查询条件
    fileprivate var reportSearchCriteria = ReportSearchCriteria() { didSet { reportSearchCriteriaLabel?.text = reportSearchCriteria.description } }
    /// 当前正在点击按钮的Promise
    fileprivate var selectedPromise: Promise?
    /// 当前正在点击按钮的FamilyActivity
    fileprivate var selectedFamilyActivity: FamilyActivity?
    /// A reference to the ReportBarChartViewController
    fileprivate var reportBarChartDelegate: ReportBarChartDelegate?
    
    // MARK: IBOutlets
    
    @IBOutlet weak var reportSearchCriteriaLabel: UILabel?
    @IBOutlet weak var fLegendView: UIView!
    @IBOutlet weak var iLegendView: UIView!
    @IBOutlet weak var kLegendView: UIView!
    @IBOutlet weak var eLegendView: UIView!
    @IBOutlet weak var aLegendView: UIView!
    @IBOutlet weak var creditTypeButtonContainerView: UIView!
    @IBOutlet weak var legendContainerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var functionsView: UIView!
    @IBOutlet weak var evaluationContainerView: UIView!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reportSearchCriteriaLabel?.text = reportSearchCriteria.description
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchCredits()
    }
    
    // MARK: UI
    
    fileprivate func toggleLegendView(_ button: UIButton) {
        legendContainerView.subviews.forEach({ (view) in
            view.isHidden = (view.tag != button.tag)
        })
    }
    
    // MARK: IBActions
    
    @IBAction func tappedCreditTypeButton(_ button: UIButton) {
        // update button state
        creditTypeButtonContainerView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        // update legend view
        toggleLegendView(button)
        reportSearchCriteria.creditType = Credit.CreditType(rawValue: button.tag) ?? .f
        reportBarChartDelegate?.update(reportSearchCriteria)
    }
    
    @IBAction func tappedChoosePromiseButton(_ sender: AnyObject) {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        PromiseManager.fetchPromisesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (promises) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(promises, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    
    @IBAction func tappedChooseFamilyActivityButton(_ sender: AnyObject) {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        FamilyActivityManager.fetchFamilyActivitiesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (familyActivities) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(familyActivities, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    @IBAction func tappedDetailReportButton(_ button: UIButton) {
        // privilege check
        guard AppUserInfo.account.isTeacherSupervisor else { return }
        // update button state
        functionsView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        reportSearchCriteria.reportType = .detail
        reportBarChartDelegate?.update(reportSearchCriteria)
    }
    
    @IBAction func tappedBriefReportButton(_ button: UIButton) {
        // update button state
        functionsView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        reportSearchCriteria.reportType = .brief
        reportBarChartDelegate?.update(reportSearchCriteria)
    }
    
    @IBAction func tappedDiscountReportButton(_ button: UIButton) {
        // update button state
        functionsView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        reportSearchCriteria.reportType = .discount
        reportBarChartDelegate?.update(reportSearchCriteria)
    }
    
    // MARK: Legend
    
    @IBAction func tappedLegendButton(_ button: UIButton) {
        reportSearchCriteria.detailCreditType = button.tag
        reportBarChartDelegate?.update(reportSearchCriteria)
    }

    @IBAction func tappedResetButton() {
        reportSearchCriteria.detailCreditType = nil
        reportBarChartDelegate?.update(reportSearchCriteria)
    }
    
    // MARK: Evaluation type
    
    @IBAction func tappedEvaluationButton(_ button: UIButton) {
        guard let evaluationType = Credit.EvaluationType(rawValue: button.tag) else { return }
        // privilege check
        if (evaluationType == .teacher) && !(AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth) {
            return
        }
        // update button state
        evaluationContainerView.subviews.forEach({ (view) in
            if let button = view as? UIButton {
                button.isEnabled = true
            }
        })
        button.isEnabled = false
        reportSearchCriteria.evaluationType = evaluationType
        searchCredits()
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        if identifier == SegueIdentifier.promise {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = hasPrivilege
        } else if identifier == SegueIdentifier.familyActivity {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent
            should = hasPrivilege
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.searchCriteria,
            let reportSearchCriteriaViewController = segue.destination as? ReportSearchCriteriaViewController
        {
            reportSearchCriteriaViewController.reportSearchCriteria = reportSearchCriteria
            reportSearchCriteriaViewController.delegate = self
        } else if segue.identifier == SegueIdentifier.barchart,
            let reportBarChartViewController = segue.destination as? ReportBarChartViewController
        {
            reportBarChartDelegate = reportBarChartViewController
            reportBarChartViewController.reportSearchCriteria = reportSearchCriteria
        } else if let navigationController = segue.destination as? UINavigationController,
            let familyActivityViewController = navigationController.topViewController as? FamilyActivityViewController, segue.identifier == SegueIdentifier.familyActivity
        {
            familyActivityViewController.familyActivity = selectedFamilyActivity
        } else if let navigationController = segue.destination as? UINavigationController,
            let promise2ViewController = navigationController.topViewController as? Promise2ViewController, segue.identifier == SegueIdentifier.promise
        {
            promise2ViewController.promise = selectedPromise
        }
    }
    
    // MARK: Service
    
    /// Search credits based on latest search criteria and refresh the bar charts accordingly
    fileprivate func searchCredits() {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        activityIndicator.startAnimating()
        CreditManager.searchCreditsInCycleWith(
            accountFamilyId,
            reportSearchCriteria: reportSearchCriteria,
            success: { [weak self] (creditsInCycles) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.reportBarChartDelegate?.update(creditsInCycles)
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }

}

// MARK: OptionDelegate
extension ReportViewController: OptionDelegate {
    /// OptionDelegate method
    func selectedOption(_ option: AnyObject) {
        if let promise = option as? Promise {
            selectedPromise = promise
            performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
        } else if let familyActivity = option as? FamilyActivity {
            selectedFamilyActivity = familyActivity
            performSegue(withIdentifier: SegueIdentifier.familyActivity, sender: self)
        }
    }
    
    func clearFlags() {}
}

// MARK: ReportSearchCriteriaDelegate
extension ReportViewController: ReportSearchCriteriaDelegate {
    func update(_ reportSearchCriteria: ReportSearchCriteria) {
        self.reportSearchCriteria = reportSearchCriteria
        searchCredits()
    }
}
