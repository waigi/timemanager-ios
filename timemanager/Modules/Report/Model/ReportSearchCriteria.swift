//
//  ReportSearchCriteria.swift
//  timemanager
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

class ReportSearchCriteria {
    /// 报表大类别，精，总，折
    enum ReportType : Int {
        case detail, brief, discount
    }
    
    /// 每个周期的天数，默认为1周
    var cycleDaysNumber = 7
    var startDate = Date()
    var endDate = Date()
    /// 积分类型FIKEA
    var creditType = Credit.CreditType.f
    /// 积分精评类型0-8
    var detailCreditType: Int?
    /// 选取的报表大类别，精，总，折
    var reportType: ReportType = .brief
    /// 评分人，“亲、自、师”，对应server端的‘graderType’
    var evaluationType: Credit.EvaluationType = .selfEvaluation
    
    /// 转换成格式"5   2016-10-28 ~ 2017-03-19"
    var description: String {
        return "\(cycleDaysNumber)   \(startDate.toCDMStandardString()) ~ \(endDate.toCDMStandardString())"
    }
    
    /// 是否显示简评报表
    var isBriefReport: Bool { return reportType == .brief }
    
    /// 是否显示精评中的某一子项的报表，如：E - 诚
    var isSingleDetailReport: Bool { return reportType == .detail && detailCreditType != nil }
    
    /// 是否加上折扣率
    var isApplyDiscount: Bool { return reportType == .discount }
    
}
