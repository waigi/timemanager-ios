//
//  CreditsInCycle.swift
//  timemanager
//
//  Created by Can on 18/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 搜索出的某一个周期内的指定类型的所有Credit对象，用于生成报表
struct CreditsInCycle {
    
    // MARK: Vars from server
    var credits = [Credit]()
    var startDate: Date?
    var endDate: Date?
    
    init?(json: JSON) {
        for subjson in json["credits"].arrayValue {
            if let credit = Credit(json: subjson) {
                credits.append(credit)
            }
        }
        if let startTimeInterval = json["startDate"].double {
            startDate = NSDate(timeIntervalSince1970: startTimeInterval / 1000.0) as Date
        }
        if let endTimeInterval = json["endDate"].double {
            endDate = NSDate(timeIntervalSince1970: endTimeInterval / 1000.0) as Date
        }
    }
    
    // MARK: Computed vars
    
    /// 选定类型(FIKEA)的简评满分
    var maxBriefRank: Double {
        return credits.reduce(0, { (total, credit) in
            total + 5 * Double(credit.weight)
        })
    }
    
    /// 选定类型(FIKEA)的精评满分
    func maxDetailRank(of creditType: Credit.CreditType) -> Double {
        return credits.reduce(0, { (total, credit) in
            total + (creditType == .e ? 45 * Double(credit.weight) : 30 * Double(credit.weight))
        })
    }
    
    /// 选定类型(FIKEA)的某一种精评满分
    var maxSingleDetailRank: Double {
        return credits.reduce(0, { (total, credit) in
            total + 5 * Double(credit.weight)
        })
    }
    
    /// 选定类型的简评总得分
    func totalBriefRank(of creditType: Credit.CreditType, discounted: Bool = false) -> Double {
        let rank: Double
        switch creditType {
        case .f:
            rank = credits.reduce(0, { (total, credit) in
                total + Double(credit.fTotal * credit.weight) * (discounted ? credit.discount : 1.0)
            })
        case .i:
            rank = credits.reduce(0, { (total, credit) in
                total + Double(credit.iTotal * credit.weight) * (discounted ? credit.discount : 1.0)
            })
        case .k:
            rank = credits.reduce(0, { (total, credit) in
                total + Double(credit.kTotal * credit.weight) * (discounted ? credit.discount : 1.0)
            })
        case .e:
            rank = credits.reduce(0, { (total, credit) in
                total + Double(credit.eTotal * credit.weight) * (discounted ? credit.discount : 1.0)
            })
        case .a:
            rank = credits.reduce(0, { (total, credit) in
                total + Double(credit.aTotal * credit.weight) * (discounted ? credit.discount : 1.0)
            })
        }
        return rank
    }
    
    /// 选定类型的精评总得分，如：F的6项精评总分
    func totalDetailRank(of creditType: Credit.CreditType, discounted: Bool = false) -> Double {
        let rank: Double
        switch creditType {
        case .f:
            rank = credits.reduce(0, { (total, credit) in
                total + credit.fDetails.reduce(0, { (total, detailCredit) in
                    total + Double(detailCredit.rank * credit.weight) * (discounted ? credit.discount : 1.0)
                })
            })
        case .i:
            rank = credits.reduce(0, { (total, credit) in
                total + credit.iDetails.reduce(0, { (total, detailCredit) in
                    total + Double(detailCredit.rank * credit.weight) * (discounted ? credit.discount : 1.0)
                })
            })
        case .k:
            rank = credits.reduce(0, { (total, credit) in
                total + credit.kDetails.reduce(0, { (total, detailCredit) in
                    total + Double(detailCredit.rank * credit.weight) * (discounted ? credit.discount : 1.0)
                })
            })
        case .e:
            rank = credits.reduce(0, { (total, credit) in
                total + credit.eDetails.reduce(0, { (total, detailCredit) in
                    total + Double(detailCredit.rank * credit.weight) * (discounted ? credit.discount : 1.0)
                })
            })
        case .a:
            rank = credits.reduce(0, { (total, credit) in
                total + credit.aDetails.reduce(0, { (total, detailCredit) in
                    total + Double(detailCredit.rank * credit.weight) * (discounted ? credit.discount : 1.0)
                })
            })
        }
        return rank
    }
    
    /// 获取指定子类型的精评总得分，如：F的6项精评中的某一项总分
    func totalDetailRank(of creditType: Credit.CreditType, and detailType: Int, discounted: Bool = false) -> Double {
        let rank: Double
        switch creditType {
        case .f:
            rank = credits.reduce(0.0, { (total, credit) in
                total + credit.fDetails.reduce(0.0, { (total, detailCredit) in
                    let discountRate = discounted ? credit.discount : 1.0
                    return detailCredit.type == detailType ? total + (Double(detailCredit.rank * credit.weight) * discountRate) : total
                })
            })
        case .i:
            rank = credits.reduce(0.0, { (total, credit) in
                total + credit.iDetails.reduce(0.0, { (total, detailCredit) in
                    let discountRate = discounted ? credit.discount : 1.0
                    return detailCredit.type == detailType ? total + (Double(detailCredit.rank * credit.weight) * discountRate) : total
                })
            })
        case .k:
            rank = credits.reduce(0.0, { (total, credit) in
                total + credit.kDetails.reduce(0.0, { (total, detailCredit) in
                    let discountRate = discounted ? credit.discount : 1.0
                    return detailCredit.type == detailType ? total + (Double(detailCredit.rank * credit.weight) * discountRate) : total
                })
            })
        case .e:
            rank = credits.reduce(0.0, { (total, credit) in
                total + credit.eDetails.reduce(0.0, { (total, detailCredit) in
                    let discountRate = discounted ? credit.discount : 1.0
                    return detailCredit.type == detailType ? total + (Double(detailCredit.rank * credit.weight) * discountRate) : total
                })
            })
        case .a:
            rank = credits.reduce(0.0, { (total, credit) in
                total + credit.aDetails.reduce(0.0, { (total, detailCredit) in
                    let discountRate = discounted ? credit.discount : 1.0
                    return detailCredit.type == detailType ? total + (Double(detailCredit.rank * credit.weight) * discountRate) : total
                })
            })
        }
        return rank
    }
    
}
