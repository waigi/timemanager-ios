//
//  ColorBarView.swift
//  timemanager
//
//  Created by Can on 13/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class ColorBarView: NibView {
    
    /// Type：-1为不存在，0-8为精评，9为简评，10为未得分项
    @IBInspectable var type: Int = 0 {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    var creditAmount: Double = 0.0
    
    @IBOutlet weak var whiteBarView: UIView!
    @IBOutlet weak var colorBarView: UIView!
    @IBOutlet weak var whiteBarHeight: NSLayoutConstraint!
    @IBOutlet weak var colorBarHeight: NSLayoutConstraint!
    
    fileprivate var color: UIColor {
        let color: UIColor
        switch type {
        case 0: color = UIColor(red: 129 / 255.0, green: 205 / 255.0, blue: 232 / 255.0, alpha: 1.0)
        case 1: color = UIColor(red: 176 / 255.0, green: 215 / 255.0, blue: 146 / 255.0, alpha: 1.0)
        case 2: color = UIColor(red: 226 / 255.0, green: 135 / 255.0, blue: 195 / 255.0, alpha: 1.0)
        case 3: color = UIColor(red: 202 / 255.0, green: 159 / 255.0, blue: 178 / 255.0, alpha: 1.0)
        case 4: color = UIColor(red: 164 / 255.0, green: 159 / 255.0, blue: 202 / 255.0, alpha: 1.0)
        case 5: color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 202 / 255.0, alpha: 1.0)
        case 6: color = UIColor(red: 159 / 255.0, green: 202 / 255.0, blue: 168 / 255.0, alpha: 1.0)
        case 7: color = UIColor(red: 192 / 255.0, green: 202 / 255.0, blue: 159 / 255.0, alpha: 1.0)
        case 8: color = UIColor(red: 202 / 255.0, green: 183 / 255.0, blue: 159 / 255.0, alpha: 1.0)
        case 9: color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 201 / 255.0, alpha: 1.0)
        case 10: color = UIColor(red: 219 / 255.0, green: 219 / 255.0, blue: 219 / 255.0, alpha: 1.0)
        default:
            color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 201 / 255.0, alpha: 1.0)
        }
        return color
    }
    
    class func instanceFromNib() -> ColorBarView {
        return UINib(nibName: "ColorBarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ColorBarView
    }
    
    override func updateView() {
        super.updateView()
        isHidden = (type == -1)
        colorBarView.backgroundColor = color
    }
    
}
