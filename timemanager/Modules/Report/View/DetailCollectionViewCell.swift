//
//  DetailCollectionViewCell.swift
//  timemanager
//
//  Created by Can on 19/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var barView: ReportBarView!
    @IBOutlet weak var cycleLabel: UILabel!
    
}
