//
//  ReportBarView.swift
//  timemanager
//
//  Created by Can on 13/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class ReportBarView: NibCellView {

    @IBOutlet weak var barsContainerView: UIView!
    @IBOutlet var barview0Height: NSLayoutConstraint?
    @IBOutlet var barview1Height: NSLayoutConstraint?
    @IBOutlet var barview2Height: NSLayoutConstraint?
    @IBOutlet var barview3Height: NSLayoutConstraint?
    @IBOutlet var barview4Height: NSLayoutConstraint?
    @IBOutlet var barview5Height: NSLayoutConstraint?
    @IBOutlet var barview6Height: NSLayoutConstraint?
    @IBOutlet var barview7Height: NSLayoutConstraint?
    @IBOutlet var barview8Height: NSLayoutConstraint?
    @IBOutlet var barview9Height: NSLayoutConstraint?
    @IBOutlet var barsContainerHeight: NSLayoutConstraint?
    @IBOutlet weak var bar9: ColorBarView!
    @IBOutlet weak var bar8: ColorBarView!
    @IBOutlet weak var bar7: ColorBarView!
    @IBOutlet weak var bar6: ColorBarView!
    @IBOutlet weak var bar5: ColorBarView!
    @IBOutlet weak var bar4: ColorBarView!
    @IBOutlet weak var bar3: ColorBarView!
    @IBOutlet weak var bar2: ColorBarView!
    @IBOutlet weak var bar1: ColorBarView!
    @IBOutlet weak var bar0: ColorBarView!
    
    var barHeight = [CGFloat](repeating: 0.0, count: 10) { didSet { setNeedsUpdateView() } }
    
    override func updateView() {
        super.updateView()
        barsContainerHeight?.constant = barHeight.reduce(0.0, { (total, height) in
            height != 0.0 ? total + height + 8 : total
        })
        layoutIfNeeded()
    }
    
}

/// Produce ReportBarView programmatically
extension ReportBarView {
    
    struct Constant {
        /// 每根柱子上的空白色条高度
        static let whiteSpaceHeight: CGFloat = 1.0
        /// 柱子宽度
        static let barWidth: CGFloat = 50
        /// 柱子每侧线条宽度
        static let barSideMargin: CGFloat = 15
        /// 底线及周期View高度
        static let bottomLineViewHeight: CGFloat = 38
        static let topCreditViewHeight:  CGFloat = 43
    }
    
    class func reportBarView(_ barHeight: [CGFloat], at origin: CGPoint, cycle: Int, credit: Double, fullmark: Double) -> UIView {
        let barView = barViewWith(barHeight, at: origin)
        let bottomLine = bottomLineView(CGRect(x: 0, y: Constant.topCreditViewHeight + barView.frame.height, width: Constant.barWidth + Constant.barSideMargin * 2, height: Constant.bottomLineViewHeight), cycle: cycle)
        let topCredit = topCreditView(CGRect(x: Constant.barSideMargin, y: 0, width: Constant.barWidth, height: Constant.topCreditViewHeight), credit: credit, fullmark: fullmark)
        let totalHeight = topCredit.frame.height + barView.frame.height + bottomLine.frame.height
        let containerView = UIView(frame: CGRect(x: origin.x, y: origin.y - totalHeight, width: bottomLine.frame.width, height: totalHeight))
        containerView.addSubview(topCredit)
        containerView.addSubview(barView)
        containerView.addSubview(bottomLine)
        return containerView
    }
    
    fileprivate class func topCreditView(_ frame: CGRect, credit: Double, fullmark: Double) -> UIView {
        let view = UIView(frame: frame)
        guard fullmark > 0 else {
            // 此周期没有评分，不现实内容
            return view
        }
        let creditLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: 21))
        let divider = UIView(frame: CGRect(x: 0, y: creditLabel.frame.maxY, width: frame.width, height: 1))
        let fullmarkLabel = UILabel(frame: CGRect(x: 0, y: divider.frame.maxY, width: frame.width, height: 21))
        creditLabel.textColor = UIColor.textColor()
        creditLabel.textAlignment = .center
        creditLabel.text = "\(credit.creditString)"
        divider.backgroundColor = UIColor.textColor()
        fullmarkLabel.textColor = UIColor.textColor()
        fullmarkLabel.textAlignment = .center
        fullmarkLabel.text = "\(fullmark.creditString)"
        view.addSubview(creditLabel)
        view.addSubview(divider)
        view.addSubview(fullmarkLabel)
        return view
    }
    
    fileprivate class func bottomLineView(_ frame: CGRect, cycle: Int) -> UIView {
        let view = UIView(frame: frame)
        let divider = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 1))
        divider.backgroundColor = UIColor.textColor()
        let label = UILabel(frame: CGRect(x: 0, y: divider.frame.maxY + 8, width: frame.width, height: 21))
        label.textColor = UIColor.textColor()
        label.textAlignment = .center
        label.text = "\(cycle)"
        view.addSubview(divider)
        view.addSubview(label)
        return view
    }
    
    /// 根据barHeight值生成柱状组图
    fileprivate class func barViewWith(_ barHeight: [CGFloat], at origin: CGPoint) -> UIView {
        let totalBarsHeight = barHeight.reduce(0.0, { (total, height) in
            height != 0.0 ? total + height + Constant.whiteSpaceHeight : total
        })
        let barsContainerView = UIView(frame: CGRect(x: Constant.barSideMargin, y: Constant.topCreditViewHeight, width: Constant.barWidth, height: totalBarsHeight))
        // y value of bars, starts from bottom of container view
        var currentY = totalBarsHeight
        for (index, height) in barHeight.enumerated() {
            if height > 0 {
                let barView = ColorBarViewMake(index, colorBarHeight: height, origin: CGPoint(x: 0, y: currentY))
                barsContainerView.addSubview(barView)
                currentY -= height + Constant.whiteSpaceHeight
            }
        }
        return barsContainerView
    }
    
    /// 生成单个柱状图
    fileprivate class func ColorBarViewMake(_ type: Int, colorBarHeight: CGFloat, origin: CGPoint) -> UIView {
        let height: CGFloat = Constant.whiteSpaceHeight + colorBarHeight
        let view = UIView(frame: CGRect(x: origin.x, y: origin.y - height, width: Constant.barWidth, height: height))
        let whiteBarView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: Constant.whiteSpaceHeight))
        let colorBarView = UIView(frame: CGRect(x: 0.0, y: Constant.whiteSpaceHeight, width: view.frame.width, height: colorBarHeight))
        colorBarView.backgroundColor = color(of: type)
        view.addSubview(whiteBarView)
        view.addSubview(colorBarView)
        
        return view
    }
    
    /// 生成制定类型的积分颜色
    fileprivate class func color(of type: Int) -> UIColor {
        let color: UIColor
        switch type {
        case 0: color = UIColor(red: 129 / 255.0, green: 205 / 255.0, blue: 232 / 255.0, alpha: 1.0)
        case 1: color = UIColor(red: 176 / 255.0, green: 215 / 255.0, blue: 146 / 255.0, alpha: 1.0)
        case 2: color = UIColor(red: 226 / 255.0, green: 135 / 255.0, blue: 195 / 255.0, alpha: 1.0)
        case 3: color = UIColor(red: 202 / 255.0, green: 159 / 255.0, blue: 178 / 255.0, alpha: 1.0)
        case 4: color = UIColor(red: 164 / 255.0, green: 159 / 255.0, blue: 202 / 255.0, alpha: 1.0)
        case 5: color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 202 / 255.0, alpha: 1.0)
        case 6: color = UIColor(red: 159 / 255.0, green: 202 / 255.0, blue: 168 / 255.0, alpha: 1.0)
        case 7: color = UIColor(red: 192 / 255.0, green: 202 / 255.0, blue: 159 / 255.0, alpha: 1.0)
        case 8: color = UIColor(red: 202 / 255.0, green: 183 / 255.0, blue: 159 / 255.0, alpha: 1.0)
        case 9: color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 201 / 255.0, alpha: 1.0)
        case 10: color = UIColor(red: 219 / 255.0, green: 219 / 255.0, blue: 219 / 255.0, alpha: 1.0)
        default:
            color = UIColor(red: 159 / 255.0, green: 187 / 255.0, blue: 201 / 255.0, alpha: 1.0)
        }
        return color
    }
    
}
