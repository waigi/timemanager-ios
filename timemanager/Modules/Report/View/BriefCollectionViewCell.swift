//
//  BriefCollectionViewCell.swift
//  timemanager
//
//  Created by Can on 19/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class BriefCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var briefBarView: ReportBriefBarView!
    @IBOutlet weak var cycleLabel: UILabel!
}
