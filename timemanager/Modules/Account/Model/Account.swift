//
//  Account.swift
//  timemanager
//
//  Created by Can on 30/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

enum Role: String {
    case student = "ROLE_STUDENT"
    case guardian = "ROLE_GUARDIAN"
    case teacher = "ROLE_TEACHER"
    case admin = "ROLE_ADMIN"
}

/// Currently Logged in User Account
class Account: NSObject, NSCoding {
    
    /// User ID
    var id: String
    /// Role of current logged in user
    var role: Role
    /// 当前登录用户所属的家庭账户或者当前选择的学生账号(如果Role为教师)
    var accountFamily: AccountFamily
    /// 当前登录用户为教师，则此列表中有所有所属的学生账号
    var studentsAccount = [AccountFamily]()
    /// Tasks belong to the Family Account
    var tasks = [Task]()
    /// 当前操作的学生的accountFamily，如果Role为学生或家长则跟accountFamily一样，如果Role为Teacher，则初始默认为第一个学生，并根据选择更改。此值并非从API获取并且不持久化到服务器。
    var selectedStudentAccountFamily: AccountFamily?
    /// 教师信息，如果此账户role为教师
    var teacher: Teacher?
    
    /// 是否学生
    var isStudent: Bool { return role == .student }
    /// 是否家长
    var isGuardian: Bool { return role == .guardian }
    /// 是否管理员
    var isAdmin: Bool { return role == .admin }
    /// 是否教师
    var isTeacher: Bool { return role == .teacher }
    /// 是否成长师
    var isTeacherGrowth: Bool { return teacher?.teacherType == .growth }
    /// 是否高级课程师
    var isTeacherSenior: Bool { return teacher?.teacherType == .senior }
    /// 是否中级课程师
    var isTeacherMid: Bool { return teacher?.teacherType == .mid }
    /// 是否初级课程师
    var isTeacherJunior: Bool { return teacher?.teacherType == .junior }
    /// 是否督导师
    var isTeacherSupervisor: Bool { return teacher?.teacherType == .supervisor }
    /// 头像
    var avatarURLString: String?
    
    /// 头像URL
    var avatarURL: URL? {
        return avatarURLString.flatMap { URL(string: $0) }
    }
    
    init(id: String, role: Role, accountFamily: AccountFamily, studentsAccount: [AccountFamily], selectedStudentAccountFamily: AccountFamily?, teacher: Teacher?, avatarURLString: String?) {
        self.id = id
        self.role = role
        self.accountFamily = accountFamily
        self.studentsAccount = studentsAccount
        self.selectedStudentAccountFamily = selectedStudentAccountFamily
        self.teacher = teacher
        self.avatarURLString = avatarURLString
    }
    
    init?(json: JSON) {
        guard let role = Role(rawValue: json["authorities"][0].stringValue) else { return nil }
        self.role = role
        switch role {
        case .student:
            guard let id = json["student"]["id"].string else { return nil }
            guard let accountFamily = AccountFamily(json: json["student"]["accountFamily"]) else {return nil }
            self.id = id
            self.accountFamily = accountFamily
            selectedStudentAccountFamily = accountFamily
        case .guardian:
            guard let id = json["guardian"]["id"].string else { return nil }
            guard let accountFamily = AccountFamily(json: json["guardian"]["accountFamily"]) else {return nil }
            self.id = id
            self.accountFamily = accountFamily
            selectedStudentAccountFamily = accountFamily
        case .teacher:
            guard let id = json["teacher"]["id"].string else { return nil }
            guard let accountFamily = AccountFamily(json: json["teacher"]["accountFamilies"][0]) else {return nil }
            self.id = id
            // set first family account as the default selected family account
            self.accountFamily = accountFamily
            // load all student accounts
            if let jsons = json["teacher"]["accountFamilies"].array {
                for json in jsons {
                    if let account = AccountFamily(json: json) {
                        studentsAccount.append(account)
                    }
                }
            }
            // load teacher
            teacher = Teacher(json: json["teacher"])
            selectedStudentAccountFamily = studentsAccount.first
        case .admin:
            //TODO: confirm below JSON is correct
            guard let id = json["admin"]["id"].string else { return nil }
            guard let accountFamily = AccountFamily(json: json["admin"]["accountFamilies"][0]) else {return nil }
            self.id = id
            // set first family account as the default selected family account
            self.accountFamily = accountFamily
        }
        avatarURLString = json["photoURL"].string
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let role = Role(rawValue: aDecoder.decodeObject(forKey: "role") as! String)!
        let accountFamily = aDecoder.decodeObject(forKey: "accountFamily") as! AccountFamily
        let studentsAccount = aDecoder.decodeObject(forKey: "studentsAccount") as! [AccountFamily]
        let selectedStudentAccountFamily = aDecoder.decodeObject(forKey: "selectedStudentAccountFamily") as? AccountFamily
        let teacher = aDecoder.decodeObject(forKey: "teacher") as? Teacher
        let avatarURLString = aDecoder.decodeObject(forKey: "avatarURLString") as? String
        self.init(id: id, role: role, accountFamily: accountFamily, studentsAccount: studentsAccount, selectedStudentAccountFamily: selectedStudentAccountFamily, teacher: teacher, avatarURLString: avatarURLString)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(role.rawValue, forKey: "role")
        aCoder.encode(accountFamily, forKey: "accountFamily")
        aCoder.encode(studentsAccount, forKey: "studentsAccount")
        aCoder.encode(selectedStudentAccountFamily, forKey: "selectedStudentAccountFamily")
        aCoder.encode(teacher, forKey: "teacher")
        if let avatarURLString = avatarURLString {
            aCoder.encode(avatarURLString, forKey: "avatarURLString")
        }
    }
    
}
