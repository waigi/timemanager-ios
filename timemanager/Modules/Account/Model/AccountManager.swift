//
//  AccountManager.swift
//  timemanager
//
//  Created by Can on 2/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct UserDefaultsKey {
    static let account = "account"
}

class AccountManager {
    
    fileprivate struct EndPoint {
        static let getAccountFamilies = "api/account-families"
        static let getAccountFamilyWithId = "api/account-families/%@"
        static let getAccount = "api/account"
        static let getTeachersProfile = "api/account-families/getAccountFamilyInfoDTO"
        static let getManagedStudents = "api/student-infos/byTeacherId"
        static let getExpiryDate = "api/account-families/getExpiryDate"
    }
    
    /// Get AccountFamily with accountFamilyId
    static func fetchAccountFamilyWithId(_ accountFamilyId: String, success: @escaping ((AccountFamily?) -> Void), failure: @escaping ((NSError?) -> Void)) {
//        let parameters: [String: AnyObject] = ["id": accountFamilyId]
//        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + String(format: EndPoint.getAccountFamilyWithId, arguments: [accountFamilyId]),
            method: .get,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                        DLog("Fetched family account = \(data)")
                        let json = JSON(data)
                        let accountFamily = AccountFamily(json: json["accountFamily"])
                        success(accountFamily)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据token获取后台当前登录用户的Account对象，返回值带有student，guardian，或teacher对象，并包括AccountFamilyId在其中
    static func fetchAccount(_ success: @escaping ((Void) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.getAccount,
            method: .get,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("received account = \(data)")
                    let json = JSON(data)
                    if let account = Account(json: json) {
                        AppUserInfo.account = account
                    }
                    success()
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据当前教师的teacherId获取其管理的学生
    static func fetchManagedStudentsWithTeacherId(_ teacherId: String, success: @escaping (([Student]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["teacher_id": teacherId as AnyObject]
        Alamofire.request(
            Api.baseURL + EndPoint.getManagedStudents,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    var students = [Student]()
                    DLog("Fetched Students = \(data)")
                    let json = JSON(data)
                    for (_, subjson): (String, JSON) in json {
                        if let student = Student(json: subjson) {
                            students.append(student)
                        }
                    }
                    success(students)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据当前学生的accountFamilyId获取其家长和教师信息
    static func fetchTeachersWithAccountFamilyId(_ accountFamilyId: String, success: @escaping (([Guardian], [Teacher]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["accountFamily_id": accountFamilyId as AnyObject]
        Alamofire.request(
            Api.baseURL + EndPoint.getTeachersProfile,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    var guardians = [Guardian]()
                    var teachers = [Teacher]()
                    DLog("Fetched Teachers and Guardians = \(data)")
                    let json = JSON(data)
                    for (_, subjson): (String, JSON) in json["guardians"] {
                        if let guardian = Guardian(json: subjson) {
                            guardians.append(guardian)
                        }
                    }
                    for (_, subjson): (String, JSON) in json["teachers"] {
                        if let teacher = Teacher(json: subjson) {
                            teachers.append(teacher)
                        }
                    }
                    success(guardians, teachers)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据当前学生的accountFamilyId获取账户到期信息
    static func fetchExpiryDateWith(_ accountFamilyId: String, success: @escaping ((Int?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["account_family_id": accountFamilyId as AnyObject]
        Alamofire.request(
            Api.baseURL + String(format: EndPoint.getExpiryDate, arguments: [accountFamilyId]),
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched Expiry Date = \(data)")
                    let json = JSON(data)
                    let days = json["days"].int
                    success(days)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
}
