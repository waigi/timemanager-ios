//
//  AccountFamily.swift
//  timemanager
//
//  Created by Can on 10/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 家庭账户，每个学员对应一个家庭账户。
class AccountFamily: NSObject, NSCoding {
    
    // MARK: Vars
    
    /// 家庭账户ID
    var id: String
    /// 家庭账户名称
    var name: String?
    /// 家庭账户描述
    var accountFamilyDescription: String?
    /// 学生头像
    var studentAvatarURLString: String?
    
    /// 学生头像URL
    var studentAvatarURL: URL? {
        return studentAvatarURLString.flatMap { URL(string: $0) }
    }
    
    init(id: String, name: String?, accountFamilyDescription: String?, studentAvatarURLString: String?) {
        self.id = id
        self.name = name
        self.accountFamilyDescription = accountFamilyDescription
        self.studentAvatarURLString = studentAvatarURLString
    }
    
    init?(json: JSON) {
        guard let id = json["id"].string else { return nil }
        self.id = id
        name = json["name"].string
        accountFamilyDescription = json["description"].string
        studentAvatarURLString = json["studentAvatarURL"].string
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let studentAvatarURLString = aDecoder.decodeObject(forKey: "studentAvatarURLString") as? String
        let accountFamilyDescription = aDecoder.decodeObject(forKey: "accountFamilyDescription") as? String
        self.init(id: id, name: name, accountFamilyDescription: accountFamilyDescription, studentAvatarURLString: studentAvatarURLString)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        if let name = name {
            aCoder.encode(name, forKey: "name")
        }
        if let studentAvatarURLString = studentAvatarURLString {
            aCoder.encode(studentAvatarURLString, forKey: "studentAvatarURLString")
        }
        if let accountFamilyDescription = accountFamilyDescription {
            aCoder.encode(accountFamilyDescription, forKey: "accountFamilyDescription")
        }
    }
    
}
