//
//  TaskManager.swift
//  timemanager
//
//  Created by Can on 5/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftDate

class TaskManager {
    
    fileprivate struct EndPoint {
        static let getTasks = "api/tasks/familyTasks/"
        static let updateTask = "api/tasks/update/"
        static let updateTaskTime = "api/tasks/updateAllTime"
        static let generateTask = "api/tasks/generateTask/"
        static let upload = "api/tasks/upload/"
        static let deleteTask = "api/tasks/"
    }
    
    /// 根据AccessToken获取当前登录用户所属的某天所有Task, 默认为Today
    static func fetchTasksWithAccountFamilyId(_ accountFamilyId: String, onDate date: Date, success: @escaping (([Task]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let startOfDate = date.startOfDay
        let endOfDate = date.endOfDay
        let parameters: [String: AnyObject] = ["accountFamily_id": accountFamilyId as AnyObject,
                                               "planned_startTime": String(Int64(startOfDate.timeIntervalSince1970 * 1000)) as AnyObject,
                                               "planned_endTime": String(Int64(endOfDate.timeIntervalSince1970 * 1000)) as AnyObject]
        DLog("parameters: \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.getTasks,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched Tasks = \(data)")
                    var tasks = [Task]()
                    if let data = response.result.value {
                        let json = JSON(data)
                        for (_, subjson):(String, JSON) in json {
                            if let task = Task(json: subjson) {
                                tasks.append(task)
                            }
                        }
                    }
                    success(tasks)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据AccessToken生成当前登录用户的Task
    static func generateTaskWithAccountFamilyId(_ accountFamilyId: String, success: @escaping ((Task?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.generateTask + "\(accountFamilyId)",
            method: .post,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Generated Task = \(data)")
                    let json = JSON(data)
                    let task = Task(json: json)
                    success(task)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 更新Task中有内容的字段
    static func updateTask(_ task: Task, success: @escaping ((Task?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let nPhotoArray: [[String: AnyObject]] = task.nPhotos.flatMap() { return $0.dictionary }
        let jPhotoArray: [[String: AnyObject]] = task.jPhotos.flatMap() { return $0.dictionary }
        let nTextsArray: [[String: AnyObject]] = task.nTexts.flatMap() { return $0.dictionary }
        let jTextsArray: [[String: AnyObject]] = task.jTexts.flatMap() { return $0.dictionary }
        let nAudiosArray: [[String: AnyObject]] = task.nAudios.flatMap() { return $0.dictionary }
        let jAudiosArray: [[String: AnyObject]] = task.jAudios.flatMap() { return $0.dictionary }
        let jVideosArray: [[String: AnyObject]] = task.jVideos.flatMap() { return $0.dictionary }
        var parameters: [String: AnyObject] = [
                                                "id": task.id as AnyObject,
                                                "plannedStartTime": String(Int64(task.plannedStartTime.timeIntervalSince1970 * 1000)) as AnyObject,
                                                "plannedEndTime": String(Int64(task.plannedEndTime.timeIntervalSince1970 * 1000)) as AnyObject,
                                                "nPhotos": nPhotoArray as AnyObject,
                                                "nTexts": nTextsArray as AnyObject,
                                                "nAudios": nAudiosArray as AnyObject,
                                                "jPhotos": jPhotoArray as AnyObject,
                                                "jTexts": jTextsArray as AnyObject,
                                                "jAudios": jAudiosArray as AnyObject,
                                                "jVideos": jVideosArray as AnyObject,
                                                "jDemeritTexts": task.jDemerits as AnyObject,
                                                "taskCategory": task.category.rawValue as AnyObject
                                              ]
        
        if let pGuardianText = task.pGuardianText {
            parameters["pGuardianText"] = pGuardianText as AnyObject?
        }
        if let pSelfText = task.pSelfText {
            parameters["pSelfText"] = pSelfText as AnyObject?
        }
        if let pTeacherText = task.pTeacherText {
            parameters["pTeacherText"] = pTeacherText as AnyObject?
        }
        if let actualStartTime = task.actualStartTime {
            parameters["actualStartTime"] = String(Int64(actualStartTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        if let actualEndTime = task.actualEndTime {
            parameters["actualEndTime"] = String(Int64(actualEndTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        DLog("parameters = \(parameters)")
        DLog("Original Task = \(task)")
        Alamofire.request(
            Api.baseURL + EndPoint.updateTask,
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Task = \(data)")
                    let json = JSON(data)
                    let task = Task(json: json)
                    success(task)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 更新Task的时间
    static func updateTaskTime(_ taskId: String, plannedStartTime: Date?, plannedEndTime: Date?, actualStartTime: Date?, actualEndTime: Date?, success: @escaping ((Task?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: [String: AnyObject] = ["id": taskId as AnyObject]
        if let plannedStartTime = plannedStartTime {
            parameters["plannedStartTime"] = String(Int64(plannedStartTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        if let plannedEndTime = plannedEndTime {
            parameters["plannedEndTime"] = String(Int64(plannedEndTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        if let actualStartTime = actualStartTime {
            parameters["actualStartTime"] = String(Int64(actualStartTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        if let actualEndTime = actualEndTime {
            parameters["actualEndTime"] = String(Int64(actualEndTime.timeIntervalSince1970 * 1000)) as AnyObject?
        }
        DLog("updateTaskTime parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.updateTaskTime,
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Task = \(data)")
                    let json = JSON(data)
                    let task = Task(json: json)
                    success(task)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 上传图片
    static func uploadTaskPhoto(_ taskId: String, taskType: TaskType, fileURL: URL, success: @escaping ((Task) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters = [
            "task_id": taskId,
            "task_type": taskType.rawValue,
            "file_type": FileType.photo.rawValue]
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileURL, withName: "file")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
            to: Api.baseURL + EndPoint.upload,
            headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let value = response.result.value,
                            let task = Task(json: JSON(value))
                        {
                            success(task)
                        } else {
                            failure(ErrorManager.errorWithMessage("上传图片失败，请重试"))
                        }
                    }
                case .failure(let encodingError):
                    DLog(encodingError)
                    failure(ErrorManager.errorWithMessage("上传图片失败，请检查网络"))
                }
        }
    }
    
    /// 上传音频
    static func uploadTaskAudio(_ taskId: String, taskType: TaskType, fileURL: URL, audioDurationSeconds: Int, success: @escaping ((Task) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters = [
            "task_id": taskId,
            "task_type": taskType.rawValue,
            "file_type": FileType.audio.rawValue,
            "audio_length": "\(audioDurationSeconds)"]
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileURL, withName: "file")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
            to: Api.baseURL + EndPoint.upload,
            headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let value = response.result.value,
                            let task = Task(json: JSON(value))
                        {
                            success(task)
                        } else {
                            failure(ErrorManager.errorWithMessage("上传音频失败，请重试"))
                        }
                    }
                case .failure(let encodingError):
                    DLog(encodingError)
                    failure(ErrorManager.errorWithMessage("上传音频失败，请检查网络"))
                }
        }
    }
    
    /// 上传视频
    static func uploadTaskJVideo(_ taskId: String, fileURL: URL, success: @escaping ((Task) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters = [
            "task_id": taskId,
            "task_type": TaskType.j.rawValue,
            "file_type": FileType.video.rawValue]
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileURL, withName: "file")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
            to: Api.baseURL + EndPoint.upload,
            headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let value = response.result.value,
                            let task = Task(json: JSON(value))
                        {
                            success(task)
                        } else {
                            failure(ErrorManager.errorWithMessage("上传视频失败，请重试"))
                        }
                    }
                case .failure(let encodingError):
                    DLog(encodingError)
                    failure(ErrorManager.errorWithMessage("上传视频失败，请检查网络"))
                }
        }
    }
    
    /// 删除Task
    static func deleteTask(_ task: Task, success: @escaping ((Bool) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.deleteTask + task.id,
            method: .delete,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    success(JSON(data)["success"].boolValue)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }

}
