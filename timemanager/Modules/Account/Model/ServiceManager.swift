//
//  ServiceManager.swift
//  beta
//
//  Created by Can on 11/04/2016.
//  Copyright © 2016 Optus. All rights reserved.
//

import Foundation

let ServiceManagerErrorDomain = "com.waigi.ServiceManager"

enum RequestAuthorisationType: Int {
    case none = 0
    case basic = 1
    case oauth = 2
}

enum ContentType: String {
    case Form = "application/x-www-form-urlencoded"
    case JSON = "application/json"
    case PNG = "*"
    case Multipart
}

struct MultiPartFormType {
    static let boundary = "Boundary-\(UUID().uuidString)"
}

// TODO: Remove hardcoded URLs
struct Api {
    static let baseURL = "https://api.vigo-2007.com:8443/cdm/"
    static let key = "Y2RtYXBwOm15LXNlY3JldC10b2tlbi10by1jaGFuZ2UtaW4tcHJvZHVjdGlvbg=="
}

let ServiceManagerDidInalidateAuthTokenNotification = "com.waigi.cdm.ServiceManagerDidInalidateAuthTokenNotification."

var headers: [String: String] {
    return [
            "Authorization": ServiceManager.authorisationStringForAuthType(.oauth),
            "Accept": ContentType.JSON.rawValue
            ]
}

class ServiceManager {
    
    struct Payload {
        static let accountLocked = "User account is locked. Please retry after 35 mins."
    }
    
    struct ErrorMessage {
        static let invalidEmployeeID = "Invalid Employee ID.".localized
        static let noInternetConnection = "No internet connection available.".localized
        static let requestTimeout = "Request has been timed out. Please try again".localized
        static let invalidData = "Invalid data.".localized
        static let internalServerError = "Something went wrong. Please try again later".localized
        static let invalidCredentials = "Invalid Login Name, Password or Employee id".localized
        static let accessDenied = "Access Denied".localized
        static let unknownError = "The operation failed due to an unknown error.".localized
    }
    
    class func httpRequest(_ request: URLRequest, completion: @escaping (Data?, NSError?) -> Void) {
        var returnedError: NSError?
        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
             if let error = error as? NSError {
                returnedError = self.handleErrorResponse(false, data: data, errorCode: error.code)
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if !(200 ... 299 ~= httpResponse.statusCode) {
                        if 401 == httpResponse.statusCode && request.value(forHTTPHeaderField: "Authorization")!.contains("Bearer") {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: ServiceManagerDidInalidateAuthTokenNotification), object: nil)
                        } else if 500 == httpResponse.statusCode {
                            // TODO: handle error
                        }
                        returnedError = self.handleErrorResponse(true, data: data, errorCode: httpResponse.statusCode)
                    }
                }
            }
            completion(data, returnedError)
        }) 
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            dataTask.resume()
        }
    }
    
    class func generateRequest(_ endpoint: String, requestType: String, requestContentType: ContentType, requestAcceptType: ContentType, authorisationType: RequestAuthorisationType, requestHttpBody: Data?) -> NSMutableURLRequest {
        let requestUrl = URL(string: Api.baseURL + endpoint)!
        let request = NSMutableURLRequest(url: requestUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        
        request.httpMethod = requestType
        request.setValue(authorisationStringForAuthType(authorisationType), forHTTPHeaderField: "Authorization")
        if let requestHttpBody = requestHttpBody {
            request.setValue(String(requestHttpBody.count), forHTTPHeaderField: "Content-Length")
            request.httpBody = requestHttpBody
        }
        
        if requestContentType == .Multipart {
            let multiPartFormContentType = "multipart/form-data; boundary=\(MultiPartFormType.boundary)"
            request.setValue(multiPartFormContentType, forHTTPHeaderField: "Accept")
            request.setValue(multiPartFormContentType, forHTTPHeaderField: "Content-Type")
        } else {
            request.setValue(requestAcceptType.rawValue, forHTTPHeaderField: "Accept")
            request.setValue(requestContentType.rawValue, forHTTPHeaderField: "Content-Type")
        }
        return request
    }
    
    class func authorisationStringForAuthType(_ authType: RequestAuthorisationType) -> String {
        var authString = ""
        let tokenValue = AppUserInfo.accessToken
        
        switch (authType) {
        case .none:
            break
        case .basic:
            authString = "Basic \(Api.key)"
        case .oauth:
            authString = "Bearer \(tokenValue)"
        }
        
        return authString
    }
    
    class func handleErrorResponse(_ hasHttpResponseErrors: Bool, data: Data?, errorCode: Int) -> NSError {
        return handleErrorResponse(hasHttpResponseErrors, data: data, errorCode: errorCode, errorMessage: nil)
    }
    
    class func handleErrorResponse(_ hasHttpResponseErrors: Bool, data: Data?, errorCode: Int, errorMessage: String?) -> NSError {
        var errorString: String!
        
        if let data = data, hasHttpResponseErrors {
            do {
                if let decodedJson = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                    errorString = decodedJson["error_description"]!
                }
            } catch let jsonError as NSError {
                return jsonError
            }
        }
        
        // TODO: Standardise common error codes across application.
        switch errorCode {
        case -1002:
            errorString = ErrorMessage.invalidEmployeeID
        case -1009:
            errorString = ErrorMessage.noInternetConnection
        case -1001:
            errorString = ErrorMessage.requestTimeout
        case -1000:
            errorString = ErrorMessage.invalidData
        case 500:
            errorString = ErrorMessage.internalServerError
        case 400 where errorString != Payload.accountLocked:
            errorString = ErrorMessage.invalidCredentials
        case 401:
            errorString = ErrorMessage.accessDenied
        default:
            errorString = ErrorMessage.unknownError
        }
        
        if let errorMessage = errorMessage {
            errorString = errorMessage
        }
        
        return NSError(domain: ServiceManagerErrorDomain, code: errorCode, userInfo: [NSLocalizedDescriptionKey: errorString])
    }
}
