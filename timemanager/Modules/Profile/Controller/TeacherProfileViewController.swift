//
//  TeacherProfileViewController.swift
//  timemanager
//
//  Created by Can on 9/1/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit

private let reuseIdentifier = "teacher"

class TeacherProfileViewController: UICollectionViewController {
    
    enum ProfileType {
        case guardian, growthTeacher, lessonTeacher, supervisor
    }
    
    var teachers = [Teacher]()
    var guardians = [Guardian]()
    var profileType: ProfileType?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // MARK: Life Cycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshData()
    }
    
    fileprivate func refreshData() {
        guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
            presentAlertViewController("没有选择的学生")
            return
        }
        activityIndicator.startAnimating()
        AccountManager.fetchTeachersWithAccountFamilyId(
            selectedStudentAccountFamily.id,
            success: { [weak self] (guardians, teachers) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.guardians = guardians
                strongSelf.teachers = teachers
                strongSelf.collectionView?.reloadData()
            },
            failure: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let profileType = profileType {
            switch profileType {
            case .supervisor:
                return teachers.filter({ $0.teacherType == .supervisor }).count
            case .lessonTeacher:
                return teachers.filter({
                    let lessonTeacher = ($0.teacherType == .senior) || ($0.teacherType == .mid) || ($0.teacherType == .junior)
                    return lessonTeacher
                }).count
            case .growthTeacher:
                return teachers.filter({ $0.teacherType == .growth }).count
            case .guardian:
                return guardians.count
            }
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StudentCell
        if let profileType = profileType {
            switch profileType {
            case .supervisor:
                let teacher = teachers.filter({ $0.teacherType == .supervisor })[indexPath.item]
                if let url = teacher.photoURL {
                    cell.avatarImageView.kf.setImage(with: url)
                }
                cell.nameLabel.text = teacher.name
            case .lessonTeacher:
                let teacher = teachers.filter({
                    let lessonTeacher = ($0.teacherType == .senior) || ($0.teacherType == .mid) || ($0.teacherType == .junior)
                    return lessonTeacher
                })[indexPath.item]
                if let url = teacher.photoURL {
                    cell.avatarImageView.kf.setImage(with: url)
                }
                cell.nameLabel.text = teacher.name
            case .growthTeacher:
                let teacher = teachers.filter({ $0.teacherType == .growth })[indexPath.item]
                if let url = teacher.photoURL {
                    cell.avatarImageView.kf.setImage(with: url)
                }
                cell.nameLabel.text = teacher.name
            case .guardian:
                let guardian = guardians[indexPath.item]
                if let url = guardian.photoURL {
                    cell.avatarImageView.kf.setImage(with: url)
                }
                cell.nameLabel.text = guardian.name
            }
        }
        return cell
    }

}
