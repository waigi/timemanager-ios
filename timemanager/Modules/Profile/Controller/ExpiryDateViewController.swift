//
//  ExpiryDateViewController.swift
//  timemanager
//
//  Created by Can on 13/1/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit

class ExpiryDateViewController: UITableViewController {

    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Days to Expiry day of current student account
    fileprivate var days = 0
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadExpiryDate()
    }
    
    fileprivate func loadExpiryDate() {
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        activityIndicator.startAnimating()
        AccountManager.fetchExpiryDateWith(
            accountFamilyId,
            success: { [weak self] (days) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                if let days = days {
                    strongSelf.daysLabel.text = String(days)
                }
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
            })
    }
}
