//
//  StudentProfileViewController.swift
//  timemanager
//
//  Created by Can on 13/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import Kingfisher

protocol StudentProfileDelegate {
    func selectedStudent(_ student: Student)
}

private let reuseIdentifier = "student"

class StudentProfileViewController: UICollectionViewController {
    
    var delegate: StudentProfileDelegate?
    var students = [Student]()
    
    fileprivate var currentStudentAccountFamilyId: String? {
        return AppUserInfo.account.selectedStudentAccountFamily?.id
    }

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return students.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StudentCell
        let student = students[indexPath.item]
        if let url = student.photoURL {
            cell.avatarImageView.kf.setImage(with: url)
        }
        cell.nameLabel.text = student.name
        if student.accountFamilyId == currentStudentAccountFamilyId {
            cell.contentView.backgroundColor = UIColor.orange
        }
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectedStudent(students[indexPath.item])
        dismiss(collectionView)
    }

}
