//
//  Guardian.swift
//  timemanager
//
//  Created by Can on 13/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 家长的profile
class Guardian: NSObject {
    
    // MARK: variables
    
    var name: String?
    var photoURLString: String?
    var relationship: String?
    
    // MARK: computed variables
    
    var photoURL: URL? {
        return photoURLString.flatMap { URL(string: $0) }
    }
    
    // MARK: init
    
    init?(json: JSON) {
        name = json["name"].string
        photoURLString = json["photoURL"].string
        relationship = json["relationship"].string
    }
    
}
