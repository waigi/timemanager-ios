//
//  StudentCell.swift
//  timemanager
//
//  Created by Can on 13/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class StudentCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
