//
//  Teacher.swift
//  timemanager
//
//  Created by Can on 13/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 教师的profile
class Teacher: NSObject, NSCoding {
    
    enum TeacherType: Int {
        case growth //成长师
        case senior //高级课程师
        case mid    //中级课程师
        case junior //初级课程师
        case supervisor // 督导师
    }
    
    // MARK: variables
    
    var name: String?
    var photoURLString: String?
    var teacherTypeInt: Int?
    
    // MARK: computed variables
    
    var photoURL: URL? {
        return photoURLString.flatMap { URL(string: $0) }
    }
    
    var teacherType: TeacherType {
        return teacherTypeInt.flatMap { TeacherType(rawValue: $0) } ?? .growth
    }
    
    // MARK: init
    
    init(name: String?, photoURLString: String?, teacherTypeInt: Int?) {
        self.name = name
        self.photoURLString = photoURLString
        self.teacherTypeInt = teacherTypeInt
    }
    
    init?(json: JSON) {
        name = json["name"].string
        photoURLString = json["photoURL"].string
        teacherTypeInt = json["teacherType"].int
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let photoURLString = aDecoder.decodeObject(forKey: "photoURLString") as? String
        let teacherTypeInt = aDecoder.decodeInteger(forKey: "teacherTypeInt")
        self.init(name: name, photoURLString: photoURLString, teacherTypeInt: teacherTypeInt)
    }
    
    func encode(with aCoder: NSCoder) {
        if let name = name {
            aCoder.encode(name, forKey: "name")
        }
        if let photoURLString = photoURLString {
            aCoder.encode(photoURLString, forKey: "photoURLString")
        }
        if let teacherTypeInt = teacherTypeInt {
            aCoder.encode(teacherTypeInt, forKey: "teacherTypeInt")
        }
    }
}
