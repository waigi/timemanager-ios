//
//  Student.swift
//  timemanager
//
//  Created by Can on 13/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

/// 学生的profile
class Student: NSObject {
    
    // MARK: variables
    
    var accountFamilyId: String
    var accountFamilyName: String?
    var name: String?
    var photoURLString: String?
    
    // MARK: computed variables
    
    var photoURL: URL? {
        return photoURLString.flatMap { URL(string: $0) }
    }
    
    // MARK: init
    
    init(id: String) {
        self.accountFamilyId = id
    }
    
    init?(json: JSON) {
        guard let id = json["accountFamilyId"].string else { return nil }
        self.accountFamilyId = id
        accountFamilyName = json["accountFamilyName"].string
        name = json["name"].string
        photoURLString = json["photoURL"].string
    }

}
