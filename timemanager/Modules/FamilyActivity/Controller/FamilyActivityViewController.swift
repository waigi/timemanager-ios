//
//  FamilyActivityViewController.swift
//  timemanager
//
//  Created by Can on 2/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVKit

protocol FamilyActivityDelegate {
    func updateSelectedImage(_ image: UIImage?)
    func updateSelectedVideo(_ video: Video?)
    func deleteMedia(_ media: AnyObject?)
}

class FamilyActivityViewController: UIViewController {
    
    fileprivate struct Constant {
        // Max video length in seconds
        static let videoLengthLimitation = 30.0
    }

    fileprivate struct SegueIdentifier {
        static let preActivity = "preActivity"
        static let photo = "photo"
        static let promise = "promise"
        static let report = "report"
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordButtonLongPressGestureRecognizer: UILongPressGestureRecognizer?
    @IBOutlet weak var nameTextField: UITextField?
    @IBOutlet weak var toggleDeletionButton: UIButton!
    
    // MARK: Variables
    
    var familyActivity: FamilyActivity!
    fileprivate var displayingAudioSet = Set<Audio>()
    fileprivate var displayingVideoSet = Set<Video>()
    fileprivate var displayingPhotoSet = Set<Photo>()
    fileprivate var displayingTextSet = Set<Text>()
    fileprivate var familyActivityType = FamilyActivity.FamilyActivityType.text
    fileprivate var playerViewController = AVPlayerViewController()
    fileprivate var recordingSession = AVAudioSession.sharedInstance()
    fileprivate var audioRecorder: AVAudioRecorder!
    /// Audio file (Local) URL, which is the one to be saved/played/uploaded and calculate audio length
    fileprivate var audioURL: URL {
        let audioFilename = getDocumentsDirectory()
        let audioURL = URL(fileURLWithPath: audioFilename).appendingPathComponent("tmp.m4a")
        return audioURL
    }
    /// 当前正在点击按钮的Promise
    fileprivate var selectedPromise: Promise?
    fileprivate var preActivityViewController: PreActivityViewController?
    fileprivate var isDeleteMode = false
    
    /// Show photo in full screen
    fileprivate var selectedImage: UIImage? { didSet { performSegue(withIdentifier: SegueIdentifier.photo, sender: self) } }
    
    /// Play video
    fileprivate var selectedVideo: Video? {
        didSet {
            selectedVideo?.url
            guard let url = selectedVideo?.url else { return }
            let player = AVPlayer(url: url as URL)
            playerViewController.player = player
            playerViewController.player?.play()
            present(playerViewController, animated: true, completion: nil)
        }
    }
    
    /// Local Image file URL, which is the one to be saved/viewed
    fileprivate var imageURL: URL {
        let imageFilename = getDocumentsDirectory()
        let imageURL = URL(fileURLWithPath: imageFilename).appendingPathComponent("tmp.png")
        return imageURL
    }
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        scrollView.contentSize = CGSizeMake(max(scrollView.frame.width, 800), max(scrollView.frame.height, 600))
        scrollView.contentSize = CGSize(width: 1200, height: 1200)
        // Gesture for record button
        recordButtonLongPressGestureRecognizer?.addTarget(self, action: #selector(FamilyActivityViewController.longPressRecordButton(_:)))
        updateUI()
    }
    
    fileprivate func updateUI() {
        // toggleDeletionButton
        toggleDeletionButton.isHidden = AppUserInfo.account.isStudent
        // refresh preActivityViewController
        preActivityViewController?.tableView.reloadData()
        // remove old data
        scrollView?.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        // add new data
        nameTextField?.text = familyActivity.name
        // Load all media onto board
        for audio in familyActivity.audios {
            if !displayingAudioSet.contains(audio) {
                addToBoard(audio)
                displayingAudioSet.insert(audio)
            }
        }
        for video in familyActivity.videos {
            if !displayingVideoSet.contains(video) {
                addToBoard(video)
                displayingVideoSet.insert(video)
            }
        }
        for photo in familyActivity.photos {
            if !displayingPhotoSet.contains(photo) {
                addToBoard(photo)
                displayingPhotoSet.insert(photo)
            }
        }
        for text in familyActivity.texts {
            if !displayingTextSet.contains(text) {
                addToBoard(text)
                displayingTextSet.insert(text)
            }
        }
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        if identifier == SegueIdentifier.preActivity {
            should = (familyActivity != nil)
        } else if identifier == SegueIdentifier.promise {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = hasPrivilege
        } else if identifier == SegueIdentifier.report {
            let hasPrivilege = AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor
            should = hasPrivilege
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.preActivity,
            let preActivityViewController = segue.destination as? PreActivityViewController
        {
            preActivityViewController.familyActivity = familyActivity
            self.preActivityViewController = preActivityViewController
        }
        else if let photoViewController = segue.destination as? PhotoViewController {
            photoViewController.image = selectedImage
        }
        else if
            let navigationController = segue.destination as? UINavigationController,
            let promise2ViewController = navigationController.topViewController as? Promise2ViewController, segue.identifier == SegueIdentifier.promise
        {
            promise2ViewController.promise = selectedPromise
        }
    }
    
    // MARK: IBActions
    
    @IBAction func tappedChoosePromiseButton(_ sender: AnyObject) {
        turnOffDeleteModeIfNeeded()
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor else { return }
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        PromiseManager.fetchPromisesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (promises) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(promises, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    @IBAction func tappedChooseFamilyActivityButton(_ sender: AnyObject) {
        turnOffDeleteModeIfNeeded()
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent else { return }
        guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
        FamilyActivityManager.fetchFamilyActivitiesWithAccountFamiltyId(
            accountFamilyId,
            success: { [weak self] (familyActivities) in
                guard let strongSelf = self else { return }
                strongSelf.displayOptionWithOptions(familyActivities, startAt: familyActivities.index(where: {$0.id == strongSelf.familyActivity.id}) ?? 0, withDelegate: strongSelf)
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    @IBAction func tappedAudioButton(_ button: UIButton) {
        turnOffDeleteModeIfNeeded()
        familyActivityType = .audio
    }
    
    @IBAction func tappedPhotoButton(_ button: UIButton) {
        turnOffDeleteModeIfNeeded()
        familyActivityType = .photo
        showVideoPhotoOptions(button, isVideo: false)
    }
    
    @IBAction func tappedVideoButton(_ button: UIButton) {
        turnOffDeleteModeIfNeeded()
        familyActivityType = .video
        showVideoPhotoOptions(button, isVideo: true)
    }
    
    @IBAction func tappedTextButton(_ button: UIButton) {
        turnOffDeleteModeIfNeeded()
        familyActivityType = .text
        showTextInputAlertView(button)
    }
    
    @IBAction func tappendRefreshButton(_ sender: UIButton) {
        FamilyActivityManager.fetchFamilyActivityWithFamilyActivityId(
            familyActivity.id,
            success: { [weak self] (familyActivity) in
                guard let strongSelf = self else { return }
                strongSelf.familyActivity = familyActivity
                strongSelf.familyActivity = familyActivity
                strongSelf.preActivityViewController?.familyActivity = familyActivity
                strongSelf.displayingAudioSet.removeAll()
                strongSelf.displayingVideoSet.removeAll()
                strongSelf.displayingPhotoSet.removeAll()
                strongSelf.displayingTextSet.removeAll()
                strongSelf.updateUI()
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
        })
    }
    @IBAction func tappedDeleteButton() {
        // privilege check
        guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor else { return }
        isDeleteMode = !isDeleteMode
        scrollView.subviews.forEach { (view) in
            if let view = view as? ActivityAudioView {
                view.deleteButton.isHidden = !isDeleteMode
            } else if let view = view as? ActivityVideoView {
                view.deleteButton.isHidden = !isDeleteMode
            } else if let view = view as? ActivityPhotoView {
                view.deleteButton.isHidden = !isDeleteMode
            } else if let view = view as? ActivityTextView {
                view.deleteButton.isHidden = !isDeleteMode
            }
        }
    }
    
    fileprivate func turnOffDeleteModeIfNeeded() {
        if isDeleteMode {
            tappedDeleteButton()
        }
    }
    
    /// Move views when drag
    @IBAction func panMediaView(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                                  y:view.center.y + translation.y)
            if recognizer.state == .ended {
                let x = Int(view.frame.origin.x), y = Int(view.frame.origin.y)
                if  let audioView = view as? ActivityAudioView,
                    let audio = audioView.audio
                {
                    audioView.leadingConstraint?.constant = view.frame.origin.x
                    audioView.topConstraint?.constant = view.frame.origin.y
                    audio.positionX = x
                    audio.positionY = y
                    updateMediaPosition(.audio, mediaId: audio.id, positionX: x, positionY: y)
                } else if let videoView = view as? ActivityVideoView,
                    let video = videoView.video
                {
                    videoView.leadingConstraint?.constant = view.frame.origin.x
                    videoView.topConstraint?.constant = view.frame.origin.y
                    video.positionX = x
                    video.positionY = y
                    updateMediaPosition(.video, mediaId: video.id, positionX: x, positionY: y)
                } else if let photoView = view as? ActivityPhotoView,
                    let photo = photoView.photo
                {
                    photoView.leadingConstraint?.constant = view.frame.origin.x
                    photoView.topConstraint?.constant = view.frame.origin.y
                    photo.positionX = x
                    photo.positionY = y
                    updateMediaPosition(.photo, mediaId: photo.id, positionX: x, positionY: y)
                } else if let textView = view as? ActivityTextView,
                    let text = textView.textItem
                {
                    textView.leadingConstraint?.constant = view.frame.origin.x
                    textView.topConstraint?.constant = view.frame.origin.y
                    text.positionX = x
                    text.positionY = y
                    updateMediaPosition(.text, mediaId: text.id, positionX: x, positionY: y)
                }
            }
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    /// Show text input
    fileprivate func showTextInputAlertView(_ button: UIButton) {
        //Create the AlertController
        let alertController: UIAlertController = UIAlertController(title: "文本", message: "请输入文本内容", preferredStyle: .alert)
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        //Create and an option action
        let saveAction: UIAlertAction = UIAlertAction(title: "保存", style: .default) { action -> Void in
            if let textFields = alertController.textFields,
                let textField = textFields.first,
                let text = textField.text {
                self.saveText(text)
            }
        }
        alertController.addAction(saveAction)
        //Add a text field
        alertController.addTextField { textField -> Void in
            //TextField configuration
            textField.textColor = UIColor.darkText
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// Show camera options
    fileprivate func showVideoPhotoOptions(_ button: UIButton, isVideo: Bool) {
        let alertController = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "取消", style: .cancel) { action -> Void in }
        alertController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "拍摄", style: .default) { action -> Void in
            self.tappedAddVideoPhotoButton(button, isVideo: isVideo)
        }
        alertController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "从相册选择", style: .default) { action -> Void in
            self.tappedSelectVideoPhotoButton(button, isVideo: isVideo)
        }
        alertController.addAction(choosePictureAction)
        alertController.popoverPresentationController?.sourceView = button // required for iPad
        present(alertController, animated: true, completion: nil)
    }
    
    /// Add media onto board
    fileprivate func addToBoard(_ resource: AnyObject) {
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(FamilyActivityViewController.panMediaView(_:)))
        let leadingConstraint: NSLayoutConstraint
        let topConstraint: NSLayoutConstraint
        let widthConstraint: NSLayoutConstraint
        let heightConstraint: NSLayoutConstraint
        if let audio = resource as? Audio {
            let frame = CGRect(x: CGFloat(audio.positionX), y: CGFloat(audio.positionY), width: 86, height: 33)
            let view = ActivityAudioView(frame: frame)
            view.audio = audio
            view.isDeleteAllowed = false
            view.delegate = self
            view.addGestureRecognizer(panGestureRecognizer)
            // position
            leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1.0, constant: CGFloat(audio.positionX))
            topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: CGFloat(audio.positionY))
            widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 86)
            heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 33)
            view.leadingConstraint = leadingConstraint
            view.topConstraint = topConstraint
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints([view.leadingConstraint!, view.topConstraint!, widthConstraint, heightConstraint])
        } else if let video = resource as? Video {
            let frame = CGRect(x: CGFloat(video.positionX), y: CGFloat(video.positionY), width: 109, height: 84)
            let view = ActivityVideoView(frame: frame)
            view.video = video
            view.isDeleteAllowed = false
            view.delegate = self
            view.addGestureRecognizer(panGestureRecognizer)
            // position
            leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1.0, constant: CGFloat(video.positionX))
            topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: CGFloat(video.positionY))
            widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 160)
            heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 160)
            view.leadingConstraint = leadingConstraint
            view.topConstraint = topConstraint
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints([view.leadingConstraint!, view.topConstraint!, widthConstraint, heightConstraint])
        } else if let photo = resource as? Photo {
            let frame = CGRect(x: CGFloat(photo.positionX), y: CGFloat(photo.positionY), width: 160, height: 100)
            let view = ActivityPhotoView(frame: frame)
            view.photo = photo
            view.isDeleteAllowed = false
            view.delegate = self
            view.addGestureRecognizer(panGestureRecognizer)
            // position
            leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1.0, constant: CGFloat(photo.positionX))
            topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: CGFloat(photo.positionY))
            widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 160)
            heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 160)
            view.leadingConstraint = leadingConstraint
            view.topConstraint = topConstraint
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints([view.leadingConstraint!, view.topConstraint!, widthConstraint, heightConstraint])
        } else if let text = resource as? Text {
            let frame = CGRect(x: CGFloat(text.positionX), y: CGFloat(text.positionY), width: scrollView.frame.width / 3, height: 50)
            let view = ActivityTextView(frame: frame)
            view.textItem = text
            view.isDeleteAllowed = false
            view.delegate = self
            view.addGestureRecognizer(panGestureRecognizer)
            // position
            leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1.0, constant: CGFloat(text.positionX))
            topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: CGFloat(text.positionY))
            widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: scrollView.frame.width / 3)
            heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
            view.leadingConstraint = leadingConstraint
            view.topConstraint = topConstraint
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints([view.leadingConstraint!, view.topConstraint!, widthConstraint, heightConstraint])
        }
    }
    
}

// MARK: FamilyActivityDelegate

extension FamilyActivityViewController: FamilyActivityDelegate {
    func updateSelectedImage(_ image: UIImage?) {
        selectedImage = image
    }
    
    func updateSelectedVideo(_ video: Video?) {
        selectedVideo = video
    }
    
    func deleteMedia(_ media: AnyObject?) {
        if let audio = media as? Audio {
            deleteMedia(.audio, mediaId: audio.id)
        } else if let video = media as? Video {
            deleteMedia(.video, mediaId: video.id)
        } else if let photo = media as? Photo {
            deleteMedia(.photo, mediaId: photo.id)
        } else if let text = media as? Text {
            deleteMedia(.text, mediaId: text.id)
        }
    }
}

// MARK: Video

extension FamilyActivityViewController {
    /// Take video with camera
    func tappedAddVideoPhotoButton(_ sender: UIButton, isVideo: Bool) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            if isVideo {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
                imagePicker.videoMaximumDuration = Constant.videoLengthLimitation
            } else {
                imagePicker.mediaTypes = [kUTTypeImage as String]
            }
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select video from album
    func tappedSelectVideoPhotoButton(_ sender: UIButton, isVideo: Bool) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            if isVideo {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
            } else {
                imagePicker.mediaTypes = [kUTTypeImage as String]
            }
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = LandscapeUIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    // MARK: Photo
    
    /// Take photo with camera
    @IBAction func tappedAddPhotoButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select photo from album
    @IBAction func tappedSelectPhotoButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension FamilyActivityViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if
            let mediaType = info[UIImagePickerControllerMediaType] as? String, mediaType == (kUTTypeMovie as String)
        {
            let urlOfVideo = info[UIImagePickerControllerMediaURL] as? URL
            if let url = urlOfVideo {
                video(url, didFinishSavingWithError: nil, contextInfo: "" as AnyObject)
            }
        }
        else if
            let mediaType = info[UIImagePickerControllerMediaType] as? String, mediaType == (kUTTypeImage as String)
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let resizedImageData = image.resizeWith(width: 1080)?.mediumQualityJPEGNSData
            let saved = ((try? resizedImageData?.write(to: imageURL, options: [.atomic])) != nil) ?? false
            if !saved {
                presentAlertViewController("图片保存失败，请重试")
            } else {
                activityIndicator.startAnimating()
                let position = randomPositionOnView(scrollView)
                FamilyActivityManager.uploadFamilyActivityFile(
                    familyActivity.id,
                    familyActivityType: familyActivityType,
                    fileURL: imageURL,
                    audioDurationSeconds: 0,
                    positionX: position.x,
                    positionY: position.y,
                    success: { [weak self] (familyActivity) in
                        guard let strongSelf = self else { return }
                        strongSelf.activityIndicator.stopAnimating()
                        strongSelf.familyActivity = familyActivity
                        // Add new Photo into ScrollView
                        if let photo = familyActivity.photos.last {
                            strongSelf.addToBoard(photo)
                        }
                    },
                    failure: { [weak self] error in
                        guard let strongSelf = self else { return }
                        strongSelf.activityIndicator.stopAnimating()
                        strongSelf.presentAlertViewController(error?.localizedFailureReason)
                    })
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if let _ = error {
            presentAlertViewController("图片保存失败，请重试")
        } else {
            
        }
    }
    
    fileprivate func video(_ videoPath: URL, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            presentAlertViewController("视频无法保存")
        } else {
            activityIndicator.startAnimating()
            let position = randomPositionOnView(scrollView)
            FamilyActivityManager.uploadFamilyActivityFile(
                familyActivity.id,
                familyActivityType: familyActivityType,
                fileURL: videoPath,
                audioDurationSeconds: 0,
                positionX: position.x,
                positionY: position.y,
                success: { [weak self] (familyActivity) in
                    guard let strongSelf = self else { return }
                    strongSelf.activityIndicator.stopAnimating()
                    strongSelf.familyActivity = familyActivity
                    // Add new Video into ScrollView
                    if let video = familyActivity.videos.last {
                        strongSelf.addToBoard(video)
                    }
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.activityIndicator.stopAnimating()
                    strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
        }
    }
}

// MARK: Recorder

extension FamilyActivityViewController {
    /// Long press on record button
    func longPressRecordButton(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            familyActivityType = .audio
            startRecording()
            displayRecordingAnimationView()
        } else if recognizer.state == .ended || recognizer.state == .cancelled || recognizer.state == .failed {
            removeRecordingAnimationView()
            if recognizer.location(in: recordButton).y < 0 {
                // 上滑取消
                finishRecording(success: false)
            } else {
                finishRecording(success: true)
            }
        }
    }
    
    func startRecording() {
        if audioRecorder == nil {
            do {
                try recordingSession.setCategory(AVAudioSessionCategoryRecord)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100.0,
                AVNumberOfChannelsKey: 2 as NSNumber,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
                AVEncoderBitRateKey : 320000
            ] as [String : Any]
            do {
                audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
            } catch {
                finishRecording(success: false)
            }
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        if success {
            uploadAudio()
        } else {
            presentAlertViewController("录音失败，请重试")
        }
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
    }
    
    /// Audio length in seconds
    fileprivate func audioLengthInSeconds(_ audioFileURL: URL) -> Int {
        let asset = AVURLAsset(url: audioFileURL, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        return Int(audioDurationSeconds)
    }
    
    // MARK: Service Call
    
    fileprivate func uploadAudio() {
        recordButton.isEnabled = false
        activityIndicator.startAnimating()
        let position = randomPositionOnView(scrollView)
        FamilyActivityManager.uploadFamilyActivityFile(
            familyActivity.id,
            familyActivityType: familyActivityType,
            fileURL: audioURL,
            audioDurationSeconds: audioLengthInSeconds(audioURL),
            positionX: position.x,
            positionY: position.y,
            success: { [weak self] familyActivity in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.familyActivity = familyActivity
                // Add new Audio into ScrollView
                if let audio = familyActivity.audios.last {
                    strongSelf.addToBoard(audio)
                }
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: AVAudioRecorderDelegate

extension FamilyActivityViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

// MARK: Text

extension FamilyActivityViewController {
    fileprivate func saveText(_ text: String) {
        activityIndicator.startAnimating()
        let position = randomPositionOnView(scrollView)
        FamilyActivityManager.addFamilyActivityText(
            familyActivity.id,
            text: text,
            positionX: position.x,
            positionY: position.y,
            success: { [weak self] (familyActivity) in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.familyActivity = familyActivity
                // Add new Text into ScrollView
                if let text = familyActivity?.texts.last {
                    strongSelf.addToBoard(text)
                }
            },
            failure: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: Delete Media, Update Media

extension FamilyActivityViewController {
    fileprivate func deleteMedia(_ familyActivityType: FamilyActivity.FamilyActivityType, mediaId: Int) {
        activityIndicator.startAnimating()
        FamilyActivityManager.deleteMedia(
            familyActivity.id,
            familyActivityType: familyActivityType,
            mediaId: mediaId,
            success: { [weak self] familyActivity in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.familyActivity = familyActivity
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    fileprivate func updateMediaPosition(_ familyActivityType: FamilyActivity.FamilyActivityType, mediaId: Int, positionX: Int, positionY: Int) {
        activityIndicator.startAnimating()
        FamilyActivityManager.updateMediaPosition(
            familyActivity.id,
            familyActivityType: familyActivityType,
            mediaId: mediaId,
            positionX: positionX,
            positionY: positionY,
            success: { [weak self] familyActivity in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.familyActivity = familyActivity
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: UITextFieldDelegate

extension FamilyActivityViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        saveText()
    }
    
    
    /// Save text to server
    fileprivate func saveText() {
        guard let familyActivity = familyActivity else { return }
        familyActivity.name = nameTextField?.text
        FamilyActivityManager.updateFamilyActivity(familyActivity,
                                     success: { [weak self] familyActivity in
                                        guard let strongSelf = self else { return }
                                        strongSelf.familyActivity = familyActivity
                                        strongSelf.nameTextField?.text = strongSelf.familyActivity?.name
                                        strongSelf.presentAlertViewController("保存成功")
            },
                                     failure: { [weak self] error in
                                        guard let strongSelf = self else { return }
                                        strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: OptionDelegate
extension FamilyActivityViewController: OptionDelegate {
    /// OptionDelegate method
    func selectedOption(_ option: AnyObject) {
        DLog("selected \(option.description)")
        if let promise = option as? Promise {
            selectedPromise = promise
            performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
        } else if let familyActivity = option as? FamilyActivity {
            self.familyActivity = familyActivity
            preActivityViewController?.familyActivity = familyActivity
            displayingAudioSet.removeAll()
            displayingVideoSet.removeAll()
            displayingPhotoSet.removeAll()
            displayingTextSet.removeAll()
            updateUI()
        }
    }
    
    func clearFlags() {}
}

