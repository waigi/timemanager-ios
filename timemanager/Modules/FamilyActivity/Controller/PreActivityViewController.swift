//
//  PreActivityViewController.swift
//  timemanager
//
//  Created by Can on 2/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation

class PreActivityViewController: UIViewController {
    
    fileprivate struct CellIdentifier {
        static let audio = "audio"
    }
    
    var familyActivity: FamilyActivity!
    
    fileprivate var recordingSession = AVAudioSession.sharedInstance()
    fileprivate var audioRecorder: AVAudioRecorder!
    /// Audio file (Local) URL, which is the one to be saved/played/uploaded and calculate audio length
    fileprivate var audioURL: URL {
        let audioFilename = getDocumentsDirectory()
        let audioURL = URL(fileURLWithPath: audioFilename).appendingPathComponent("tmp.m4a")
        return audioURL
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordButtonLongPressGestureRecognizer: UILongPressGestureRecognizer?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Lift cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recordButtonLongPressGestureRecognizer?.addTarget(self, action: #selector(PreActivityViewController.longPressRecordButton(_:)))
    }
    
    // MARK: Recorder
    
    /// Long press on record button
    func longPressRecordButton(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            startRecording()
            displayRecordingAnimationView()
        } else if recognizer.state == .ended || recognizer.state == .cancelled || recognizer.state == .failed {
            removeRecordingAnimationView()
            if recognizer.location(in: recordButton).y < 0 {
                // 上滑取消
                finishRecording(success: false)
            } else {
                finishRecording(success: true)
            }
        }
    }
    
    func startRecording() {
        if audioRecorder == nil {
            do {
                try recordingSession.setCategory(AVAudioSessionCategoryRecord)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100.0,
                AVNumberOfChannelsKey: 2 as NSNumber,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
                AVEncoderBitRateKey : 320000
            ] as [String : Any]
            do {
                audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
            } catch {
                finishRecording(success: false)
            }
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        if success {
            uploadAudio()
        } else {
            presentAlertViewController("录音失败，请重试")
        }
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
    }
    
    /// Audio length in seconds
    fileprivate func audioLengthInSeconds(_ audioFileURL: URL) -> Int {
        let asset = AVURLAsset(url: audioFileURL, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        return Int(audioDurationSeconds)
    }
    
    // MARK: Service Call
    
    fileprivate func uploadAudio() {
        recordButton.isEnabled = false
        activityIndicator.startAnimating()
        FamilyActivityManager.uploadFamilyActivityFile(
            familyActivity.id,
            familyActivityType: .preAudio,
            fileURL: audioURL,
            audioDurationSeconds: audioLengthInSeconds(audioURL),
            success: { [weak self] familyActivity in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.familyActivity = familyActivity
                strongSelf.tableView.reloadData()
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
    
    fileprivate func deleteAudio(_ audio: Audio) {
        recordButton.isEnabled = false
        activityIndicator.startAnimating()
        FamilyActivityManager.deleteMedia(
            familyActivity.id,
            familyActivityType: .preAudio,
            mediaId: audio.id,
            success: { [weak self] familyActivity in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.familyActivity = familyActivity
                strongSelf.tableView.reloadData()
            },
            failure:  { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }

}

// MARK: AVAudioRecorderDelegate

extension PreActivityViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

// MARK: UITableViewDataSource & UITableViewDelegate

extension PreActivityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return familyActivity.preActivityAudios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let audio = familyActivity.preActivityAudios[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.audio) as! PreActivityAudioCell
        cell.lengthLabel.text = audio.length.audioLengthShortStringFromSeconds
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteAudio(familyActivity.preActivityAudios[indexPath.row])
        }
    }
}

extension PreActivityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let audio = familyActivity.preActivityAudios[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! PreActivityAudioCell
        cell.playAudio(audio)
    }
}
