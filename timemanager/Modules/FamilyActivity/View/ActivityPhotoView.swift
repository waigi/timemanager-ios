//
//  ActivityPhotoView.swift
//  timemanager
//
//  Created by Can on 14/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class ActivityPhotoView: NibCellView {
    
    var leadingConstraint: NSLayoutConstraint?
    var topConstraint: NSLayoutConstraint?
    var delegate: FamilyActivityDelegate?
    var photo: Photo? { didSet {setNeedsUpdateView()} }
    var isDeleteAllowed = true { didSet {setNeedsUpdateView()} }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!

    @IBAction func tappedViewButton(_ sender: UIButton) {
        delegate?.updateSelectedImage(imageView.image)
    }
    
    @IBAction func tappedDeleteButton(_ sender: UIButton) {
        delegate?.deleteMedia(photo)
        removeFromSuperview()
    }
    
    override func updateView() {
        super.updateView()
        guard let imageURL = photo?.url else { return }
        imageView.kf.setImage(with: imageURL)
        deleteButton.isHidden = !isDeleteAllowed
    }
    
}
