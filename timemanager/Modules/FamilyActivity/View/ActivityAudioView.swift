//
//  ActivityAudioView.swift
//  timemanager
//
//  Created by Can on 14/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation

class ActivityAudioView: NibCellView {
    
    var leadingConstraint: NSLayoutConstraint?
    var topConstraint: NSLayoutConstraint?
    var delegate: FamilyActivityDelegate?
    var audio: Audio? { didSet {setNeedsUpdateView()} }
    var isDeleteAllowed = true { didSet {setNeedsUpdateView()} }
    
    fileprivate var player: AVPlayer?
    
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func tappedPlayButton(_ sender: UIButton) {
        guard let audio = audio else { return }
        playAudio(audio)
    }
    
    @IBAction func tappedDeleteButton(_ sender: UIButton) {
        delegate?.deleteMedia(audio)
        removeFromSuperview()
    }
    
    override func updateView() {
        super.updateView()
        guard let audio = audio else { return }
        lengthLabel.text = audio.length.audioLengthShortStringFromSeconds
        deleteButton.isHidden = !isDeleteAllowed
    }
    
    /// Play audio
    func playAudio(_ audio: Audio) {
        guard let url = audio.url else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
        player = AVPlayer(url: url as URL)
        startedPlayAudio()
        player?.play()
    }
    
    func startedPlayAudio() {
        playImageView.animationImages = UIView.audioAnimationImages
        playImageView.animationDuration = 2
        playImageView.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(PreActivityAudioCell.finishedPlayAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    func finishedPlayAudio() {
        playImageView.stopAnimating()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
}
