//
//  ActivityVideoView.swift
//  timemanager
//
//  Created by Can on 14/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVKit

class ActivityVideoView: NibCellView {
    
    var leadingConstraint: NSLayoutConstraint?
    var topConstraint: NSLayoutConstraint?
    var delegate: FamilyActivityDelegate?
    var video: Video? { didSet {setNeedsUpdateView()} }
    var isDeleteAllowed = true { didSet {setNeedsUpdateView()} }

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func tappedPlayButton(_ sender: UIButton) {
        delegate?.updateSelectedVideo(video)
    }
    
    @IBAction func tappedDeleteButton(_ sender: UIButton) {
        delegate?.deleteMedia(video)
        removeFromSuperview()
    }
    
    override func updateView() {
        super.updateView()
        guard let video = video else { return }
        thumbnailImageView.kf.setImage(with: video.thumbnailURL, placeholder: UIImage(named: "video_thumbnail_default"))
        deleteButton.isHidden = !isDeleteAllowed
    }

}
