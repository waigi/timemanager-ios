//
//  PreActivityAudioCell.swift
//  timemanager
//
//  Created by Can on 2/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import AVFoundation

class PreActivityAudioCell: UITableViewCell {
    
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var lengthLabel: UILabel!
    
    fileprivate var player: AVPlayer?
    
    /// Play audio
    func playAudio(_ audio: Audio) {
        guard let url = audio.url else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
        player = AVPlayer(url: url as URL)
        startedPlayAudio()
        player?.play()
    }
    
    func startedPlayAudio() {
        playImageView.animationImages = UIView.audioAnimationImages
        playImageView.animationDuration = 2
        playImageView.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(PreActivityAudioCell.finishedPlayAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    func finishedPlayAudio() {
        playImageView.stopAnimating()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
}
