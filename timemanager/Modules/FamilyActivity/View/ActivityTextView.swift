//
//  ActivityTextView.swift
//  timemanager
//
//  Created by Can on 14/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class ActivityTextView: NibCellView {
    
    var leadingConstraint: NSLayoutConstraint?
    var topConstraint: NSLayoutConstraint?
    var delegate: FamilyActivityDelegate?
    var textItem: Text? { didSet {setNeedsUpdateView()} }
    var isDeleteAllowed = true { didSet {setNeedsUpdateView()} }

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func tappedDeleteButton(_ sender: UIButton) {
        delegate?.deleteMedia(textItem)
        removeFromSuperview()
    }
    
    override func updateView() {
        super.updateView()
        textView.text = textItem?.text
        deleteButton.isHidden = !isDeleteAllowed
    }
    
}
