//
//  FamilyActivityManager.swift
//  timemanager
//
//  Created by Can on 14/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftDate

class FamilyActivityManager {
    
    fileprivate struct EndPoint {
        static let getFamilyActivityWithId = "api/family-activities/%@"
        static let getFamilyActivities = "api/family-activities/"
        static let getFamilyActivitiesByAccount = "api/family-activities/familyActivities"
        static let getFamilyActivityByTask = "api/family-activities/byTask"
        static let updateFamilyActivity = "api/family-activities/update"
        static let generateFamilyActivity = "api/family-activities"
        static let upload = "api/family-activities/upload"
        static let deleteFamilyActivity = "api/family-activities"
        static let addText = "api/family-activities/newText"
        static let deleteMedia = "api/family-activities/deleteAPVT"
        static let updateMediaPosition = "api/family-activities/updatePosition"
    }
    
    
    
    /// 根据FamilyActivity id获取FamilyActivity
    static func fetchFamilyActivityWithFamilyActivityId(_ familyActivityId: String, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + String(format: EndPoint.getFamilyActivityWithId, arguments: [familyActivityId]),
            method: .get,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据AccessToken生成当前登录用户的FamilyActivity
    static func generateFamilyActivityWithAccountFamiltyId(_ accountFamilyId: String, taskId: String?, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["accountFamilyId": accountFamilyId as AnyObject, "taskId": taskId as AnyObject? ?? "" as AnyObject]
        Alamofire.request(
            Api.baseURL + EndPoint.generateFamilyActivity,
            method: .post,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Generated FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 删除FamilyActivity
    static func deleteFamilyActivity(_ familyActivity: FamilyActivity, success: @escaping ((Bool) -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.deleteFamilyActivity + familyActivity.id,
            method: .delete,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let _):
                    success(true)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据Task Id获取FamilyActivity
    static func fetchFamilyActivityWithTaskId(_ taskId: String, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["task_id": taskId as AnyObject]
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.getFamilyActivityByTask,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Fetched FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 根据AccessToken获取当前登录用户所属的所有FamilyActivity
    static func fetchFamilyActivitiesWithAccountFamiltyId(_ accountFamilyId: String, success: @escaping (([FamilyActivity]) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = ["accountFamily_id": accountFamilyId as AnyObject]
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.getFamilyActivitiesByAccount,
            method: .get,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    var familyActivities = [FamilyActivity]()
                    DLog("received family accounts = \(data)")
                    let json = JSON(data)
                    for (_, subjson):(String, JSON) in json {
                        if let familyActivity = FamilyActivity(json: subjson) {
                            familyActivities.append(familyActivity)
                        }
                    }
                    success(familyActivities)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 添加文字到FamilyActivity中
    static func addFamilyActivityText(_ familyActivityId: String, text: String, positionX: Int? = nil, positionY: Int? = nil, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: [String: AnyObject] = ["family_activity_id": familyActivityId as AnyObject, "family_activity_text": text as AnyObject]
        if let positionX = positionX {
            parameters["position_x"] = positionX as AnyObject?
        }
        if let positionY = positionY {
            parameters["position_y"] = positionY as AnyObject?
        }
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.addText,
            method: .put,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 更新FamilyActivity中有内容的字段
    static func updateFamilyActivity(_ familyActivity: FamilyActivity, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: [String: AnyObject] = ["id": familyActivity.id as AnyObject]
//        parameters["texts"] = familyActivity.texts
        if let name = familyActivity.name {
            parameters["name"] = name as AnyObject?
        }
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.updateFamilyActivity,
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 上传文件
    static func uploadFamilyActivityFile(_ familyActivityId: String, familyActivityType: FamilyActivity.FamilyActivityType, fileURL: URL, audioDurationSeconds: Int, positionX: Int? = nil, positionY: Int? = nil, success: @escaping ((FamilyActivity) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: [String: AnyObject] = [
            "family_activity_id": familyActivityId as AnyObject,
            "family_activity_type": String(familyActivityType.rawValue) as AnyObject,
            "audio_length": "\(audioDurationSeconds)" as AnyObject]
        if let positionX = positionX {
            parameters["position_x"] = String(positionX) as AnyObject?
        }
        if let positionY = positionY {
            parameters["position_y"] = String(positionY) as AnyObject?
        }
        DLog("上传文件参数: \(parameters)")
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileURL, withName: "file")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
            to: Api.baseURL + EndPoint.upload,
            headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let
                            value = response.result.value,
                            let familyActivity = FamilyActivity(json: JSON(value))
                        {
                            DLog("上传文件后FamilyActivity=\n\(value)")
                            success(familyActivity)
                        } else {
                            failure(ErrorManager.errorWithMessage("上传文件失败，请重试"))
                        }
                    }
                case .failure(let encodingError):
                    DLog(encodingError)
                    failure(ErrorManager.errorWithMessage("上传文件失败，请检查网络"))
                }
        }
    }
    
    /// 删除资源文件
    static func deleteMedia(_ familyActivityId: String, familyActivityType: FamilyActivity.FamilyActivityType, mediaId: Int, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = [
            "family_activity_id": familyActivityId as AnyObject,
            "family_activity_type": familyActivityType.rawValue as AnyObject,
            "id": mediaId as AnyObject]
        DLog("delete文件参数: \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.deleteMedia,
            method: .put,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("FamilyActivity after deletion = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    /// 更新Media的位置
    static func updateMediaPosition(_ familyActivityId: String, familyActivityType: FamilyActivity.FamilyActivityType, mediaId: Int, positionX: Int, positionY: Int, success: @escaping ((FamilyActivity?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        let parameters: [String: AnyObject] = [
            "family_activity_id": familyActivityId as AnyObject,
            "family_activity_type": familyActivityType.rawValue as AnyObject,
            "id": mediaId as AnyObject,
            "position_x": positionX as AnyObject,
            "position_y": positionY as AnyObject]
        DLog("parameters = \(parameters)")
        Alamofire.request(
            Api.baseURL + EndPoint.updateMediaPosition,
            method: .put,
            parameters: parameters,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("Updated Position FamilyActivity = \(data)")
                    let json = JSON(data)
                    let familyActivity = FamilyActivity(json: json)
                    success(familyActivity)
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
}
