//
//  FamilyActivity.swift
//  timemanager
//
//  Created by Can on 1/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON

class FamilyActivity: NSObject {
    
    enum FamilyActivityType: Int {
        case preAudio = 0
        case photo = 1
        case audio = 2
        case video = 3
        case text = 4
    }
    
    // MARK: Vars
    
    /// 家庭活动ID，从server端获取，正式生成FamilyMeeting，返回生成的ID
    var id: String
    /// 家庭活动序号，根据每个Family Account独立编号
    var sequenceNumber: Int = 0
    /// 家庭活动名称
    var name: String?
    /// 家庭活动所属的Task
    var task: Task?
    /// 家庭活动日期时间
    var familyActivityTime: Date
    /// 活动前发言报名
    var preActivityAudios = [Audio]()
    /// 活动录音
    var audios = [Audio]()
    /// 活动视频
    var videos = [Video]()
    /// 活动文字
    var texts = [Text]()
    /// 活动照片
    var photos = [Photo]()
    
    // MARK: init
    
    init(id: String, familyActivityTime: Date) {
        self.id = id
        self.familyActivityTime = familyActivityTime
    }
    
    init?(json: JSON) {
        guard let id = json["id"].string else { return nil }
        self.id = id
        familyActivityTime = NSDate(timeIntervalSince1970: json["familyActivityTime"].doubleValue / 1000.0) as Date
        sequenceNumber = json["sequenceNumber"].intValue
        name = json["name"].string
        task = Task(json: json["task"])
        if let jsons = json["preActivityAudios"].array {
            for json in jsons {
                if let audio = Audio(json: json) {
                    preActivityAudios.append(audio)
                }
            }
        }
        if let jsons = json["audios"].array {
            for json in jsons {
                if let audio = Audio(json: json) {
                    audios.append(audio)
                }
            }
        }
        if let jsons = json["videos"].array {
            for json in jsons {
                if let video = Video(json: json) {
                    videos.append(video)
                }
            }
        }
        if let jsons = json["texts"].array {
            for json in jsons {
                if let text = Text(json: json) {
                    texts.append(text)
                }
            }
        }
        if let jsons = json["photos"].array {
            for json in jsons {
                if let photo = Photo(json: json) {
                    photos.append(photo)
                }
            }
        }
    }
    
    /// FamilyActivity的描述
    override var description: String {
        return "\(sequenceNumber) \(name ?? "")"
    }
    
}
