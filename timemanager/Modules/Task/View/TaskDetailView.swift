//
//  TaskDetailView.swift
//  timemanager
//
//  Created by Can on 23/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol TaskOptionDelegate {
    func tappedNPhotoButton(_ task: Task)
    func tappedNTextButton(_ task: Task)
    func tappedNAudioButton(_ task: Task)
    
    func tappedSStartButton(_ task: Task)
    func tappedSEndButton(_ task: Task)
    
    func tappedJPhotoButton(_ task: Task)
    func tappedJTextButton(_ task: Task)
    func tappedJAudioButton(_ task: Task)
    func tappedJVideoButton(_ task: Task)
    func tappedJDemeritButton(_ task: Task)
    
    func tappedPSelfButton(_ task: Task)
    func tappedPGuardianButton(_ task: Task)
    func tappedPTeacherButton(_ task: Task)
    
    func tappedJumpView(_ task: Task)
}

@IBDesignable class TaskDetailView: NibView {
    
    var task: Task? {
        didSet {
            dollHeadImageView.image = task?.headerImage
            updateStartEndButton()
            updateJumpView()
            updateOptionButtons()
            durationInMinutes = task?.plannedDurationInMinutes ?? 0
            taskCategory = task?.category ?? .original
            // 任务开始结束后显示时间
            if let actualStartTime = task?.actualStartTime {
                startButton.isHidden = true
                actualStartTimeLabel.isHidden = false
                actualStartTimeLabel.text = actualStartTime.timeString
            }
            if let actualEndTime = task?.actualEndTime {
                endButton.isHidden = true
                actualEndTimeLabel.isHidden = false
                actualEndTimeLabel.text = actualEndTime.timeString
            }
        }
    }
    
    var delegate: TaskOptionDelegate?
    
    var taskDetailBarView: TaskDetailBarView? {
        return superview as? TaskDetailBarView
    }
    
    fileprivate var showJump: Bool {
        guard let category = task?.category else { return false }
        return (category == .promise || category == .family)
    }
    
    fileprivate var taskCategory: TaskCategory = .original {
        didSet {
            let color: UIColor
            switch taskCategory {
            case .original:
                color = CDMColor.cdmGreen
            case .study, .read, .skill:
                color = CDMColor.cdmRed
            case .rest, .diet, .practice, .introspection, .promise:
                color = CDMColor.cdmBlue
            case .entertainment, .makeFriends, .dedication, .family:
                color = CDMColor.cdmOrange
            }
            durationInMinutesLabel.backgroundColor = color
        }
    }
    
    fileprivate var durationInMinutes: Int = 0 {
        didSet {
            durationInMinutesLabel?.text = "\(durationInMinutes)"
        }
    }
    
    @IBOutlet weak var dollHeadImageView: UIImageView!
    @IBOutlet weak var optionListView: UIView!
    @IBOutlet weak var nOptionsView: UIView!
    @IBOutlet weak var sOptionsView: UIView!
    @IBOutlet weak var jOptionsView: UIView!
    @IBOutlet weak var pOptionsView: UIView!
    @IBOutlet weak var jumpView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var actualStartTimeLabel: UILabel!
    @IBOutlet weak var actualEndTimeLabel: UILabel!
    @IBOutlet weak var taskDetailTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var taskDetailSwipeDownGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet weak var jumpViewTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var durationInMinutesLabel: UILabel!
    @IBOutlet weak var nButton: UIButton!
    @IBOutlet weak var sButton: UIButton!
    @IBOutlet weak var jButton: UIButton!
    @IBOutlet weak var pButton: UIButton!
    @IBOutlet weak var nPhotoButton: UIButton!
    @IBOutlet weak var nTextButton: UIButton!
    @IBOutlet weak var nAudioButton: UIButton!
    @IBOutlet weak var jPhotoButton: UIButton!
    @IBOutlet weak var jTextButton: UIButton!
    @IBOutlet weak var jAudioButton: UIButton!
    
    /// Show selected option details and hide others animatedly
    @IBAction func optionChanged(_ optionControl: UISegmentedControl) {
        var optionViews = [nOptionsView, sOptionsView, jOptionsView, pOptionsView]
        let viewToShow = optionViews.remove(at: optionControl.selectedSegmentIndex)
        viewToShow?.isHidden = false
        optionViews.forEach({ (view) in
            view?.isHidden = true
        })
        // bubble the selected option
        UIView.animate(withDuration: 0.9,
            animations: {
                viewToShow?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: { finish in
                UIView.animate(withDuration: 0.9, animations: {
                viewToShow?.transform = CGAffineTransform.identity
                }) 
            })
    }
    
    @IBAction func tappedOptionButton(_ button: UIButton) {
        var optionViews = [nOptionsView, sOptionsView, jOptionsView, pOptionsView]
        let viewToShow = optionViews.remove(at: button.tag)
        // P goes to credit view now
//        if viewToShow == pOptionsView {
//            tappedPTeacherButton()
//            return
//        }
        viewToShow?.isHidden = false
        optionViews.forEach({ (view) in
            view?.isHidden = true
        })
        // bubble the selected option
        UIView.animate(withDuration: 0.9,
                                   animations: {
                                    viewToShow?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: { finish in
                UIView.animate(withDuration: 0.9, animations: {
                    viewToShow?.transform = CGAffineTransform.identity
                }) 
        })
    }
    
    @IBAction func tappedNPhotoButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedNPhotoButton(task)
        }
    }
    
    @IBAction func tappedNTextButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedNTextButton(task)
        }
    }
    
    @IBAction func tappedNAudioButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedNAudioButton(task)
        }
    }
    
    @IBAction func tappedSStartButton() {
        // privilege check
        if !(AppUserInfo.account.isStudent || AppUserInfo.account.isTeacherSupervisor) {
            return
        }
        if let delegate = delegate, let task = task, task.actualStartTime == nil {
            task.actualStartTime = Date()
            delegate.tappedSStartButton(task)
            updateStartEndButton()
        }
    }
    
    @IBAction func tappedSEndButton() {
        // privilege check
        if !(AppUserInfo.account.isStudent || AppUserInfo.account.isTeacherSupervisor) {
            return
        }
        if let delegate = delegate, let task = task, task.actualEndTime == nil {
            task.actualEndTime = Date()
            delegate.tappedSEndButton(task)
            updateStartEndButton()
        }
    }
    
    @IBAction func tappedJPhotoButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedJPhotoButton(task)
        }
    }
    
    @IBAction func tappedJTextButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedJTextButton(task)
        }
    }
    
    @IBAction func tappedJAudioButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedJAudioButton(task)
        }
    }
    
    @IBAction func tappedJVideoButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedJVideoButton(task)
        }
    }
    
    @IBAction func tappedJDemeritButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedJDemeritButton(task)
        }
    }
    
    @IBAction func tappedPSelfButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedPSelfButton(task)
        }
    }
    
    @IBAction func tappedPGuardianButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedPGuardianButton(task)
        }
    }
    
    @IBAction func tappedPTeacherButton() {
        if let delegate = delegate, let task = task {
            delegate.tappedPTeacherButton(task)
        }
    }
    
    /// Swipe up on TaskDollView
    func tapJumpView(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            if let delegate = delegate, let task = task {
                delegate.tappedJumpView(task)
            }
        }
    }
    
    fileprivate func updateStartEndButton() {
        //startButton.isEnabled = true
        //endButton.isEnabled = true
        startButton.isEnabled = task?.actualStartTime == nil
        endButton.isEnabled = task?.actualEndTime == nil && task?.actualStartTime != nil
    }
    
    fileprivate func updateOptionButtons() {
        guard let task = task else { return }
        let nButtonBackgroundImage = task.hasN ? UIImage(named: "task highlight")! : UIImage(named: "task")!
        nButton.setBackgroundImage(nButtonBackgroundImage, for: UIControlState())
        let sButtonBackgroundImage = task.hasS ? UIImage(named: "time highlight")! : UIImage(named: "time")!
        sButton.setBackgroundImage(sButtonBackgroundImage, for: UIControlState())
        let jButtonBackgroundImage = task.hasJ ? UIImage(named: "record highlight")! : UIImage(named: "record")!
        jButton.setBackgroundImage(jButtonBackgroundImage, for: UIControlState())
        let pButtonBackgroundImage = task.hasP ? UIImage(named: "evaluate highlight")! : UIImage(named: "evaluate")!
        pButton.setBackgroundImage(pButtonBackgroundImage, for: UIControlState())
        let nTextButtonBackgroundImage = task.hasNText ? UIImage(named: "writing highlight")! : UIImage(named: "writing")!
        nTextButton.setBackgroundImage(nTextButtonBackgroundImage, for: UIControlState())
        let jTextButtonBackgroundImage = task.hasJText ? UIImage(named: "writing highlight")! : UIImage(named: "writing")!
        jTextButton.setBackgroundImage(jTextButtonBackgroundImage, for: UIControlState())
        let nPhotoButtonBackgroundImage = task.hasNPhoto ? UIImage(named: "photo highlight")! : UIImage(named: "photo")!
        nPhotoButton.setBackgroundImage(nPhotoButtonBackgroundImage, for: UIControlState())
        let jPhotoButtonBackgroundImage = task.hasJPhoto ? UIImage(named: "photo highlight")! : UIImage(named: "photo")!
        jPhotoButton.setBackgroundImage(jPhotoButtonBackgroundImage, for: UIControlState())
        let nAudioButtonBackgroundImage = task.hasNAudio ? UIImage(named: "mic highlight")! : UIImage(named: "mic")!
        nAudioButton.setBackgroundImage(nAudioButtonBackgroundImage, for: UIControlState())
        let jAudioButtonBackgroundImage = task.hasJAudio ? UIImage(named: "mic highlight")! : UIImage(named: "mic")!
        jAudioButton.setBackgroundImage(jAudioButtonBackgroundImage, for: UIControlState())
    }
    
    /// 根据选择的Task类型显示NSJP选项或者跳转按钮
    fileprivate func updateJumpView() {
        guard showJump else { return }
        jumpView.isHidden = !showJump
        optionListView.isHidden = showJump
        nOptionsView.isHidden = showJump
        sOptionsView.isHidden = showJump
        jOptionsView.isHidden = showJump
        pOptionsView.isHidden = showJump
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        jumpViewTapGestureRecognizer.addTarget(self, action: #selector(TaskDetailView.tapJumpView(_:)))
    }
}
