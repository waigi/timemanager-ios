//
//  TaskClockView.swift
//  timemanager
//
//  Created by Can on 9/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class TaskClockView: NibView {

    @IBOutlet weak var leftTimeTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightTimeLeadingConstraint: NSLayoutConstraint!
    var taskClockViewCenterXConstraint: NSLayoutConstraint?
    var taskClockViewWidthConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var plannedStartTimeLabel: UILabel!
    @IBOutlet weak var plannedEndTimeLabel: UILabel!
    @IBOutlet weak var durationInMinutes: UILabel!
    @IBOutlet weak var leftHandPanGestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var rightHandPanGestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var taskClockViewPanGestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var taskClockViewSwipeUpGestureRecognizer: UISwipeGestureRecognizer!

    @IBInspectable var leftTimeLengthFromMiddle: CGFloat = 20 {
        didSet {
            setNeedsUpdateView()
        }
    }
    @IBInspectable var rightTimeLengthFromMiddle: CGFloat = 20 {
        didSet {
            setNeedsUpdateView()
        }
    }
    
    override func updateView() {
        super.updateView()
        leftTimeTrailingConstraint.constant = leftTimeLengthFromMiddle
        rightTimeLeadingConstraint.constant = rightTimeLengthFromMiddle
    }
}
