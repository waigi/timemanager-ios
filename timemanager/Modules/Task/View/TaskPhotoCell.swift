//
//  TaskPhotoCell.swift
//  timemanager
//
//  Created by Can on 29/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class TaskPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
