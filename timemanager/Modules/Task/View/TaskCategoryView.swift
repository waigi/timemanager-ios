//
//  TaskCategoryView.swift
//  timemanager
//
//  Created by Can on 7/10/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class TaskCategoryView: NibView {
    
    var hightlightButtonIndex: Int? { didSet { updateView() } }
    
    @IBOutlet weak var button0: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var button10: UIButton!
    @IBOutlet weak var button11: UIButton!
    
    fileprivate var buttons: [UIButton] {
        return [button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11]
    }
    
    func indexOfButton(_ button: UIButton) -> Int? {
        return buttons.index(of: button)
    }
    
    /// If the Button is "诺"
    func isPromise(_ button: UIButton) -> Bool {
        return button == button10
    }
    
    override func updateView() {
        super.updateView()
        if let hightlightButtonIndex = hightlightButtonIndex, hightlightButtonIndex >= 0 {
            buttons[hightlightButtonIndex].isHighlighted = true
        }
    }
    
}
