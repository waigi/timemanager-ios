//
//  TaskDetailBarView.swift
//  timemanager
//
//  Created by Can on 28/11/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class TaskDetailBarView: NibView {
    
    var task: Task? {
        didSet {
            taskDetailView.task = task
            taskRaiseView.task = task
        }
    }

    let intrinsicHeight: CGFloat = 212
    let raiseHeight: CGFloat = 84
    let slideViewWidth: CGFloat = 62
    var heightConstraint: NSLayoutConstraint?
    var dollView: TaskDollView?
    
    @IBOutlet weak var taskDetailView: TaskDetailView!
    @IBOutlet weak var durationInMinutesLabel: UILabel!
    @IBOutlet weak var taskRaiseView: TaskRaiseView!
    @IBOutlet weak var leftSlideView: UIView!
    @IBOutlet weak var rightLineView: UIView!
    @IBOutlet weak var rightSlideView: UIView!
    @IBOutlet weak var raiseCoverView: UIView!
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var raiseViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftSlideWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightSlideWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        taskDetailView.durationInMinutesLabel = durationInMinutesLabel
    }
 
    /// override this method to allow out of boundary buttons clickable
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if taskDetailView.frame.contains(point) {
            return true
        }
        return super.point(inside: point, with: event)
    }
    
    func showTaskDetailViewAnimated() {
        leftLineView.isHidden = true
        rightLineView.isHidden = true
        self.dollView?.isHidden = true
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.raiseViewTopConstraint.constant = 0
                self.layoutIfNeeded()
            },
            completion: { finished in
                self.taskDetailView.isHidden = false
                self.taskRaiseView.isHidden = true
                self.leftLineView.isHidden = false
                self.rightLineView.isHidden = false
                self.raiseCoverView.isHidden = true
                UIView.animate(
                    withDuration: 0.5,
                    animations: {
                        self.leftSlideWidthConstraint.constant = 1
                        self.rightSlideWidthConstraint.constant = 1
                        self.layoutIfNeeded()
                    },
                    completion: { finished in
                        self.leftSlideView.isHidden = true
                        self.rightSlideView.isHidden = true
                        self.superview?.bringSubview(toFront: self)
                })
        })
    }
    
    func hideTaskDetailViewAnimated() {
        self.superview?.sendSubview(toBack: self)
        self.leftLineView.isHidden = false
        self.rightLineView.isHidden = false
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.leftSlideWidthConstraint.constant = self.slideViewWidth
                self.rightSlideWidthConstraint.constant = self.slideViewWidth
                self.layoutIfNeeded()
            },
            completion: { finished in
                self.raiseCoverView.isHidden = false
                self.taskRaiseView.isHidden = false
                self.leftLineView.isHidden = true
                self.rightLineView.isHidden = true
                self.taskDetailView.isHidden = true
                UIView.animate(
                    withDuration: 0.5,
                    animations: {
                        self.raiseViewTopConstraint.constant = self.raiseHeight
                        self.layoutIfNeeded()
                    },
                    completion: { finished in
                        self.dollView?.isHidden = false
                        self.removeFromSuperview()
                })
        })
    }
    
}
