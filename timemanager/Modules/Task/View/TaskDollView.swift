//
//  TaskDollView.swift
//  timemanager
//
//  Created by Can on 18/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

struct CDMColor {
    static let cdmRed = UIColor(colorLiteralRed: 250.0/255.0, green: 125.0/255.0, blue: 143.0/255.0, alpha: 1.0)
    static let cdmBlue = UIColor(colorLiteralRed: 102.0/255.0, green: 196.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    static let cdmOrange = UIColor(colorLiteralRed: 255.0/255.0, green: 184.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    static let cdmGreen = UIColor(colorLiteralRed: 68.0/255.0, green: 178.0/255.0, blue: 75.0/255.0, alpha: 1.0)
}

@IBDesignable class TaskDollView: NibView {
    
    let intrinsicHeight = CGFloat(150)
    
    var task: Task? {
        didSet {
            dollHeadImageView.image = task?.headerImage
            durationInMinutes = task?.plannedDurationInMinutes ?? 0
            taskCategory = task?.category ?? .original
            if let task = task {
                plannedStartTimeLabel.text = DateFormatter.timeDateFormatter.string(from: task.plannedStartTime)
                plannedEndTimeLabel.text = DateFormatter.timeDateFormatter.string(from: task.plannedEndTime)
                discountLabel.text = task.isFinished ? task.discount.creditString : (task.hasS ? "⏱" : "")
            }
        }
    }
    
    fileprivate var taskCategory: TaskCategory = .original {
        didSet {
            let color: UIColor
            switch taskCategory {
            case .original:
                color = CDMColor.cdmGreen
            case .study, .read, .skill:
                color = CDMColor.cdmRed
            case .rest, .diet, .practice, .introspection, .promise:
                color = CDMColor.cdmBlue
            case .entertainment, .makeFriends, .dedication, .family:
                color = CDMColor.cdmOrange
            }
            durationInMinutesLabel.backgroundColor = color
        }
    }
    
    fileprivate var durationInMinutes: Int = 0 {
        didSet {
            durationInMinutesLabel?.text = "\(durationInMinutes)"
        }
    }
    
    var centerXConstraint: NSLayoutConstraint?
    var heightConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var dollHeadImageView: UIImageView!
    @IBOutlet weak var durationInMinutesLabel: UILabel!
    @IBOutlet weak var dollSwipeUpGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet weak var dollTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var taskDollViewSwipeDownGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet weak var plannedStartTimeLabel: UILabel!
    @IBOutlet weak var plannedEndTimeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var dollLongPressGestureRecognizer: UILongPressGestureRecognizer!


}
