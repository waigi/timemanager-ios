//
//  TaskRaiseView.swift
//  timemanager
//
//  Created by Can on 3/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

@IBDesignable class TaskRaiseView: NibView {
    
    var task: Task? {
        didSet {
            dollHeadImageView.image = task?.headerImage
        }
    }
    
    @IBOutlet weak var dollHeadImageView: UIImageView!

}
