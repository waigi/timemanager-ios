//
//  TaskJVideoViewController.swift
//  timemanager
//
//  Created by Can on 14/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVKit

class TaskJVideoViewController: UITableViewController {
    
    fileprivate struct Constant {
        // Max video length in seconds
        static let videoLengthLimitation = 30.0
    }
    
    fileprivate struct ImageName {
        static let record = "icon_record"
        static let stop = "icon_stop"
    }
    
    fileprivate var playerViewController = AVPlayerViewController()
    
    /// Current selected Task
    var task: Task? {
        get {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task
            } else {
                return nil
            }
        }
        set {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task = newValue
            }
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    // Lift Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadVideo()
    }
    
    /// Play recorded video if available
    @IBAction func playVideo() {
        guard let url = task?.jVideo?.url else { return }
        let player = AVPlayer(url: url as URL)
        playerViewController.player = player
        playerViewController.player?.play()
        present(playerViewController, animated: true, completion: nil)
    }
    
    /// Take video with camera
    @IBAction func tappedAddButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.videoMaximumDuration = Constant.videoLengthLimitation
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select video from album
    @IBAction func tappedSelectButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = LandscapeUIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    fileprivate func reloadVideo() {
        if let thumbnailURL = task?.jVideo?.thumbnailURL {
            thumbnailImageView.kf.setImage(with: thumbnailURL, placeholder: UIImage(named: "video_thumbnail_default"))
        }
    }

}

// MARK: - UIImagePickerControllerDelegate
extension TaskJVideoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let mediaType = info[UIImagePickerControllerMediaType] as? String, mediaType == (kUTTypeMovie as String) {
            let urlOfVideo = info[UIImagePickerControllerMediaURL] as? URL
            if let url = urlOfVideo {
                video(url,
                      didFinishSavingWithError: nil,
                      contextInfo: "" as AnyObject)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func video(_ videoPath: URL, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            presentAlertViewController("视频无法保存")
        } else {
            guard let task = task else {
                presentAlertViewController("找不到任务无法上传视频")
                return
            }
            activityIndicator.startAnimating()
            TaskManager.uploadTaskJVideo(task.id,
                                         fileURL: videoPath,
                                         success: { [weak self] task in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.task = task
                                            strongSelf.reloadVideo()
                                            strongSelf.presentAlertViewController("视频上传成功")
                                         },
                                         failure: { [weak self] error in
                                            guard let strongSelf = self else { return }
                                            strongSelf.activityIndicator.stopAnimating()
                                            strongSelf.presentAlertViewController(error?.localizedFailureReason)
                                         })
        }
    }
}
