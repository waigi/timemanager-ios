//
//  TaskNTextViewController.swift
//  timemanager
//
//  Created by Can on 28/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import MBProgressHUD

class TaskNTextViewController: UITableViewController, UITextViewDelegate {
    
    /// Current selected Task
    var task: Task? {
        get {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task
            } else {
                return nil
            }
        }
        set {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task = newValue
            }
        }
    }

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.frame = CGRect(x: 100, y: 30, width: 200, height: 200)
        textView.text = task?.nText?.text
    }

    @IBAction func tappedDoneButtonItem(_ sender: UIBarButtonItem) {
        textView.resignFirstResponder()
        saveText()
    }
    
    // MARK: UITextViewDelegate
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        saveText()
    }
    
    /// Save text to server
    fileprivate func saveText() {
        guard let task = task else { return }
        guard task.nText?.text != textView.text else { return }
        // Keep one text only as per requirement for now
        task.nTexts = [Text(text: textView.text)]
        activityIndicator.startAnimating()
        TaskManager.updateTask(task,
                               success: { [weak self] task in
                                guard let strongSelf = self else { return }
                                strongSelf.activityIndicator.stopAnimating()
                                strongSelf.task = task
                                strongSelf.presentAlertViewController("保存成功")
                                },
                               failure: { [weak self] error in
                                guard let strongSelf = self else { return }
                                strongSelf.activityIndicator.stopAnimating()
                                strongSelf.presentAlertViewController(error?.localizedFailureReason)
                                })
    }
}
