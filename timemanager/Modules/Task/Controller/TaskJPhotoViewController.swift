//
//  TaskJPhotoViewController.swift
//  timemanager
//
//  Created by Can on 28/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//


import UIKit
import MobileCoreServices
import Kingfisher

class TaskJPhotoViewController: UIViewController {
    
    fileprivate struct SegueIdentifier {
        static let photoCarousel = "photoCarousel"
        static let photo = "photo"
    }
    
    fileprivate struct CellIdentifier {
        static let photo = "photo"
    }
    
    fileprivate struct ImageName {
        static let error = "image_error"
    }
    
    var selectedImage: UIImage?
    
    /// Current selected Task
    var task: Task? {
        get {
            return tabBarController.flatMap() { $0 as? TaskTabBarController }?.task
        }
        set {
            tabBarController.flatMap() { $0 as? TaskTabBarController }?.task = newValue
        }
    }
    
    /// Image file URL, which is the one to be saved/viewed
    var imageURL: URL {
        let imageFilename = getDocumentsDirectory()
        let imageURL = URL(fileURLWithPath: imageFilename).appendingPathComponent("tmp.png")
        return imageURL
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Take photo with camera
    @IBAction func tappedAddButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select photo from album
    @IBAction func tappedSelectButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = LandscapeUIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Delete photo
    @IBAction func tappedEditButton(_ button: UIButton) {
        if let
            task = task,
            let cell = button.superviewCollectionCell,
            let indexPath = collectionView.indexPath(for: cell)
        {
            presentAlertViewController("删除此图片？", showCancel: true, handler: {
                task.jPhotos.remove(at: indexPath.item)
                self.activityIndicator.startAnimating()
                TaskManager.updateTask(task,
                    success: { [weak self] task in
                        guard let strongSelf = self else { return }
                        strongSelf.activityIndicator.stopAnimating()
                        strongSelf.collectionView.deleteItems(at: [indexPath])
                        strongSelf.task = task
                    },
                    failure: { [weak self] error in
                        guard let strongSelf = self else { return }
                        strongSelf.activityIndicator.stopAnimating()
                        strongSelf.presentAlertViewController("删除失败请重试")
                    })
            })
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let photoViewController = segue.destination as? PhotoViewController {
            photoViewController.image = selectedImage
        }
    }
    
}

// MARK: UICollectionViewDataSource, UICollectionViewDelegate
extension TaskJPhotoViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return task?.jPhotos.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.photo, for: indexPath) as! TaskPhotoCell
        if let
            task = task,
            let url = task.jPhotos[indexPath.item].url
        {
            cell.imageView.kf.setImage(with: url, placeholder: UIImage(named: ImageName.error))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width = min(collectionView.frame.width, collectionView.frame.height)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? TaskPhotoCell {
            selectedImage = cell.imageView.image
            performSegue(withIdentifier: SegueIdentifier.photo, sender: cell)
        }
    }
}

// MARK: Camera
extension TaskJPhotoViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let task = task else { return }
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismiss(animated: true, completion: nil)
        if mediaType == (kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let resizedImageData = image.resizeWith(width: 1080)?.mediumQualityJPEGNSData
            let saved = ((try? resizedImageData?.write(to: imageURL, options: [.atomic])) != nil) ?? false
            if !saved {
                presentAlertViewController("图片保存失败，请重试")
            } else {
                activityIndicator.startAnimating()
                TaskManager.uploadTaskPhoto(task.id,
                                            taskType: .j,
                                            fileURL: imageURL,
                                            success: { [weak self] task in
                                                guard let strongSelf = self else { return }
                                                strongSelf.activityIndicator.stopAnimating()
                                                strongSelf.task = task
                                                strongSelf.collectionView.reloadData()
                    },
                                             failure: { [weak self] error in
                                                guard let strongSelf = self else { return }
                                                strongSelf.activityIndicator.stopAnimating()
                                                strongSelf.presentAlertViewController(error?.localizedFailureReason)
                    })
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer) {
        if error != nil {
            presentAlertViewController("图片保存失败，请重试")
            collectionView.reloadData()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
