//
//  TaskTabBarController.swift
//  timemanager
//
//  Created by Can on 28/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol TaskDelegate {
    func updateTask(_ task: Task?)
}

class TaskTabBarController: UITabBarController {
    
    var task: Task?
    var taskDelegate: TaskDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.frame = CGRect(x: 60.0, y: 30.0, width: view.frame.width - 120.0, height: view.frame.height - 60.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DLog(view.frame)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        taskDelegate?.updateTask(task)
    }

}
