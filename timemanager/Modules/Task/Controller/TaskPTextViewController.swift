//
//  TaskPTextViewController.swift
//  timemanager
//
//  Created by Can on 4/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

class TaskPTextViewController: UITableViewController, UITextViewDelegate {
    
    fileprivate enum PTextType {
        case guardian, selfie, teacher
    }
    
    /// Indicates which type of Text is currently editing
    fileprivate var pTextType: PTextType?
    
    /// Current selected Task
    var task: Task? {
        get {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task
            } else {
                return nil
            }
        }
        set {
            if let taskTabBarController = tabBarController as? TaskTabBarController {
                return taskTabBarController.task = newValue
            }
        }
    }
    
    /// TextView being editing
    var textView: UITextView {
        return guardianTextView != nil ? guardianTextView! : (selfTextView != nil ? selfTextView! : teacherTextView!)
    }
    
    @IBOutlet weak var guardianTextView: UITextView?
    @IBOutlet weak var selfTextView: UITextView?
    @IBOutlet weak var teacherTextView: UITextView?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let guardianTextView = guardianTextView {
            pTextType = .guardian
            guardianTextView.text = task?.pGuardianText
        } else if let selfTextView = selfTextView {
            pTextType = .selfie
            selfTextView.text = task?.pSelfText
        } else if let teacherTextView = teacherTextView {
            pTextType = .teacher
            teacherTextView.text = task?.pTeacherText
        }
    }
    
    @IBAction func tappedDoneButtonItem(_ sender: UIBarButtonItem) {
        textView.resignFirstResponder()
        saveText()
    }
    
    // MARK: UITextViewDelegate
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        saveText()
    }
    
    /// Save text to server
    fileprivate func saveText() {
        guard let task = task, let pTextType = pTextType else { return }
        switch pTextType {
        case .guardian:
            task.pGuardianText = textView.text
        case .selfie:
            task.pSelfText = textView.text
        case .teacher:
            task.pTeacherText = textView.text
        }
        activityIndicator.startAnimating()
        TaskManager.updateTask(task,
                               success: { [weak self] task in
                                guard let strongSelf = self else { return }
                                strongSelf.activityIndicator.stopAnimating()
                                strongSelf.task = task
                                strongSelf.presentAlertViewController("保存成功")
            },
                               failure: { [weak self] error in
                                guard let strongSelf = self else { return }
                                strongSelf.activityIndicator.stopAnimating()
                                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}
