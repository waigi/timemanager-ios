//
//  HomeViewController+Task.swift
//  timemanager
//
//  Created by Can on 18/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

// MARK: Task Doll View

extension HomeViewController {
    /// Load all tasks belong to the current user and create TaskDollView on timeline and clear existing tasks
    func loadTasksIntoTimeline() {
        guard AppUserInfo.hasAccount else { return }
        guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
            presentAlertViewController("没有选择的学生")
            return
        }
        //li guan wei begin
         //if let url = selectedStudentAccountFamily.studentAvatarURL{
            self.studentAvatarImageView.kf.setImage(with: selectedStudentAccountFamily.studentAvatarURL)
         //}
        //liguanwei end
        removeAllTaskViewsFromTimeline()
        TaskManager.fetchTasksWithAccountFamilyId(
            selectedStudentAccountFamily.id,
            onDate: taskDate,
            success: { tasks in
                for task in tasks {
                    let taskWithView = TaskWithView(taskId: task.id, task, nil, nil, nil)
                    self.taskWithViews[task.id] = taskWithView
                    DispatchQueue.main.async(execute: { 
                        self.generateTaskDollViewOfTask(taskWithView, inView: self.timelineContentView)
                    })
                }
            },
            failure: { error in
                self.presentAlertViewController(error?.localizedFailureReason, showCancel: false, handler: {
                    if error?.code == 401 {
                        self.performSegue(withIdentifier: SegueIdentifier.login, sender: self)
                    }
                })
        })
    }
    
    /// Generate a TaskDollView and remove the TaskClockView
    func generateTaskDollViewOfTask(_ taskWithView: TaskWithView, inView view: UIView) {
        let dollView = TaskDollView()
        dollView.task = taskWithView.task
        CalendarManager.addEvent(withTask: taskWithView.task)
        let centerXConstraintConstant = (timelinePositionOfDate(taskWithView.task.plannedStartTime) + timelinePositionOfDate(taskWithView.task.plannedEndTime)) / 2
        // position
        let centerXConstraint = NSLayoutConstraint(item: dollView, attribute: .centerX, relatedBy: .equal, toItem: timelineView, attribute: .leading, multiplier: 1.0, constant: centerXConstraintConstant)
        let bottomConstraint = NSLayoutConstraint(item: dollView, attribute: .bottom, relatedBy: .equal, toItem: timelineView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: dollView, attribute: .width, relatedBy: .equal, toItem: timelineView, attribute: .width, multiplier: widthPercentageOfTask(taskWithView.task), constant: 0)
        // gestures
        dollView.dollSwipeUpGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeUpTaskDollView(_:)))
        dollView.taskDollViewSwipeDownGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeDownTaskDollView(_:)))
        dollView.dollTapGestureRecognizer.addTarget(self, action: #selector(HomeViewController.tapTaskDollView(_:)))
        dollView.dollLongPressGestureRecognizer.addTarget(self, action: #selector(HomeViewController.longPressTaskDollView(_:)))
        // add to superView
        dollView.translatesAutoresizingMaskIntoConstraints = false
        dollView.centerXConstraint = centerXConstraint
        view.addSubview(dollView)
        view.sendSubview(toBack: dollView)
//        view.addConstraints([centerXConstraint, bottomConstraint, widthConstraint])
        NSLayoutConstraint.activate([centerXConstraint, bottomConstraint, widthConstraint])
        view.layoutIfNeeded()
        
        // remove taskClockView
        taskWithView.taskClockView?.removeFromSuperview()
        taskWithViews[taskWithView.taskId] = (taskWithView.taskId, taskWithView.task, taskWithView.taskClockView, dollView, nil)
//        DLog("dollView created: \(dollView.frame)")
    }
    
    /// Update Task clock width and position on timeline with Task value when zoom in/out timeline.
    func updateTaskDollViewPosition(_ taskWithView: TaskWithView) {
        let task = taskWithView.task
        guard let taskDollView = taskWithView.taskDollView else { return }
        let leftHandPosition = timelinePositionOfDate(task.plannedStartTime)
        let rightHandPosition = timelinePositionOfDate(task.plannedEndTime)
        let handToMiddleLength = (rightHandPosition - leftHandPosition) / 2
        taskDollView.centerXConstraint?.constant = leftHandPosition + handToMiddleLength
        taskDollView.layoutIfNeeded()
    }
    
    /// Swipe up on TaskDollView
    func swipeUpTaskDollView(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDollView = view as? TaskDollView,
                let taskWithView = taskWithViewAssociatedWithTaskDollView(taskDollView)
            {
                guard taskWithView.task.category != TaskCategory.original else {
                    presentAlertViewController("请先选取任务类型")
                    return
                }
                taskDollView.isHidden = true
                generateTaskDetailBarViewOfTask(taskWithView, inView: self.timelineContentView)
            }
        }
    }
    
    /// Swipe down on TaskDollView
    func swipeDownTaskDollView(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDollView = view as? TaskDollView,
                let taskWithView = taskWithViewAssociatedWithTaskDollView(taskDollView)
            {
                // privilege check
                if (taskWithView.task.isInProgress || taskWithView.task.isAfterEnd) && !(AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor) {
                    return
                }
                reverseToTaskClockViewWith(taskWithView)
            }
        }
    }
    
    /// Tap on TaskDollView
    func tapTaskDollView(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDollView = view as? TaskDollView {
                // find taskWithView
                guard let taskWithView = taskWithViewAssociatedWithTaskDollView(taskDollView) else { return }
                selectedTask = taskWithView.task
                if selectedTask?.category == TaskCategory.original {
                    performSegue(withIdentifier: SegueIdentifier.taskCategoryPopOver, sender: taskDollView)
                } else {
//                    presentAlertViewController("只能选择一次任务类别")
                }
            }
        }
    }
    
    func longPressTaskDollView(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDollView = view as? TaskDollView {
                // find taskWithView
                guard let taskWithView = taskWithViewAssociatedWithTaskDollView(taskDollView) else { return }
                selectedTask = taskWithView.task
                if selectedTask?.category == TaskCategory.original {
                    performSegue(withIdentifier: SegueIdentifier.taskCategoryPopOver, sender: taskDollView)
                } else {
                    //li guan wei begin
                    if(!taskWithView.task.hasS){
                        taskWithView.task.actualStartTime = Date();
                        taskWithView.task.plannedEndTime = (taskWithView.task.actualStartTime?.addingTimeInterval(taskWithView.task.plannedDuration))!;
                        taskWithView.task.plannedStartTime = taskWithView.task.actualStartTime!;
                        // update taskClockView
                        taskWithView.taskDollView?.plannedStartTimeLabel.text = DateFormatter.timeDateFormatter.string(from: taskWithView.task.plannedStartTime)
                        taskWithView.taskDollView?.plannedEndTimeLabel.text = DateFormatter.timeDateFormatter.string(from: taskWithView.task.plannedEndTime)
                        taskWithView.taskDollView?.discountLabel.text = "⏱"
                        tappedSStartButton(taskWithView.task)
                        
                        updateTaskViewPosition(taskWithView)
                        //let taskWithView2 = taskWithViews[taskWithView.task.id]
                        //taskWithView2.taskClockView?.plannedStartTimeLabel.text
                        
                    }else if(!taskWithView.task.isFinished){
                        taskWithView.task.actualEndTime = Date()
                        tappedSEndButton(taskWithView.task);
                        taskWithView.taskDollView?.discountLabel.text =  taskWithView.task.discount.creditString
                        //taskWithView.taskDollView?.discountLabel.text = ""
                    }
                    //li guan wei end
                    //                    presentAlertViewController("只能选择一次任务类别")
                }
            }
        }
    }
    /// Reverse a TaskDollView to be taskClockView and remove generated Doll and Detail views from superview
    func reverseToTaskClockViewWith(_ taskWithView: TaskWithView) {
        
        /// Create task clock view with generated task id
        func generateTask(_ taskDollView: TaskDollView) -> TaskClockView {
            let taskClockView = TaskClockView(frame: CGRect(x: clockView.frame.origin.x, y: clockView.frame.origin.y, width: Constant.taskClockViewWidth, height: Constant.taskClockViewHeight))
            // set constraints
            let centerXConstraintConstant = (timelinePositionOfDate(taskWithView.task.plannedStartTime) + timelinePositionOfDate(taskWithView.task.plannedEndTime)) / 2
            let topConstraint = NSLayoutConstraint(item: taskClockView, attribute: .top, relatedBy: .equal, toItem: timelineView, attribute: .bottom, multiplier: 1.0, constant: Constant.clockViewTop)
            let bottomConstraint = NSLayoutConstraint(item: taskClockView, attribute: .bottom, relatedBy: .equal, toItem: timelineContentView, attribute: .bottom, multiplier: 1.0, constant: -Constant.taskClockViewBottom)
            let centerXConstraint = NSLayoutConstraint(item: taskClockView, attribute: .centerX, relatedBy: .equal, toItem: timelineView, attribute: .leading, multiplier: 1.0, constant: centerXConstraintConstant)
            taskClockView.taskClockViewCenterXConstraint = centerXConstraint
            taskClockView.isHidden = true
            timelineContentView.addSubview(taskClockView)
            taskClockView.translatesAutoresizingMaskIntoConstraints = false
            timelineContentView.addConstraints([topConstraint, bottomConstraint, centerXConstraint])
            taskClockView.isHidden = false
            // gestures
            taskClockView.leftHandPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockViewLeftHand(_:)))
            taskClockView.rightHandPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockViewRightHand(_:)))
            taskClockView.taskClockViewPanGestureRecognizer.addTarget(self, action: #selector(HomeViewController.panTaskClockView(_:)))
            taskClockView.taskClockViewSwipeUpGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeUpTaskClockView(_:)))
            return taskClockView
        }

        // create new taskClockView
        guard let taskDollView = taskWithView.taskDollView else { return }
        let taskClockView = generateTask(taskDollView)
        taskWithViews[taskWithView.taskId] = (taskWithView.taskId, taskWithView.task, taskClockView, nil, nil)
        taskWithView.taskDollView?.removeFromSuperview()
        taskWithView.taskDetailBarView?.removeFromSuperview()
        if let updatedTaskWithView = taskWithViews[taskWithView.taskId] {
            updateTaskViewPosition(updatedTaskWithView)
            CalendarManager.deleteEvent(withTask: updatedTaskWithView.task)
        }
        updateTimeOfTaskClockView(taskClockView)
    }
    
    /// Find the TaskWithView object which contains given taskDollView
    fileprivate func taskWithViewAssociatedWithTaskDollView(_ taskDollView: TaskDollView) -> TaskWithView? {
        return taskWithViews.values.filter { (taskWithView) -> Bool in
            return taskWithView.taskDollView == taskDollView
            }.first
    }
    
    /// Remove TaskClockViews, TaskDollViews, TaskDetailViews from Timeline
    fileprivate func removeAllTaskViewsFromTimeline() {
        for view in timelineContentView.subviews {
            if view is TaskClockView || view is TaskDollView || view is TaskDetailView {
                view.removeFromSuperview()
                DLog("\(view) is removed from timelineView")
            }
        }
        taskWithViews = [:]
    }
    
    /// 一个Task在时间轴上总宽度的比例
    fileprivate func widthPercentageOfTask(_ task: Task) -> CGFloat {
        return CGFloat(task.plannedDurationInMinutes) / CGFloat(Date.minutesOfADay)
    }

}

// MARK: Task Detail View

extension HomeViewController {
    /// Generate a TaskDetailBarView and put it on top of its TaskDollView
    func generateTaskDetailBarViewOfTask(_ taskWithView: TaskWithView, inView view: UIView) {
        let detailBarView = TaskDetailBarView()
        detailBarView.taskDetailView.delegate = self
        detailBarView.task = taskWithView.task
        taskWithViews[taskWithView.taskId] = (taskWithView.taskId, taskWithView.task, taskWithView.taskClockView, taskWithView.taskDollView, detailBarView)
        // position
        let centerXConstraint = NSLayoutConstraint(item: detailBarView, attribute: .centerX, relatedBy: .equal, toItem: taskWithView.taskDollView, attribute: .centerX, multiplier: 1.0, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: detailBarView, attribute: .bottom, relatedBy: .equal, toItem: taskWithView.taskDollView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: detailBarView, attribute: .width, relatedBy: .equal, toItem: taskWithView.taskDollView, attribute: .width, multiplier: 1.0, constant: 0)
        // gestures
        detailBarView.taskDetailView.taskDetailTapGestureRecognizer.addTarget(self, action: #selector(HomeViewController.tapTaskDetailView(_:)))
        detailBarView.taskDetailView.taskDetailSwipeDownGestureRecognizer.addTarget(self, action: #selector(HomeViewController.swipeDownTaskDetailView(_:)))
        // add to superView and in front of doll view
        detailBarView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(detailBarView)
        view.sendSubview(toBack: detailBarView)
        view.addConstraints([centerXConstraint, bottomConstraint, widthConstraint])
        view.layoutIfNeeded()
        
        detailBarView.dollView = taskWithView.taskDollView
        detailBarView.showTaskDetailViewAnimated()
    }
    
    /// Swipe down on TaskDetailView
    func swipeDownTaskDetailView(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDetailView = view as? TaskDetailView,
                let taskDetailBarView = taskDetailView.taskDetailBarView
            {
                taskDetailBarView.hideTaskDetailViewAnimated()
            }
        }
    }
    
    /// Tap on TaskDetailView
    func tapTaskDetailView(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            if let view = recognizer.view,
                let taskDetailView = view as? TaskDetailView
            {
                timelineContentView.bringSubview(toFront: taskDetailView)
            }
        }
    }
    
}

// MARK: TaskOptionDelegate

extension HomeViewController: TaskOptionDelegate {
    
    func tappedNPhotoButton(_ task: Task) {
        // privilege check
        if task.isPreStart
            || AppUserInfo.account.isTeacherSupervisor
            || AppUserInfo.account.isTeacherGrowth
        {
            selectedTask = task
            tappedButtonType = .nPhoto
            performSegue(withIdentifier: SegueIdentifier.taskNDetail, sender: self)
        }
    }
    
    func tappedNTextButton(_ task: Task) {
        // privilege check
        if task.isPreStart
            || AppUserInfo.account.isTeacherSupervisor
            || AppUserInfo.account.isTeacherGrowth
        {
            selectedTask = task
            tappedButtonType = .nText
            performSegue(withIdentifier: SegueIdentifier.taskNDetail, sender: self)
        }
    }
    
    func tappedNAudioButton(_ task: Task) {
        // privilege check
        if task.isPreStart
            || AppUserInfo.account.isTeacherSupervisor
            || AppUserInfo.account.isTeacherGrowth
        {
            selectedTask = task
            tappedButtonType = .nAudio
            performSegue(withIdentifier: SegueIdentifier.taskNDetail, sender: self)
        }
    }
    
    func tappedSStartButton(_ task: Task) {
        selectedTask = task
        tappedButtonType = .sStart
        TaskManager.updateTaskTime(
            task.id,
            plannedStartTime: task.plannedStartTime,
            plannedEndTime: task.plannedEndTime,
            actualStartTime: task.actualStartTime,
            actualEndTime: nil,
            success: { [weak self] (task) in
                guard let strongSelf = self else { return }
                DLog("Task actualStartTime updated success")
                if let task = task,
                    let taskWithView = strongSelf.taskWithViews[task.id],
                    let taskDollView = taskWithView.taskDollView,
                    let taskDetailBarView = taskWithView.taskDetailBarView
                {
                    taskDollView.task = task
                    taskDetailBarView.task = task
                }
            },
            failure: { [weak self] error in
                DLog("Task actualStartTime updated failed")
                guard let strongSelf = self else { return }
                if let taskWithView = strongSelf.taskWithViews[task.id],
                    let taskDetailView = taskWithView.taskDetailBarView?.taskDetailView
                {
                    taskDetailView.task = task
                }
        })
    }
    
    func tappedSEndButton(_ task: Task) {
        selectedTask = task
        tappedButtonType = .sEnd
        TaskManager.updateTaskTime(
            task.id,
            plannedStartTime: task.plannedStartTime,
            plannedEndTime: task.plannedEndTime,
            actualStartTime: task.actualStartTime,
            actualEndTime: task.actualEndTime,
            success: { [weak self] (task) in
                guard let strongSelf = self else { return }
                DLog("Task actualEndTime updated success")
                if let task = task,
                    let taskWithView = strongSelf.taskWithViews[task.id],
                    let taskDollView = taskWithView.taskDollView,
                    let taskDetailBarView = taskWithView.taskDetailBarView
                {
                    taskDollView.task = task
                    taskDetailBarView.task = task
                }
            },
            failure: { [weak self] error in
                DLog("Task actualEndTime updated failed")
                guard let strongSelf = self else { return }
                if let taskWithView = strongSelf.taskWithViews[task.id],
                    let taskDetailView = taskWithView.taskDetailBarView?.taskDetailView
                {
                    taskDetailView.task = task
                }
        })
    }
    
    func tappedJPhotoButton(_ task: Task) {
        // privilege check
        if AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherMid || AppUserInfo.account.isStudent || AppUserInfo.account.isGuardian {
            selectedTask = task
            tappedButtonType = .jPhoto
            performSegue(withIdentifier: SegueIdentifier.taskJDetail, sender: self)
        }
    }
    
    func tappedJTextButton(_ task: Task) {
        // privilege check
        if AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherMid || AppUserInfo.account.isStudent || AppUserInfo.account.isGuardian {
            selectedTask = task
            tappedButtonType = .jText
            performSegue(withIdentifier: SegueIdentifier.taskJDetail, sender: self)
        }
    }
    
    func tappedJAudioButton(_ task: Task) {
        // privilege check
        if AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherMid || AppUserInfo.account.isStudent || AppUserInfo.account.isGuardian {
            selectedTask = task
            tappedButtonType = .jAudio
            performSegue(withIdentifier: SegueIdentifier.taskJDetail, sender: self)
        }
    }
    
    func tappedJVideoButton(_ task: Task) {
        // privilege check
        if AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherMid || AppUserInfo.account.isStudent || AppUserInfo.account.isGuardian {
            selectedTask = task
            tappedButtonType = .jVideo
            performSegue(withIdentifier: SegueIdentifier.taskJDetail, sender: self)
        }
    }
    
    func tappedJDemeritButton(_ task: Task) {
        // privilege check
        if AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherMid || AppUserInfo.account.isStudent || AppUserInfo.account.isGuardian {
            selectedTask = task
            tappedButtonType = .jDemerit
            performSegue(withIdentifier: SegueIdentifier.taskJDetail, sender: self)
        }
    }
    
    func tappedPSelfButton(_ task: Task) {
        selectedTask = task
        tappedButtonType = .pSelf
        performSegue(withIdentifier: SegueIdentifier.taskPDetail, sender: self)
    }
    
    func tappedPGuardianButton(_ task: Task) {
        selectedTask = task
        tappedButtonType = .pGuardian
        performSegue(withIdentifier: SegueIdentifier.taskPDetail, sender: self)
    }
    
    func tappedPTeacherButton(_ task: Task) {
        selectedTask = task
        tappedButtonType = .pTeacher
        performSegue(withIdentifier: SegueIdentifier.taskPDetail, sender: self)
    }
    
    /// 点击展开的承诺或家庭活动任务
    func tappedJumpView(_ task: Task) {
        selectedTask = task
        if task.category == .promise {
            // privilege check
            guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSupervisor else { return }
            if let promise = task.promise {
                // 有关联的promise，如果是已加载过的，直接显示，否则加载再显示
                if promise.sequenceNumber > 0 {
                    performSegue(withIdentifier: SegueIdentifier.promise, sender: self)
                } else {
                    // Fetch Promise
                    PromiseManager.fetchPromiseWithPromiseId(
                        promise.id,
                        success: { [weak self] (promise) in
                            guard let strongSelf = self else { return }
                            if let promise = promise {
                                strongSelf.selectedPromise = promise
                                DispatchQueue.main.async {
                                    strongSelf.performSegue(withIdentifier: SegueIdentifier.promise, sender: strongSelf)
                                }
                            } else {
                                strongSelf.presentAlertViewController("获取承诺失败，请重试")
                            }
                        },
                        failure: { [weak self] error in
                            guard let strongSelf = self else { return }
                            strongSelf.presentAlertViewController("获取承诺失败，请重试")
                        })
                }
            } else {
                // 无关联promise，创建新的或者关联
                guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
                    presentAlertViewController("没有选择的学生")
                    return
                }
                // 没有Promise的时候，可以选择创建一个新的Promise或者关联一个已有的Promise
                let alertViewController = UIAlertController(title: "请选择", message: "创建一个新的承诺还是关联一个已有的承诺", preferredStyle: .alert)
                let alertCreateButton = UIAlertAction(title: "创建", style: UIAlertActionStyle.default) { _ in
                    // Create a new Promise if there is no one linked with the Task
                    PromiseManager.generatePromiseWithAccountFamiltyId(
                        selectedStudentAccountFamily.id,
                        taskId: task.id,
                        success: { [weak self] (promise) in
                            guard let strongSelf = self else { return }
                            if let promise = promise {
                                strongSelf.selectedPromise = promise
                                DispatchQueue.main.async {
                                    strongSelf.performSegue(withIdentifier: SegueIdentifier.promise, sender: strongSelf)
                                }
                            } else {
                                strongSelf.presentAlertViewController("生成承诺失败，请重试")
                            }
                        },
                        failure: { [weak self] (error) in
                            guard let strongSelf = self else { return }
                            strongSelf.presentAlertViewController("生成承诺失败，请重试")
                        })
                }
                let alertAssociateButton = UIAlertAction(title: "关联", style: .default, handler: { (_) in
                    guard let accountFamilyId = AppUserInfo.account.selectedStudentAccountFamily?.id else { return }
                    PromiseManager.fetchPromisesWithAccountFamiltyId(
                        accountFamilyId,
                        success: { [weak self] (promises) in
                            guard let strongSelf = self else { return }
                            strongSelf.displayOptionWithOptions(promises, withDelegate: strongSelf)
                            strongSelf.isAssociatingPromise = true
                        },
                        failure:  { [weak self] error in
                            guard let strongSelf = self else { return }
                            strongSelf.presentAlertViewController(error?.localizedFailureReason)
                        })
                })
                alertViewController.addAction(alertCreateButton)
                alertViewController.addAction(alertAssociateButton)
                present(alertViewController, animated: true, completion: nil)
            }
            
        } else if task.category == .family {
            // privilege check
            guard AppUserInfo.account.isGuardian || AppUserInfo.account.isTeacherGrowth || AppUserInfo.account.isTeacherSenior || AppUserInfo.account.isTeacherSupervisor || AppUserInfo.account.isStudent else { return }
            // Fetch FamilyActivity
            FamilyActivityManager.fetchFamilyActivityWithTaskId(
                task.id,
                success: {  [weak self] (familyActivity) in
                    guard let strongSelf = self else { return }
                    if let familyActivity = familyActivity {
                        strongSelf.selectedFamilyActivity = familyActivity
                        DispatchQueue.main.async {
                            strongSelf.performSegue(withIdentifier: SegueIdentifier.familyActivity, sender: strongSelf)
                        }
                    } else {
                        guard let selectedStudentAccountFamily = AppUserInfo.account.selectedStudentAccountFamily else {
                            strongSelf.presentAlertViewController("没有选择的学生")
                            return
                        }
                        // Create a new FamilyActivity if there is no one linked with the Task
                        FamilyActivityManager.generateFamilyActivityWithAccountFamiltyId(
                            selectedStudentAccountFamily.id,
                            taskId: task.id,
                            success: { [weak self] (familyActivity) in
                                guard let strongSelf = self else { return }
                                if let familyActivity = familyActivity {
                                    strongSelf.selectedFamilyActivity = familyActivity
                                    DispatchQueue.main.async {
                                        strongSelf.performSegue(withIdentifier: SegueIdentifier.familyActivity, sender: strongSelf)
                                    }
                                } else {
                                    strongSelf.presentAlertViewController("生成家庭活动失败，请重试")
                                }
                            },
                            failure: { [weak self] (error) in
                                guard let strongSelf = self else { return }
                                strongSelf.presentAlertViewController("生成家庭活动失败，请重试")
                            })
                    }
                },
                failure: { [weak self] error in
                    guard let strongSelf = self else { return }
                    strongSelf.presentAlertViewController("获取家庭活动失败，请重试")
            })
        }
    }
    
}
