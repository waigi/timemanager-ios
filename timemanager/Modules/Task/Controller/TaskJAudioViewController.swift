//
//  TaskJAudioViewController.swift
//  timemanager
//
//  Created by Can on 28/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//


import UIKit
import AVFoundation
import AVKit

class TaskJAudioViewController: UITableViewController {
    
    /// Current selected Task
    var task: Task? {
        get {
            return tabBarController.flatMap() { $0 as? TaskTabBarController }?.task
        }
        set {
            tabBarController.flatMap() { $0 as? TaskTabBarController }?.task = newValue
        }
    }
    
    /// Remote audio
    var audio: Audio? {
        didSet {
            audioLengthLabel?.text = audio?.length.audioLengthShortStringFromSeconds
        }
    }
    
    /// Audio file (Local) URL, which is the one to be saved/played/uploaded and calculate audio length
    fileprivate var audioURL: URL {
        let audioFilename = getDocumentsDirectory()
        let audioURL = URL(fileURLWithPath: audioFilename).appendingPathComponent("tmp.m4a")
        return audioURL
    }
    
    fileprivate var audioRecorder: AVAudioRecorder!
    fileprivate var player: AVPlayer?
    
    // MARK: IBOutlets
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordButtonLongPressGestureRecognizer: UILongPressGestureRecognizer?
    @IBOutlet weak var audioLengthLabel: UILabel?
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        audio = task?.jAudio
        recordButtonLongPressGestureRecognizer?.addTarget(self, action: #selector(TaskJAudioViewController.longPressRecordButton(_:)))
    }
    
    // MARK: IBActions
    @IBAction func tappedPlayButton() {
        guard let audio = audio else { return }
        playAudio(audio)
    }
    
    
    // MARK: Player
    
    fileprivate func playAudio(_ audio: Audio) {
        guard let url = audio.url else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
        player = AVPlayer(url: url as URL)
        startedPlayAudio()
        player?.play()
    }
    
    func startedPlayAudio() {
        playImageView.animationImages = UIView.audioAnimationImages
        playImageView.animationDuration = 2
        playImageView.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(TaskJAudioViewController.finishedPlayAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    func finishedPlayAudio() {
        playImageView.stopAnimating()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    
    // MARK: Recorder
    
    /// Long press on record button
    func longPressRecordButton(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            startRecording()
            displayRecordingAnimationView()
        } else if recognizer.state == .ended || recognizer.state == .cancelled || recognizer.state == .failed {
            removeRecordingAnimationView()
            if recognizer.location(in: recordButton).y < 0 {
                // 上滑取消
                finishRecording(success: false)
            } else {
                finishRecording(success: true)
            }
        }
    }
    
    func startRecording() {
        if audioRecorder == nil {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryRecord)
            } catch {
                DLog("set AVAudioSessionCategoryPlayAndRecord error")
            }
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100.0,
                AVNumberOfChannelsKey: 2 as NSNumber,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
                AVEncoderBitRateKey : 320000
            ] as [String : Any]
            do {
                audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
            } catch {
                finishRecording(success: false)
            }
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        if success {
            uploadAudio()
        } else {
            presentAlertViewController("录音失败，请重试")
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            DLog("set AVAudioSessionCategoryPlayAndRecord error")
        }
    }
    
    /// Audio length in seconds
    fileprivate func audioLengthInSeconds(_ audioFileURL: URL) -> Int {
        let asset = AVURLAsset(url: audioFileURL, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        return Int(audioDurationSeconds)
    }
    
    // MARK: Service Call
    
    fileprivate func uploadAudio() {
        guard let task = task else {
            presentAlertViewController("没有任务可上传录音")
            return
        }
        recordButton.isEnabled = false
        activityIndicator.startAnimating()
        TaskManager.uploadTaskAudio(
            task.id,
            taskType: .j,
            fileURL: audioURL,
            audioDurationSeconds: audioLengthInSeconds(audioURL),
            success: { [weak self] task in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.task = task
                strongSelf.audio = task.jAudio
                strongSelf.presentAlertViewController("录音上传成功")
            },
            failure: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.recordButton.isEnabled = true
                strongSelf.presentAlertViewController(error?.localizedFailureReason)
            })
    }
}

// MARK: AVAudioRecorderDelegate
extension TaskJAudioViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

extension TaskJAudioViewController: AVAudioPlayerDelegate {
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        presentAlertViewController("无法播放 \(error?.localizedDescription)")
    }
}
