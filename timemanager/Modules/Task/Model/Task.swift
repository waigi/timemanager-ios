//
//  Task.swift
//  timemanager
//
//  Created by Can on 9/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftDate
import SwiftyJSON

enum TaskType: String {
    case n = "1"
    case j = "2"
}

enum FileType: String {
    case photo = "1"
    case audio = "2"
    case video = "3"
}

enum TaskCategory: Int {
    case original = -1
    case study = 0
    case read = 1
    case skill = 2
    case rest = 3
    case diet = 4
    case practice = 5
    case entertainment = 6
    case makeFriends = 7
    case dedication = 8
    case family = 9
    case promise = 10
    case introspection = 11
}

class Task {
    
    // MARK: Vars
    
    /// 任务ID，从server端获取，正式生成Task，返回生成的ID
    var id: String
    /// 任务计划开始时间
    var plannedStartTime: Date
    /// 任务计划结束时间
    var plannedEndTime: Date
    /// 任务实际开始时间
    var actualStartTime: Date?
    /// 任务实际结束时间
    var actualEndTime: Date?
    /// 任务N，图，>= 1
    var nPhotos = [Photo]()
    /// 任务N，音，= 1
    var nAudios = [Audio]()
    /// 任务N，文，= 1
    var nTexts = [Text]()
    /// 任务J，图，>= 1
    var jPhotos = [Photo]()
    /// 任务J，文，= 1
    var jTexts = [Text]()
    /// 任务J，音，= 1
    var jAudios = [Audio]()
    /// 任务J，视，= 1
    var jVideos = [Video]()
    /// 任务J，败，= 1
    var jDemerits = [String]()
    /// 任务P，亲
    var pGuardianText: String?
    /// 任务P，自
    var pSelfText: String?
    /// 任务P，师
    var pTeacherText: String?
    /// 承诺任务
    var promise: Promise?
    /// 家庭活动任务
    var familyActivity: FamilyActivity?
    /// 任务事况：内化：学习，阅读，技艺, 平衡：休息，饮食，修行, 外化：娱乐，交友，奉献
    var category: TaskCategory = .original
    /// 评价
    var credit: Credit?
    /// 折扣率
    var discount: Double = 1.0
    /// 是否有过评价
    var hasCredit: Bool = false
    
    // MARK: Calculated Vars
    
    /// Duration between selected start time and end time of a task in seconds.
    var plannedDuration: TimeInterval {
        return plannedEndTime.timeIntervalSince(plannedStartTime)
    }
    
    /// Duration between selected start time and end time of a task in minutes.
    var plannedDurationInMinutes: Int {
        return Int(plannedDuration / 60.0)
    }
    
    /// Duration between actual start time and end time of a task in seconds.
    var actualDuration: TimeInterval {
        guard let startTime = actualStartTime, let endTime = actualEndTime else { return 0.0 }
        return endTime.timeIntervalSince(startTime)
    }
    
    /// Duration between actual start time and end time of a task in minutes.
    var actualDurationInMinutes: Int {
        return Int(plannedDuration / 60.0)
    }
    
    /// Return the latest one
    var nAudio: Audio? { return nAudios.last }
    
    /// Return the latest one
    var nText: Text? { return nTexts.last }
    
    /// Return the latest one
    var jAudio: Audio? { return jAudios.last }
    
    /// Return the latest one
    var jVideo: Video? { return jVideos.last }
    
    /// Return the latest one
    var jText: Text? { return jTexts.last }
    
    /// The Header image of Task based on selected category
    var headerImage: UIImage {
        var image: UIImage
        switch category {
        case .original:
            image = UIImage(named: "Original face Header")!
        case .study:
            image = UIImage(named: "Study Header")!
        case .read:
            image = UIImage(named: "Read Header")!
        case .skill:
            image = UIImage(named: "Skill Header")!
        case .rest:
            image = UIImage(named: "Rest Header")!
        case .diet:
            image = UIImage(named: "Diet Header")!
        case .practice:
            image = UIImage(named: "Practice Header")!
        case .entertainment:
            image = UIImage(named: "Entertainment Header")!
        case .makeFriends:
            image = UIImage(named: "Make friends Header")!
        case .dedication:
            image = UIImage(named: "Dedication Header")!
        case .family:
            image = UIImage(named: "Family Header")!
        case .promise:
            image = UIImage(named: "Promise Header")!
        case .introspection:
            image = UIImage(named: "Introspection Header")!
        }
        return image
    }
    
    var calendarEventStartTitle: String {
        return "🦄CDM任务开始:\(id)"
    }
    
    var calendarEventEndTitle: String {
        return "🐻CDM任务结束:\(id)"
    }
    
    var hasNPhoto: Bool { return !nPhotos.isEmpty }
    var hasNText: Bool { return nText != nil }
    var hasNAudio: Bool { return nAudio != nil }
    var hasN: Bool { return hasNPhoto || hasNText || hasNAudio }
    var hasJPhoto: Bool { return !jPhotos.isEmpty }
    var hasJText: Bool { return jText != nil }
    var hasJAudio: Bool { return jAudio != nil }
    var hasJ: Bool { return hasJPhoto || hasJText || hasJAudio }
    var hasP: Bool { return hasCredit }
    var hasS: Bool { return actualStartTime != nil }
    /// Task is finished
    var isFinished: Bool { return actualStartTime != nil && actualEndTime != nil}
    /// 进入时区前
    var isPreStart: Bool { return Date() < plannedStartTime }
    /// 进行中
    var isInProgress: Bool { return Date() >= plannedStartTime && Date() <= plannedEndTime }
    /// 离开时区后
    var isAfterEnd: Bool { return Date() > plannedEndTime }
    
    // MARK: init
    
    init(id: String, startTime: Date, endTime: Date) {
        self.id = id
        plannedStartTime = startTime
        plannedEndTime = endTime
    }
    
    init?(json: JSON) {
        guard let id = json["id"].string else { return nil }
        let plannedStartTimeInterval = json["plannedStartTime"].double ?? NSDate().timeIntervalSince1970
        let plannedStartTime = NSDate(timeIntervalSince1970: plannedStartTimeInterval / 1000.0)
        let plannedEndTimeInterval = json["plannedEndTime"].double ?? NSDate().timeIntervalSince1970
        let plannedEndTime = NSDate(timeIntervalSince1970: plannedEndTimeInterval / 1000.0)
        self.id = id
        self.plannedStartTime = plannedStartTime as Date
        self.plannedEndTime = plannedEndTime as Date
        if let actualStartTimeInterval = json["actualStartTime"].double {
            actualStartTime = NSDate(timeIntervalSince1970: actualStartTimeInterval / 1000.0) as Date
        }
        if let actualEndTimeInterval = json["actualEndTime"].double {
            actualEndTime = NSDate(timeIntervalSince1970: actualEndTimeInterval / 1000.0) as Date
        }
        if let jsons = json["nPhotos"].array {
            nPhotos = jsons.flatMap() { json in return Photo(json: json) }
        }
        if let jsons = json["nAudios"].array {
            nAudios = jsons.flatMap() { json in return Audio(json: json) }
        }
        if let jsons = json["nTexts"].array {
            nTexts = jsons.flatMap() { json in return Text(json: json) }
        }
        if let jsons = json["jPhotos"].array {
            jPhotos = jsons.flatMap() { json in return Photo(json: json) }
        }
        if let jsons = json["jAudios"].array {
            jAudios = jsons.flatMap() { json in return Audio(json: json) }
        }
        if let jsons = json["jTexts"].array {
            jTexts = jsons.flatMap() { json in return Text(json: json) }
        }
        if let jsons = json["jVideos"].array {
            jVideos = jsons.flatMap() { json in return Video(json: json) }
        }
        jDemerits = json["jDemerits"].arrayObject as? [String] ?? [String]()
        pGuardianText = json["pGuardianText"].string
        pSelfText = json["pSelfText"].string
        pTeacherText = json["pTeacherText"].string
        promise = Promise(json: json["promise"])
        familyActivity = FamilyActivity(json: json["familyActivityDTOs"])
        category = TaskCategory(rawValue: json["taskCategory"].intValue) ?? .original
        if let discount = json["discount"].double {
            self.discount = discount
        }
        hasCredit = json["hasCredit"].boolValue
    }
    
}
