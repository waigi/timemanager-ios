//
//  UIViewControllerExtension.swift
//  beta
//
//  Created by Can on 11/04/2016.
//  Copyright © 2016 Optus. All rights reserved.
//

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

extension UIViewController {
    
    func portraitOrientation() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func openAlertViewController(_ errorString: String) {
        openAlertViewController(errorString, showCancel: false, handler: nil)
    }
    
    func openAlertViewController(_ message: String, showCancel: Bool, handler: ((Void) -> Void)?) {
        let alertViewController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let alertOKButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { _ in
            handler?()
        }
        alertViewController.addAction(alertOKButton)
        
        if showCancel {
            alertViewController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        }
        
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
        }
        
    }
    
}

extension UITableViewController {
    func setDefaultBackground() {
        let backgroundImageView = UIImageView(image: UIImage(named: "background"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
    }
}
